#Author: Shadaba Alam
#This contains all the function core of the correlation function code

from __future__ import print_function,division
import numpy as np
import ccorr
import time

import General_FITS_selection as GFS
import sys
import os

import io

#MULTI PROCESSING
#The problem with multiprocessing is that every subprocess uses copy of the memory
#Experimenting with other options so, far joblibs seems to be doing ok
#still this code need conversion only part of it being converted so far
import multiprocessing as mp
import joblib
import PairCountTOxi_utility as pc2xi_util


#sampmode to sampcode relation
sampcodes=['rmu','rp-pi','rtheta','logrp-pi','logr-theta','logrmu','angular','logangular']

def calculate_xi(xc_dic, nnode, njn, sampmode, samplim,nbins,rand='', sumwt_dic={}, Lbox=0.0, usepip=0,
        coord='cartesian',noDR=False,noRD=False,noRR=False,xi2droot=None,xiprojroot=None):
    
    #To figure out what decomposition to use
    samp=sampcodes[sampmode]
    polar_coord=['rmu','rtheta','logr-theta','logrmu']
    cart_coord=['rp-pi','logrp-pi']
    ang_coord=['angular','logangular']

    xitype='auto'
    # Extract sbins and mubins from x
    sbins =np.linspace(samplim[0],samplim[1],nbins[0]+1)
    mubins=np.linspace(samplim[2],samplim[3],nbins[1]+1)
    ns = len(sbins) - 1
    nmu = len(mubins) - 1
    sbins_mid=0.5*(sbins[:-1]+sbins[1:])
    mubins_mid=0.5*(mubins[:-1]+mubins[1:])
    if('log' in samp):
        sbins_mid=np.power(10,sbins_mid)

    pcdict = {'sbins': sbins,'ns': ns,
            'mubins': mubins,'nmu': nmu,
            }

    keyreplace={'rper': 'sbins','nper': 'ns','rpar':'mubins','npar':'nmu'}
    for key in keyreplace.keys():
        pcdict[key]=pcdict.pop(keyreplace[key])

    if(coord !='sky' and xitype=='auto'):
        if( (noDR or noRD) and noRR):
            RRtype='analytic_RR'
        elif(noRR and (not noDR)):
            RRtype='noRR'
        else:
            RRtype=''
    else:
        RRtype=''
        tdic={'noDR':nodR , 'noRD':noRD, 'noRR':noRR}
        for tkey in ['noDR','noRD','noRR']:
            if(tdic.__dict__[tkey]):
                RRtype=RRtype+tkey

    #determine the estimator
    pclist, xi_estimator=pc2xi_util.get_xitype(RRtype=RRtype,xitype=xitype)

    results =[]
    for ii in range(0,njn+1): 
        # Extract DD, DR, and RR counts
        if(njn==0):
            DD=xc_dic['DD']
        elif(ii==njn):
            DD=xc_dic['DD'][:,:,njn]
        else:
            DD=xc_dic['DD'][:,:,njn]-xc_dic['DD'][:,:,ii]

        DR = np.zeros_like(DD)
        RR = np.zeros_like(DD)

        if 'SDwt' in sumwt_dic:
            Wsum_data = sumwt_dic['SDwt'][ii] if njn > 0 else sumwt_dic['SDwt']
            Wsum_rand = sumwt_dic['SRwt'][ii] if njn > 0 else sumwt_dic['SRwt']
            DDnorm = Wsum_data ** 2
            DRnorm = Wsum_data * Wsum_rand
            RRnorm = Wsum_rand ** 2
        elif 'DDnorm' in sumwt_dic:
            DDnorm = sumwt_dic['DDnorm'][ii] if njn > 0 else sumwt_dic['DDnorm']
            DRnorm = sumwt_dic['DRnorm'][ii] if njn > 0 else sumwt_dic['DRnorm']
            RRnorm = sumwt_dic['RRnorm'][ii] if njn > 0 else sumwt_dic['RRnorm']
        else:
            DDnorm = 1.0
            DRnorm = 1.0
            RRnorm = 1.0

        # Normalize DD
        DD = DD / DDnorm
        pcdict['DD'] = DD
        pcdict['DDnorm'] = DDnorm

        if RRtype in ['', 'noRR']:
            if(njn==0):
                DR=xc_dic['DR']
            elif(ii==njn):
                DR=xc_dic['DR'][:,:,njn]
            else:
                DR=xc_dic['DR'][:,:,njn]-xc_dic['DR'][:,:,ii]

            DR = DR / DRnorm
            pcdict['DR'] = DR
            pcdict['DRnorm'] = DRnorm

        if RRtype in ['', 'analytic_RR']:
            if RRtype == 'analytic_RR' and samp in ['rmu', 'rp-pi', 'logrp-pi'] and Lbox != 0:
                ndens = Wsum_data / (Lbox ** 3)
                if samp == 'rmu':
                    dr3 = (2.0 * np.pi / 3.0) * (sbins[1:] ** 3 - sbins[:-1] ** 3)
                    RR = np.outer(dr3, mubins[1:] - mubins[:-1])
                elif samp == 'rp-pi':
                    dr3 = np.pi * (sbins[1:] ** 2 - sbins[:-1] ** 2)
                    RR = np.outer(dr3, mubins[1:] - mubins[:-1])
                elif samp == 'logrp-pi':
                    rpbins = 10 ** sbins
                    dr3 = np.pi * (rpbins[1:] ** 2 - rpbins[:-1] ** 2)
                    RR = np.outer(dr3, mubins[1:] - mubins[:-1])
                RR = RR * ndens * (Wsum_data - 1)
            else:
                # Assume RR is provided in xc if not using analytic_RR
                if(njn==0):
                    RR=xc_dic['RR']
                elif(ii==njn):
                    RR=xc_dic['RR'][:,:,njn]
                else:
                    RR=xc_dic['RR'][:,:,njn]-xc_dic['RR'][:,:,ii]


            RR = RR / RRnorm
            pcdict['RR'] = RR
            pcdict['RRnorm'] = RRnorm

        #do it for each jacknife
        xi2d=pc2xi_util.estimate_xi(xitype,xi_estimator,pcdict)

        if(samp in polar_coord):
            xi02root='tmp'
            xi02=pc2xi_util.Xi_Legendre(xi02root,sbins_mid, mubins_mid, xi2d,samp=samp,write=0)
            results.append((sbins_mid,mubins_mid,xi2d,xi02))
        elif(samp in cart_coord):
            dr_par=np.mean(pcdict['rpar'][1:] - pcdict['rpar'][:-1])
            wp=np.sum(xi2d,axis=1)*dr_par
            results.append((sbins_mid,mubins_mid,xi2d,wp))

    #Now figure out how to organise and write these to output
    xi_dic=pc2xi_util.process_results(results, samp, njn, xi2droot, xiprojroot)

    return xi_dic 


#writes the pair count to a file
def write_pair_count(pair_count,outfile,args):
   xsamp=np.linspace(args.samplim[0],args.samplim[1],args.nbins[0]+1)
   ysamp=np.linspace(args.samplim[2],args.samplim[3],args.nbins[1]+1)
   with io.FileIO(outfile, 'w') as fwrite:
      xsamp_str=''
      for xx in xsamp:
         xsamp_str="%s %10.5f"%(xsamp_str,xx)
      #fwrite.write(xsamp_str+'\n')
      np.savetxt(fwrite,[xsamp_str],fmt='%s')

      ysamp_str=''
      for yy in ysamp:
         ysamp_str="%s %10.5f"%(ysamp_str,yy)
      #fwrite.write(ysamp_str+'\n')
      np.savetxt(fwrite,[ysamp_str],fmt='%s')

      np.savetxt(fwrite,pair_count,fmt='% 25.15e')
   return 0

#collects the files from individual nodes and write one merged files
def collect_pcnodes(type='DD',cleandir=0):
   for ii in range(0,args.nnode):
      if(args.njn==0):
         tag=''
      else:
         tag='-All'

      outroot=args.outfile+'_nodeid'+str(ii)
      JNdir=outroot+'-'+sampcodes[args.sampmode]+'-JNdir/'
      pcfile=outroot+tag+'-'+sampcodes[args.sampmode]+'-'+type+'.dat'
      data=np.loadtxt(pcfile,skiprows=2)
      if(ii==0):
         pcsum=data
      else:
         pcsum=pcsum+data
      #remove the file
      os.system('rm -f '+pcfile)

      if(args.njn>0):
         if(ii==0):
            pcsumJN=np.zeros(data.shape[0]*data.shape[1]*(args.njn+1)).reshape(
                                 data.shape[0],data.shape[1],(args.njn+1))
         for jj in range(0,args.njn):
            pcfile=JNdir+'jk-'+str(jj)+'-'+type+'.dat'
            data=np.loadtxt(pcfile,skiprows=2)
            pcsumJN[:,:,jj]=pcsumJN[:,:,jj]+data

         #remove the directoty of node and JN for auto correlation
         if(cleandir==1):
            os.system('rm -r '+JNdir)

   #end of node loop
   if(args.njn>0):
      pcsumJN[:,:,args.njn]=pcsum;
      pcsum=pcsumJN

   write_output(pcsum,args,type=type,combnode=1)
   return 0


#clean the files written by individual nodes
def combine_clean(corrtype='auto'):
   #check if all the nodes are done
   t0=time.time()
   check=0
   while(check!=args.nnode-1):
      check=0;
      for ii in range(1,args.nnode):
         donefile=args.outfile+'-'+sampcodes[args.sampmode]
         donefile=donefile+'_nodeid'+str(ii)+'.done'
         if(os.path.isfile(donefile)):
            check=check+1
      if(args.interactive>1):
         print('checking node %d nodes finished out of %d: %d sec'%(
            check,args.nnode,time.time()-t0))

      if(check!=args.nnode-1):
         time.sleep(2)
      else:
         print('This is node 0, Waiting time for other nodes:%d sec'%(time.time()-t0))

   #Collect the pair counts
   if(corrtype=='auto'):
      collect_pcnodes(type='DD',cleandir=0)
      collect_pcnodes(type='DR',cleandir=0)
      collect_pcnodes(type='RR',cleandir=1) #RR must be called in the end after DD and DR call
   elif(corrtype=='cross'):
      collect_pcnodes(type='D1D2',cleandir=0)
      collect_pcnodes(type='D1R2',cleandir=0)
      collect_pcnodes(type='R1D2',cleandir=0)
      collect_pcnodes(type='R1R2',cleandir=1) #RR must be called in the end after DD and DR call
   else:
      print('Invalid corrtype = %s'%corrtype)
      sys.exit()

   #clean the donefiles
   if(args.interactive>1):
      print('clenaing the files for individual nodes')
   for ii in range(1,args.nnode):
      donefile=args.outfile+'-'+sampcodes[args.sampmode]
      donefile=donefile+'_nodeid'+str(ii)+'.done'
      os.system('rm -f '+donefile)

def write_output(xc,args,type='DD',sumwt_dic={},combnode=0):
   if(args.nnode==1 or combnode==1):
      outroot=args.outfile
   else:
      outroot=args.outfile+'_nodeid'+str(args.nodeid)

   if('SDwt' in sumwt_dic.keys()): #args.pip==False and args.iip==False):
      sumwt_order=['SDwt','SRwt','SDwt2','SRwt2']
      wtkey_dic={'SDwt':args.data,'SRwt':args.rand,'SDwt2':args.data2,'SRwt2':args.rand2}
   elif('DDnorm' in sumwt_dic.keys() and type=='DD'):
      sumwt_order=['DDnorm','DRnorm','RRnorm']
      wtkey_dic={'DDnorm':'DDnorm','DRnorm':'DRnorm','RRnorm':'RRnorm'}
   elif('D1D2norm' in sumwt_dic.keys() and type=='D1D2'):
      sumwt_order=['D1D2norm','D1R2norm','R1D2norm','R1R2norm']
      wtkey_dic={'D1D2norm':'D1D2norm','D1R2norm':'D1R2norm',
              'R1D2norm':'R1D2norm','R1R2norm':'R1R2norm'}

   #figure out what weight to write
   wt_order=[]
   if(sumwt_dic!={}):
       for tw in sumwt_order:
           if(tw in sumwt_dic.keys()):
               wt_order.append(tw)

   if(args.njn==0):
      outfile=outroot+'-'+sampcodes[args.sampmode]+'-'+type+'.dat'
      write_pair_count(xc,outfile,args)
      #write the norm file
      if(len(wt_order)!=0 and args.nodeid==0): #only for first node
         outfile=args.outfile+'-'+sampcodes[args.sampmode]+'-norm.dat'
         with io.FileIO(outfile, 'w') as fwrite:
            for tw in wt_order:
                np.savetxt(fwrite,[wtkey_dic[tw]+': %15.10e'%(sumwt_dic[tw])],fmt='%s')
   else:
      #first write the all pair count
      outfile=outroot+'-All-'+sampcodes[args.sampmode]+'-'+type+'.dat'
      xcAll=xc[:,:,args.njn]
      write_pair_count(xcAll,outfile,args)
      #write the norm file
      if(len(wt_order)!=0  and args.nodeid==0): #only for first node
         outfile=args.outfile+'-All-'+sampcodes[args.sampmode]+'-norm.dat'
         with io.FileIO(outfile, 'w') as fwrite:
            for tw in wt_order:
                np.savetxt(fwrite,[wtkey_dic[tw]+': %15.10e'%(sumwt_dic[tw][args.njn])],fmt='%s')
      #now write the each jacknife in the folder
      JNdir=outroot+'-'+sampcodes[args.sampmode]+'-JNdir/'
      if(not os.path.isdir(JNdir)):
         os.system('mkdir '+JNdir)
      if(args.nodeid==0):
         JNdirnorm=args.outfile+'-'+sampcodes[args.sampmode]+'-JNdir/'
         if(not os.path.isdir(JNdirnorm)):
            os.system('mkdir '+JNdirnorm)

      for ii in range(0,args.njn):
         outfile=JNdir+'jk-'+str(ii)+'-'+type+'.dat'
         xcJN=xc[:,:,ii]
         if(combnode==0):
            xcJN_All=xcAll-xcJN
            write_pair_count(xcJN_All,outfile,args)
         else:
            write_pair_count(xcJN,outfile,args)

         #write the norm file
         if(len(wt_order)!=0 and args.nodeid==0):
            outfile=JNdirnorm+'jk-'+str(ii)+'-norm.dat'
            with io.FileIO(outfile, 'w') as fwrite:
                for tw in wt_order:
                    np.savetxt(fwrite,[wtkey_dic[tw]+': %15.10e'%(sumwt_dic[tw][ii])],fmt='%s')

   return 0


def mp_pair_count_NOFITS(data1_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c, sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid):
    def corr2d_warp(data1_c, fp1, lp1, data2_c, fp2, lp2, rlim_c, nbins, nhocells, blen_c, pos_min_c, sampmode, njn, pbc, los, interactive):
        pc = ccorr.corr2dpy(data1_c, fp1, lp1, data2_c, fp2, lp2, rlim_c, nbins[0], nbins[1], nhocells,
                    blen_c, pos_min_c, sampmode, njn, pbc, los, interactive)
        if interactive > 1:
            print(f'using pid: {os.getpid()}')
        return pc

    ndata = len(data1_c)
    ndata2 = len(data2_c)

    if interactive > 1:
        print('creating queue')

    nproc_this = 1 if ndata < 1000 else nproc
    chunknode = int(np.ceil(ndata / nnode))
    chunksize = int(np.ceil(chunknode / nproc_this))
    nodebeg = nodeid * chunknode

    def create_job_inputs():
        for ii in range(nproc_this):
            ind1 = chunksize * ii + nodebeg
            ind2 = min(chunksize * (ii + 1) + nodebeg, ndata if nodeid == nnode - 1 else chunknode + nodebeg)
            yield (data1_c, ind1, ind2, data2_c, 0, ndata2, rlim_c, nbins,
                   nhocells, blen_c, pos_min_c, sampmode, njn, pbc, los, interactive)

    if nproc_this > 1:
        with joblib.Parallel(n_jobs=nproc_this, verbose=0) as parallel:
            jobres = parallel(joblib.delayed(corr2d_warp)(*job_input) for job_input in create_job_inputs())
        pcAll = sum(jobres[1:], jobres[0])
    else:
        pcAll = corr2d_warp(*next(create_job_inputs()))

    return pcAll


#This is to account for pip weighting
def mp_pair_count_wpip(data1_c, data2_c,fib1_c, fib2_c, nfib_int1, nfib_int2, rlim_c, nbins, 
        nhocells, blen_c, pos_min_c, need_angup,log_theta_lim_c, nbin_log_theta, wang_up_c, args):
   def corr2d_warp_wpip(data1_c, fp1, lp1, fib1_c,nfib_int1, data2_c, fp2, lp2, fib2_c, nfib_int2,need_iip,rlim_c, nbins, nhocells, blen_c, 
           pos_min_c, need_angup,log_theta_lim_c, nbin_log_theta, wang_up_c,argsin,out_q):
      #pc={}
      #pc[mp.current_process()]
      pc= ccorr.corr2dpy_wpip(data1_c, fp1, lp1, data2_c, fp2, lp2, rlim_c, nbins[0],nbins[1], nhocells,
                 blen_c, pos_min_c, argsin.sampmode, argsin.njn,
                 argsin.pbc,argsin.los,argsin.interactive,
                 fib1_c, fib2_c, nfib_int1, nfib_int2, argsin.bit_perlong, need_iip,
                 need_angup, log_theta_lim_c, nbin_log_theta, wang_up_c,argsin.fastpc)
      if(argsin.interactive>1):
         print('using pid:',os.getpid())
      #print(pc)
      #outdict={}
      #outdict[mp.current_process()]=pc
      out_q.put(pc)

   ndata=data1_c.shape[0]
   ndata2=data2_c.shape[0]

   #To decide whether to usee iip or pip
   if(args.iip==True):
       need_iip=1
   else:
       need_iip=0


   if(args.fastpc):
       data1_c,data2_c=fast_consistency_check(data1_c,data2_c,args)

   #print('nfib_int1, nfib-int2: ',nfib_int1,nfib_int2)
   if(args.interactive>1):
      print('creating queue')
   # Each process will get 'chunksize' nums and a queue to put his out
   # dict into
   out_q = mp.Queue()
   chunknode = int(np.ceil(ndata / float(args.nnode)))
   chunksize = int(np.ceil(chunknode / float(args.nproc)))
   procs = []

   nodebeg=args.nodeid*chunknode
   if(args.interactive>1):
      print('submitting processes:')
   for ii in range(args.nproc):
      ind1=chunksize*ii+nodebeg
      ind2=chunksize*(ii+1)+nodebeg
      if(ii==args.nproc-1):
         if(args.nodeid==args.nnode-1):
            ind2=ndata
         else:
            ind2=chunknode+nodebeg

      if(args.interactive>2):
         print("submit: node= %d ,proc= %d : %d %d %d"%(
            args.nodeid,ii,ind1,ind2, ind2-ind1))

      p = mp.Process(
                target=corr2d_warp_wpip,
                args=(data1_c, ind1,ind2,fib1_c, nfib_int1, data2_c,0,ndata2,fib2_c, nfib_int2, need_iip,rlim_c, nbins,
                nhocells, blen_c, pos_min_c, need_angup,log_theta_lim_c, nbin_log_theta, wang_up_c,args,out_q))
      procs.append(p)
      p.start()

   # Collect all results into a single result dict. We know how many dicts
   # with results to expect.
   resultdict = {}
   for ii in range(args.nproc):
      #resultdict[ii]=out_q.get(timeout=3600.)
      resultdict[ii]=out_q.get()
      if(args.interactive>2):
         print('collect proc:',ii)

      if(ii==0):
         pcAll=resultdict[ii]
      else:
         pcAll=pcAll+resultdict[ii]

   if(args.interactive>1):
      print('joining results')
   # Wait for all worker processes to finish
   for p in procs:
      p.join()

   return pcAll

def setup_angular_upweight(args,pair_type='DD'):
    '''This loads the angular upweight file and set up for loading'''

    #pair_list=['DD','DR']
    #if(args.noRD==False and args.data2!=''):
    #    pair_list=pair_list+['RD']

    if(args.ang_up and pair_type in ['DD','DR','RD']):
        #load the file first
        wang_data=np.loadtxt(args.wang_up_file)

        nbin_log_theta=wang_data.shape[0]

        log_theta_bin=np.linspace(np.log10(wang_data[0,1]),np.log10(wang_data[-1,2]),nbin_log_theta+1)

        #check if the file indeed have uniform log bins
        #check minimum values are aligned
        assert(np.sum(np.abs(log_theta_bin[:-1]-np.log10(wang_data[:,1]))/np.log10(wang_data[:,1]) <1e-3))
        #check maximum values are aligned
        assert(np.sum(np.abs(log_theta_bin[1:]-np.log10(wang_data[:,2]))/np.log10(wang_data[:,2]) <1e-3))

        delta_log_theta=log_theta_bin[1]-log_theta_bin[0]
        #store minimum,maximum and delta_log_theta
        log_theta_lim=np.array([log_theta_bin[0],log_theta_bin[-1],delta_log_theta])

        #store things in dictioray in c compatible array and return
        wang_dic={'need_angup':1,
                'nbin_log_theta':nbin_log_theta,
              'log_theta_lim_c': np.ascontiguousarray(log_theta_lim,dtype='double')
              }
        if(pair_type=='DD'):
            #get the upweight for DD
            wang_dic['wang_up_c']= np.ascontiguousarray(wang_data[:,3],dtype='double')
        elif(pair_type=='DR'):
            #get the upweight for DR
            wang_dic['wang_up_c']= np.ascontiguousarray(wang_data[:,4],dtype='double')
        elif(pair_type=='RD'):
            #get the upweight for DR
            wang_dic['wang_up_c']= np.ascontiguousarray(wang_data[:,5],dtype='double')
        else:
            print('invalid pair type for angular upweighting: %s'%(pair_type))
            sys.exit()
    else:
        wang_dic={'need_angup':0,
                'nbin_log_theta':0, 
              'log_theta_lim_c': np.ascontiguousarray(np.array([0,0]),dtype='double'),
              'wang_up_c': np.ascontiguousarray(np.array([0,0]),dtype='double')
              #'wang_up_DR_c': np.ascontiguousarray(np.array([0,0]),dtype='double')
              }

    return wang_dic
    
def fast_consistency_check(data1_LC,data2_LC,args):
    '''This performs some needed consistency check to use fas version of paircount'''
    if(args.sampmode in [6,7]): #for angular clustering the points must be in unit sphere
        dat_order=['data','rand']
        for tt,tdat in enumerate([data1_LC,data2_LC]):
            rad1=np.sqrt(np.sum(np.power(tdat[:,:3],2),axis=1))
            if(np.sum(np.abs(rad1-1<1e-4))!=rad1.size):
                print("To use fastpc for angular clustering the input co-ordinate must be on unit sphere")
                print("This is not true and hence converting %s before performing pair counts"%dat_order[tt])
                tdat[:,:3]=tdat[:,:3]/rad1[:,None]

    return data1_LC, data2_LC


def mp_pair_count(data1_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid, filetype):
    if(filetype!='fits'):
        #pcAll=mp_pair_count_NOFITS(data1_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
        pcAll=mp_pair_count_NOFITS(data1_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c, sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid)
        nd1=data1_c.shape[0]
        nd2=data2_c.shape[0]
    else:
        #populate pair type as this might be useful for some things
        if(data1_c['typein']=='data' and data2_c['typein']=='data'):
            data1_c['pair_type']='DD'; data2_c['pair_type']='DD'
        elif(data1_c['typein']=='data' and data2_c['typein']=='rand'):
            data1_c['pair_type']='DR'; data2_c['pair_type']='DR'
        elif(data1_c['typein']=='rand' and data2_c['typein']=='data'):
            data1_c['pair_type']='RD'; data2_c['pair_type']='RD'
        elif(data1_c['typein']=='rand' and data2_c['typein']=='rand'):
            data1_c['pair_type']='RR'; data2_c['pair_type']='RR'


        if(args.pip+args.iip==False):
            data1_LC=args.LPfunc(data1_c,0,data1_c['isel'].size,args)
            data2_LC=args.LPfunc(data2_c,0,data2_c['isel'].size,args)
            fib1_LC=np.array([])
            fib2_LC=np.array([])
            pcAll=mp_pair_count_NOFITS(data1_LC, data2_LC, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
        else:
            data1_LC, fib1_LC=args.LPfunc(data1_c,0,data1_c['isel'].size,args)
            data2_LC, fib2_LC=args.LPfunc(data2_c,0,data2_c['isel'].size,args)
   

            #if fib1 and fib2 is empty array then pass some small array with nfib_in=0
            #print('fibLC:',fib1_LC)
            if(fib1_LC.size!=0):  
                nfib_int1=fib1_LC.shape[1]
            else:  
                nfib_int1=0; 
                fib1_LC=np.ascontiguousarray(np.array([[0,0],[0,0]]),dtype=int) 
            
            if(fib2_LC.size!=0):  
                nfib_int2=fib2_LC.shape[1]
            else:  
                nfib_int2=0; 
                fib2_LC=np.ascontiguousarray(np.array([[0,0],[0,0]]),dtype=int) 

            #get the dictionary for angular upweighting
            wang_dic=setup_angular_upweight(args,pair_type=data1_c['pair_type'])

            print('Using mp_pair_count_wpip') 
            pcAll=mp_pair_count_wpip(data1_LC, data2_LC,fib1_LC, fib2_LC, nfib_int1,nfib_int2,
                                  rlim_c, nbins, nhocells, blen_c, pos_min_c,
                                  wang_dic['need_angup'],wang_dic['log_theta_lim_c'], 
                                  wang_dic['nbin_log_theta'], wang_dic['wang_up_c'], args)

        nd1=data1_LC.shape[0]
        nd2=data2_LC.shape[0]

    return pcAll, nd1,nd2


def node_clean(args,xitype='auto'):
   if(args.nodeid!=0):
      #write a file to indicate that it has finished
      donefile=args.outfile+'-'+sampcodes[args.sampmode]
      donefile=donefile+'_nodeid'+str(args.nodeid)+'.done'
      os.system('touch '+donefile)
   elif(args.nnode>1):
      #wait until all other nodes are done and then combine
      #results from all nodes and clean the disk
      combine_clean(corrtype=xitype)

   return


def compute_auto(data_c, rand_c,rlim_c, nbins, blen_c, pos_min_c=[0,0,0], 
        sampmode=3, njn=0, nproc=8, pbc=1, los=1,nhocells=200,rand='',
        interactive=1,nnode=1, nodeid=0,noDD=False,noDR=False,noRD=False,noRR=False,filetype='txt',
        xi2droot=None,xiprojroot=None,sumwt_dic={}):

   #figure out optimum nhocells
   ndata_min=data_c.shape[0]
   if(ndata_min/np.power(nhocells,3)<0.05):
      nhocells=int(np.power(ndata_min/0.05,0.33))
      print('Updating nhocells to : %d, because ndata_min=%d'%(nhocells,ndata_min))
    
   #compute DD pair count and write to file
   xc_dic={}
   t1 = time.time() #pair count start time
   if(noDD==False):
       if(interactive>0):
           print('\nWorkind on DD (. every 100k points):')
       xcDD,nd1,nd2=mp_pair_count(data_c, data_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,
               sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid,filetype)
       xc_dic['DD']=xcDD
       #write_output(xcDD,args,type='DD',sumwt_dic=sumwt_dic)
       if(interactive>0):
          print("\nFinished DD in %d sec with %d process and %d %d particles, max scale %10.4f"%(
             int(time.time() - t1),nproc,nd1,nd2 ,rlim_c[1]))
   else:
       print('Not computing DD counts as noDD is set to True')

   #if(args.noDR==True or args.rand=='norand'):
   #    print('Not computing DR counts as noRR is set to True')
   #    node_clean(args,xitype='auto')
   #    return 0

   #compute DR pair count and write to file
   if(noDR==False and rand!='norand'):
       if(interactive>0):
           print('\nWorkind on DR (. every 100k points):')
       xcDR, nd1, nd2=mp_pair_count(data_c, rand_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,
               sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid,filetype)
       xc_dic['DR']=xcDR
       #write_output(xcDR,args,type='DR')
       if(interactive>0):
           print("\nFinished DR in %d sec with %d process and %d %d particles, max scale %10.4f"%(
             int(time.time() - t1),nproc,nd1,nd2,rlim_c[1]))
   else:
       print('Not computing DR counts as noDR is set to True')
   #if(args.noRR==True):
   #    print('Not computing RR counts as noRR is set to True')
   #    node_clean(args,xitype='auto')
   #    return 0

   #compute RR pair count and write to file
   if(noRR==False and rand!='norand'):
       if(interactive>0):
           print('\nWorkind on RR (. every 100k points):')
       xcRR,nd1 ,nd2=mp_pair_count(rand_c, rand_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,
               sampmode, njn, pbc, los, interactive, nproc, nnode, nodeid,filetype)
       xc_dic['RR']=xcRR
       #write_output(xcRR,args,type='RR')
       if(interactive>0):
           print("\nFinished RR in %d sec with %d process and %d %d particles, max scale %10.4f"%(
             int(time.time() - t1),nproc,nd1,nd2,rlim_c[1]))
   else:
       print('Not computing RR counts as noRR is set to True')
   if(interactive>0):
       print("\nFinished job in %d sec with %d process and nrand %d ,max scale %10.4f"%(
         int(time.time() - t1),nproc,nd1,rlim_c[1]))

   #estimate the correlation
   xi_dic=calculate_xi(xc_dic, nnode, njn, sampmode, rlim_c,nbins,rand=rand, sumwt_dic=sumwt_dic, 
           Lbox=blen_c, usepip=0,coord='cartesian',noDR=noDR,noRD=noRD,noRR=noRR,
           xi2droot=xi2droot,xiprojroot=xiprojroot)

   #clean the node
   #node_clean(args,xitype='auto')

   return xi_dic
#end of compute_auto

def compute_cross(data_c, rand_c,data2_c, rand2_c, rlim_c, nbins, blen_c, pos_min_c,nhocells,args,sumwt_dic={}):
   #compute DD pair count and write to file
   print('\nWorkind on D1D2 (. every 100k points):')
   t1 = time.time() #pair count start time
   xcDD,nd1,nd2=mp_pair_count(data_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
   write_output(xcDD,args,type='D1D2',sumwt_dic=sumwt_dic)
   if(args.interactive>0):
      print("\nFinished D1D2 in %d sec with %d process and %d %d particles, max scale %10.4f"%(
         int(time.time() - t1),args.nproc,nd1,nd2,rlim_c[1]))

   if(args.noDR==True):
       print('Not computing D1R2, R1D2 and R1R2 counts as noDR is set to True')
       node_clean(args,xitype='cross')
       return 0

   #compute D1R2 pair count and write to file
   print('\nWorkind on D1R2 (. every 100k points):')
   xcDR,nd1,nd2=mp_pair_count(data_c, rand2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
   write_output(xcDR,args,type='D1R2')
   if(args.interactive>0):
      print("\nFinished D1R2 in %d sec with %d process and %d %d particles, max scale %10.4f"%(
         int(time.time() - t1),args.nproc,nd1,nd2,rlim_c[1]))

   if(args.noRD==True):
       print('Not computing R1D2 and R1R2 counts as noRD is set to True')
       node_clean(args,xitype='cross')
       return 0

   #compute R1D2 pair count and write to file
   print('\nWorkind on R1D2 (. every 100k points):')
   xcDR,nd1,nd2=mp_pair_count(rand_c, data2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
   write_output(xcDR,args,type='R1D2')
   if(args.interactive>0):
       print("\nFinished R1D2 in %d sec with %d process and %d %d particles, max scale %10.4f"%(
               int(time.time() - t1),args.nproc,nd1,nd2 ,rlim_c[1]))

   if(args.noRR==True):
       print('Not computing R1R2 counts as noRR is set to True')
       node_clean(args,xitype='cross')
       return 0

   #compute RR pair count and write to file
   print('\nWorkind on R1R2 (. every 100k points):')
   xcRR,nd1,nd2=mp_pair_count(rand_c, rand2_c, rlim_c, nbins, nhocells, blen_c, pos_min_c,args)
   write_output(xcRR,args,type='R1R2')
   if(args.interactive>0):
       print("\nFinished RR in %d sec with %d process and %d %d particles, max scale %10.4f"%(
                 int(time.time() - t1),args.nproc,nd1,nd2,rlim_c[1]))

   #if(args.interactive>0):
   print("\nFinished job in %d sec with %d process for max scale %10.4f"%(int(time.time() - t1),args.nproc,rlim_c[1]))

   #clean the node
   node_clean(args,xitype='cross')

   return 0
#end of compute_cross

