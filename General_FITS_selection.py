#Author: Shadab Alam, March 2018
#This holds general fits selection function
import numpy as np
import time
import fitsio as F
import os

try:
   import pandas as pd
   pd_flag=1
except:
   pd_flag=0
   print('''pandas not found: you can install pandas from
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads
            The code will work without pandas but pandas will speed up loading large text files
            ''')


def getminmax(data,rand=''):
   #determin blen
   blen  = np.array([0,0,0],dtype='double')
   POS_min = np.array([0,0,0],dtype='double')
   POS_max = np.array([0,0,0],dtype='double')
   for ii in range(0,3):
      #compute the minimum
      d1=np.min(data[:,ii])
      try:
         d2=np.min(rand[:,ii])
         POS_min[ii]=min(d1,d2)
      except:
         POS_min[ii]=d1
      #compute the maximum
      d1=np.max(data[:,ii])
      try:
         d2=np.max(rand[:,ii])
         POS_max[ii]=max(d1,d2)
      except:
         POS_max[ii]=d1
      #compute the blen
      blen[ii]=POS_max[ii]-POS_min[ii]

   return POS_min,POS_max, blen


#This function is not used anywhere currently
def getvalFITS_keys(fin,indsel,tkey,coordmap):
    '''Evaluate s column with operation between fits columns
    fin: FITS handle
    indsel: selected indices
    tkey: The key we are trying to evluate
    coodmap: contains dictionary of map for example if xx= a*x+ b*y then it should be
    coordmap['xx']=['a*x','+b*y']
    '''

    nel=len(coordmap[tkey])
    tarr=fin[1][coordmap[tkey][0]][indsel]

    if(nel>1):
       for ii in range(0,nel):
           elkey=coordmap[tkey][ii]
           if(elkey[0]=='+'):
               tarr=tarr+fin[1][elkey[1:]][indsel]
           elif(elkey[0]=='-'):
               tarr=tarr-fin[1][elkey[1:]][indsel]
           else:
               print("invalid inputs of coordmap:", elkey)
               print("Should have operation before key val")
               print("Full coordmap: ",coordmap)
               print("exiting!")
               sys.exit()

    return tarr

    #tvmin=np.min(tarr)
    #tvmax=np.max(tarr)
    #return tvmin, tvmax


#This uses pandas if available otherwise numpy loadtxt to read files
def load_txt(fname):
   if(pd_flag==1):
      tb=pd.read_table(fname,delim_whitespace=True,comment='#',header=None)
      #tb=tb.as_matrix()
      tb=tb.to_numpy()
   else:
      tb=np.loadtxt(fname)
   return tb

def xyz2polar(xyz):
   rdist=np.sqrt(np.sum(np.power(xyz,2),axis=1))
   theta=np.arccos(xyz[:,2]/rdist)
   phi=np.arccos(xyz[:,0]/np.sqrt((rdist*rdist)-(xyz[:,2]*xyz[:,2])))
   #where y<0 phi=phi+pi for 0-2pi range
   ind_y=xyz[:,1]<0
   phi[ind_y]=2*np.pi-phi[ind_y]

   return rdist,theta,phi


#load the data file
def prep_data_rand(datafile,randfile,args,RSD=0):
   t0=time.time()
   if(args.interactive>1):
      print("Loading files:\n "+datafile+" \n "+randfile)

   if(args.filetype in ['txt','xyzfits'] or args.filetype=='polartxt'):
      if(args.filetype in ['txt','polartxt']):
         data=load_txt(datafile)
      elif(args.filetype=='xyzfits'):
         with F.FITS(datafile) as fin:
            data=fin[1]['xyz'][0]
            data=np.column_stack([data,np.ones(data.shape[0])])
      
      if(RSD==1):
         data[:,2]=np.mod(data[:,2]+(data[:,5]/100.0),args.Lbox)

      if(args.filetype=='polartxt'):
         import mycosmo as mcosm
         XYZ,interp=mcosm.RDZ2XYZ(data[:,0],data[:,1],data[:,2],args.H0,args.omM,1-args.omM,interp='')
         if(data.shape[1]>3):
            #data=np.column_stack([XYZ,data[:,3]])
            data[:,:3]=XYZ
            #if(args.njn>0):
            #    data=np.column_stack([XYZ,data[:,4]])
         else:
            data=np.column_stack([XYZ,np.ones(XYZ.shape[0])])
      elif(data.shape[1]>3):
         data=np.column_stack([data[:,:3],np.ones(data.shape[0])])
      print('read:',datafile,data.shape)
      if(os.path.isfile(randfile)):
         rand=load_txt(randfile)
         if(args.filetype=='polartxt'):
            import mycosmo as mcosm
            XYZ,interp=mcosm.RDZ2XYZ(rand[:,0],rand[:,1],rand[:,2],args.H0,args.omM,1-args.omM,interp='')
            if(rand.shape[1]>3):
                rand[:,:3]=XYZ
               #rand=np.column_stack([XYZ,rand[:,3]])
               #if(args.njn>0):
               #   rand=np.column_stack([XYZ,rand[:,4]])
            else:
               rand=np.column_stack([XYZ,np.ones(XYZ.shape[0])])

         #selecting subsample of random
         if(args.randfactor>0):
            nrand=args.randfactor*data.shape[0]
            idx=np.random.choice(np.arange(rand.shape[0]),nrand,replace=False)
            print('nrand selected: ',rand.shape[0],nrand)
            rand=rand[idx,:]


   elif(args.filetype=='webskyHOD'):
      galaxy=load_txt(datafile)
      rdist=np.sqrt(np.sum(np.power(galaxy[:,:3],2),axis=1))
      #apply radial selection
      ind_r=(rdist>args.z1z2[0])*(rdist<=args.z1z2[1]);
      data=np.copy(galaxy[ind_r,:5])
      rdist=rdist[ind_r]
      if(RSD==1):
         vlos=np.sum(galaxy[ind_r,:3]*galaxy[ind_r,3:6],axis=1)/rdist
         red_rdist=rdist+(vlos/args.H0)
         #new redshift space positions
         ratio_r=red_rdist/rdist
         data[:,:3]=data[:,:3]*ratio_r[:,None]
         rdist=red_rdist

      #empty the galaxy array
      galaxy=None

      #set weights to 1
      data[:,3]=1.0
      #set jackknife region with healpix for data
      import healpy as hp
      tmp_r,theta,phi=xyz2polar(data[:,:3])
      nside=hp.npix2nside(args.njn)
      data[:,4]=hp.ang2pix(nside,theta,phi)
      ngal=data[:,0].size
      print('number of data selected/total',ngal,ind_r.size)

      #generate random
      if(randfile=='generate'):
         nrand=int(args.randfactor*data.shape[0])
         #generate random in the sphere and assign distance sampled from the rdist
         phi=np.random.random(nrand)*2.0*np.pi
         theta=np.arccos(np.random.random(nrand)*2-1)
         #sample rdist
         rand_r=np.random.choice(rdist,size=nrand,replace=True)
         rand=np.column_stack([np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)])
         rand=rand*rand_r[:,None]
         #add weights
         rand=np.column_stack([rand,np.ones(nrand),np.zeros(nrand)-1])
         #assign jackknif rgions
         rand[:,4]=hp.ang2pix(nside,theta,phi)
         print('number of randoms generated:',nrand)
      else:
         print('invalid random file option: with this filetype you should use ranfile=generate')
         sys.exit()

      
      np.savetxt('tmpdir/tmpdata.txt',data)
      np.savetxt('tmpdir/tmprand.txt',rand)

   elif(args.filetype=='fhandle'):
      if (args.pbc==1):
         data=readBOXwithifhandle(datafile)
      else:
         data,rand=readSKYwithifhandle(datafile,randfile)
   elif(args.filetype=='fitsa'):
      fin=F.FITS(datafile)
      data=np.column_stack([fin[1]['xx'][:],fin[1]['yy'][:],fin[1]['zz'][:],fin[1]['wt'][:],fin[1]['jn'][:]])
      fin.close()
      fin=F.FITS(randfile)
      rand=np.column_stack([fin[1]['xx'][:],fin[1]['yy'][:],fin[1]['zz'][:],fin[1]['wt'][:],fin[1]['jn'][:]])
      fin.close()
      #data,rand=readSKYwithifits(datafile,randfile)
   else:
      print('unknown file type')
      sys.exit()
   #generate randoms with fixed seed for periodic box if random file
   #is not provides
   if(args.pbc==1 and randfile=='generate'  and args.filetype!='webskyHOD'):
      nrand=int(args.randfactor*data.shape[0])
      print("random file not given, Generating internal %d random"%nrand)
      rand=randoms_fixedseed(data,nrand,args.Lbox)
   elif(args.pbc==1 and randfile=='norand' and args.filetype!='webskyHOD'):
      print("random file not given, should use analytic randoms")
      nrand=args.njn
      rand=randoms_fixedseed(data,nrand,args.Lbox)

   return prep_data_random_jn_wt(data,rand,njn=args.njn,los=args.los,pbc=args.pbc,interactive=args.interactive,
           outarr='c',datafile=datafile,randfile=randfile)
   
def prep_data_random_jn_wt(data,rand,njn=0,los=1,pbc=1,interactive=1,outarr='python',datafile='array',randfile='array'):
   t0=time.time()
   #add jacknife region if its pbc, los=zaxis and data have only x,y,z,wt
   if(pbc==1 and los==1 and data.shape[1]==4 and njn>0):
      data,rand=add_pbc_jncol(data,rand,njn,los)
      if(interactive>0):
          print('Added pbc jn reg:',data[:,4].min(),data[:,4].max(),rand[:,4].min(),rand[:,4].max())
      if(rand.shape[0]==njn): #to pass through the check that randoms exists in each jn ragion
          rand[:,4]=np.arange(0,njn)

   ngal =int(data.shape[0])
   nrand=int(rand.shape[0])
   if(interactive>=1):
      print("%10d galaxies from %s"%(ngal,datafile))
      print("%10d randoms from %s"%(nrand,randfile))
      print("\nTime:%d sec"%int(time.time() - t0))

   POS_min,POS_max, blen=getminmax(data,rand=rand)

   if(interactive>1):
      print("Extent of the data/random:")
      print("MIN: ",', '.join('%8.2f'%x for x in POS_min) )
      print("MAX: ",', '.join('%8.2f'%x for x in POS_max) )
      print("Length: ",', '.join('%8.2f'%x for x in blen),'\n' )

   #compute sum of weights and write norm file
   if(njn==0):
      sumwtdata=np.sum(data[:,3])
      sumwtrand=np.sum(rand[:,3])
   else:
      sumwtdata=np.zeros(njn+1)
      sumwtrand=np.zeros(njn+1)
      #all weight
      sumwtdata[njn]=np.sum(data[:,3])
      sumwtrand[njn]=np.sum(rand[:,3])
      #jn weights
      for ii in range(0,njn):
         ind=data[:,4]==ii
         sumwtdata[ii]= sumwtdata[njn]-np.sum(data[ind,3])
         ind=rand[:,4]==ii
         sumwtrand[ii]= sumwtrand[njn]-np.sum(rand[ind,3])

   #convert arrays to contiguous array
   if(njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
      rand_c = np.ascontiguousarray(rand[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      rand_c = np.ascontiguousarray(rand[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != njn-1 or np.max(rand[:,4]) != njn-1):
         print("number of jacknife given and in the file do not match")
         print("file njn data,random:  %d %d given njn: %d"
               %(np.max(data[:,4]),np.max(rand[:,4]),njn))
         sys.exit()

   return data_c, rand_c, blen, POS_min, POS_max, sumwtdata, sumwtrand


def readBOXwithifhandle(datafile,Lbox=3000):
   '''This function reads file with Martins filehandle, assumes cubic box and read only XYZ co-ordinate,
   set weight =1 and in case of jacknife creates equalsize regions, also generate uniform randoms in the box size,
   Also assumes co-ordinate to be in between zeros and 1 which is scaled by Lbox in Mpc/h'''

   #import Martins file handler
   import ndfilehandler as FH
   data = FH.read_file(datafile)['pos']*Lbox
   data=np.column_stack([data,np.ones(data.shape[0])])
   return data

def readSKYwithifhandle(datafile,randfile):
   '''This function reads file with Martins filehandle,
   assumes SKY projected mocks read only RA,DEC and Z,
   set weight =1 and in do not support jacknife at the moment,
   Assumes cosmology '''
   H0=68; omM=0.30; omL=1-omM

   #import Martins file handler
   import ndfilehandler as FH
   #read RA,DEC and redshift
   RA  = FH.read_file(datafile)['RA']
   DEC = FH.read_file(datafile)['DEC']
   #Red = FH.read_file(datafile)['ZR']
   Red = FH.read_file(datafile)['Z']

   XYZ,interp=mcosm.RDZ2XYZ(RA,DEC,Red,H0,omM,omL,interp='')
   data=np.column_stack([XYZ,np.ones(XYZ.shape[0])])

   #Working on randoms
   RA  = FH.read_file(randfile)['RA']
   DEC = FH.read_file(randfile)['DEC']
   #Red = FH.read_file(randfile)['ZR']
   Red = FH.read_file(randfile)['Z']
   #Setup subsample random
   nrand=int(args.randfactor*data.shape[0])
   idx = np.random.choice(np.arange(RA.size), nrand, replace=False)
   print('nrand:',RA.size,nrand)
   #subsampled RA,DEC and Red
   RA=RA[idx]; DEC=DEC[idx];Red=Red[idx]
   print('RED:',np.min(Red) , np.max(Red), np.mean(Red))

   XYZ,interp=mcosm.RDZ2XYZ(RA,DEC,Red,H0,omM,omL,interp='')
   rand=np.column_stack([XYZ,np.ones(XYZ.shape[0])])

   return data,rand

##
def randoms_fixedseed(data,nrand,Lbox):
   '''box size is decide by the data'''

   np.random.seed(600)
   POS_min,POS_max, blen=getminmax(data)
   #check if all the dimension have same lengtj
   #assert (np.abs(blen[0]-blen[1])<1e-4*blen[0] and
   # np.abs(blen[1]-blen[2])<1e-3*blen[0]), "Not a cubic box all dimensions are not same: %12.8lf %12.8lf %12.8lf "%(blen[0],blen[1],blen[2])

   #Lbox=Lbox #blen[1]
   rand=np.random.random(nrand*3).reshape(nrand,3)*Lbox
   rand=np.column_stack([rand,np.ones(nrand)])

   return rand

def is_perfect_power(number,power):
    """
    Indicates (with True/False) if the provided number is a perfect power.
    """
    number = abs(number)  # Prevents errors due to negative numbers
    return round(number ** (1 / power)) ** power == number

def add_pbc_jncol(data,rand,njn=0,los=1):
   '''If the input is a periodic box and los is along z axis then jacknife region is simply equal area region in the x-y space which can be done in using this function and not needed to be supplied with data file.
   If njn is perfect cube then 3d jacknife is implemented
   If njn is perfect square then jn split is in x-y plane
   '''


   if(is_perfect_power(njn,3)):
       jntype='3d'
       NJNx=int(np.round(njn ** (1. / 3)))
       NJNy=NJNx;NJNz=NJNx
       print('Using 3d Jacknife:', NJNx, NJNy, NJNz)
   elif(is_perfect_power(njn,2)):
       jntype='2d'
       NJNx=int(np.sqrt(njn))
       NJNy=int(njn/NJNx)
       print('Using 2d Jacknife:', NJNx, NJNy)
   else:
       print('''error: njn must be either a perfect square or perfect cube''')
       sys.exit()

   #adding jacknife regions
   if(njn>0 and los==1):
      POS_min,POS_max, blen=getminmax(data,rand=rand)
      #NJNx=int(np.sqrt(args.njn))
      #NJNy=int(args.njn/NJNx)
      for ii in range(0,2):
         if(ii==0): mat=data
         else: mat=rand

         #get the x and y indx as integers
         indx=np.zeros(mat[:,0].size,dtype=int)
         indy=np.zeros(mat[:,0].size,dtype=int)

         for kk in range(0,indx.size):
             indx[kk]=int(NJNx*(mat[kk,0]-POS_min[0])/blen[0])
             indy[kk]=int(NJNy*(mat[kk,1]-POS_min[1])/blen[1])
         #apply modulo operation on x an y index
         indx=np.mod(indx,NJNx)
         indy=np.mod(indy,NJNy)

         if(jntype=='2d'):
            jnreg=NJNy*indx+indy
         elif(jntype=='3d'):
            indz=int_(NJNz*(mat[:,2]-POS_min[2])/blen[2])
            indz=np.mod(indz,NJNz)
            jnreg=NJNz*(NJNy*indx+indy)+indz

         #convert index to integers
         #indx.astype(np.int64); indy.astype(np.int64);
         mat=np.column_stack([mat,jnreg])

         if(ii==0): data=mat
         else: rand=mat
      return data,rand
   else:
      print('not appropriate input to add jacknife internally')
      #sys.exit()
      return 0
