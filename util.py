#This contains some utility functions
#Part of Shadab's Corrfunc library

from __future__ import print_function,division
import numpy as np

def compute_JN_data(RA,DEC,JNregfile):
   RAbuf=np.zeros(2) #for north and south
   RAmin=np.zeros(2) #for north and south
   RAmax=np.zeros(2) #for north and south

   for ff,file in enumerate(JNregfile):
   #extract information from north and south header
      found=0
      lines=open(file,'r').readlines()
      for ii in range(0,10):
         spl=lines[ii].split()
         if(spl[0]=='#RAbuf='):
            RAbuf[ff]=float(spl[1])
            found=found+1;
         if(spl[0]=='#RAcut='):
            RAmin[ff]=float(spl[1])
            RAmax[ff]=float(spl[2])
            found=found+1;
         if(found==2):
            break

      if(found!=2):
         print(JNregfile[ff].split('/')[-1], RAbuf[ff],RAmin[ff],RAmax[ff])
         print('Region files missing header information')
         sys.exit(-1)
      #select the indices
      ind1=RA>=RAmin[ff]; ind2=RA<RAmax[ff];
      if(RAmin[ff]>RAmax[ff]):
         ind=ind1+ind2
      else:
         ind=ind1*ind2
      #North only
      if(ff==0):
         NS_N=np.copy(ind)
      else: #South only
         NS_S=np.copy(ind)

   #if(len(JNregfile)>1):
   #   print '\n\n***Galaxies (N,S,common,All): ',np.sum(NS_N),np.sum(NS_S), np.sum(NS_N*NS_S),RA.size
   #else:
   #   print '\n\n***Galaxies (selected,All): ',np.sum(NS_N),RA.size

   JN_reg=np.zeros(RA.size,dtype='int')
   JN_reg[NS_N]=compute_JN(RA[NS_N],DEC[NS_N],JNregfile[0],
                                base=0 ,RAbuf=RAbuf[0])
   if(len(JNregfile)>1):
      NNorthJN=np.max(JN_reg[NS_N])+1
      JN_reg[NS_S]=compute_JN(RA[NS_S],DEC[NS_S],JNregfile[1],
                                base=NNorthJN,RAbuf=RAbuf[1])

   return JN_reg


def compute_JN(RA,DEC,reg_file,base=0,RAbuf=0,debug=0):
   if(RAbuf!=0):
      RA=np.mod(RA-RAbuf,360)

   JN_reg=np.zeros(RA.size,dtype='int')-1
   regions=np.loadtxt(reg_file)
   for ii in range(0,regions.shape[0]):
      ind1=RA >=regions[ii,1]
      ind2=RA <regions[ii,3]
      ind3=DEC>=regions[ii,2]
      ind4=DEC<regions[ii,4]
      ind=ind1*ind2*ind3*ind4
      JN_reg[ind]=ii+base

   if(debug==1):
      print("RA-DEC region of data:" ,reg_file.split('/')[-1], RAbuf)
      print(np.min(RA),np.max(RA),np.min(DEC),np.max(DEC))
      print("RA-DEC region of Jacknife file")
      print(regions[0,1],regions[-1,3],regions[0,2],regions[-1,4])

   return JN_reg
