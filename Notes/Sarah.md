## This is a set of notes written for Sarah

In order to calculate the eBOSS LRG autocorrelation function with jackniffe covariance you need to do following steps.
1) Clone and Install this code git@gitlab.com:shadaba/CorrelationFunction.git
2) Set up the jacknife regions
3) Run the pair count
4) Convert pair count to desired measurements

These steps are described in more details below.

## Directory structure (It is only a recommendation)
Lets first setup some directory structure to work with
datadir='/Users/shock/Documents/Projects/Projects/MultiHODFitter/eBOSS_fromSarah/'

The above is the main directory under which you want to store everything.  
Lets say the catalog files are under a subdirectory called data. I would create following subdirectory to store various pieces under datadir.  
$datadir/JN/   : To store jacknife files  
$datadir/XI    : To store everything related to correlation function  
$datadir/XI/PairCount/  : To store paircounts  
$datadir/XI/XI02/ : To store multipoles 
$datadir/XI/XI2D/ : To store 2d correlation functions  
$datadir/XI/WP/ : To store wps  


## Set up jacknife:
You can use following command to generate the jacknife partition:   
python Jacknife_region_2D_keys.py -r1 <rand_fits_file> -NjackRA 15 -NjackDEC 6 -ax1 RA -ax2 DEC -odir <$jnroot>

Here rand_fits_file: is the random file -odir should contain jndir with file root I had following:  
jnroot=$datadir/JN/eBOSS_LRG_NGC

Once this is finished then look at the this file to see the partition created by jacknife:  
jnroot+'-zlim-0-0-RA-DEC-2D_JN-15-6_v1.png'

The actual partition stored in this file. This is the jnfile you should provide in the configuration file.  
jnroot+'-zlim-0-0-Border_RA-DEC_JN-15-6_v1.txt'


## Run Pair Count:
This is simple to run you just have to type:  
   python Runme_Correlation.py -config_file config/config_sarah.ini

Please check the config/config_sarah.ini for details, but the only thing you need to update is the data and rand file along with outfile. You should also update the path to jnfile in the configuration file.

## Convert Pair Count to correlation function:
   python PairCountTOxi.py -sampmode 3 -plots 1 -pcroot jnk/abc -xi2droot jnk/xi2d/abc -xi02root jnk/xi02/abc -njn 90 -usepip 1

If the paircount was calculated using usepip=1 then set usepip to 1.
The pcroot should be same as the outfile in the configuration file.
xi2droot and xi02root can be anything. I generally keep them in different directory as mentioned above keeping the file root same as pcroot.

At this point you should have a file of wp with jacknife realization which you can plot and compare with other measurement.

## Update 19 October 2020: verion 0 pip weight
Now the code has ability to deal with pip weight and systematic weight.
Check the new config  (config/config_sarah_wpip.ini) file for pip weight, basically a new input called usepip when set to 1 can deal with pip weight. The main changes you might want to do should be in the catlib/sarah_eBOSS.py . This function script should also provide an insight into what is used fro systematic weight and can be easily modified.

