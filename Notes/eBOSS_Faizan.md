# This describes the steps used to analyze Faizan's eBOSS catalogues

- Define main data dir
$datadir=/Users/shock/Documents/Projects/Projects/MultiHODFitter/eBOSS_fromFaizan/
The above can be set using: export datadir='/Users/shock/Documents/Projects/Projects/MultiHODFitter/eBOSS_fromFaizan/'
Analysis Steps
==================
 * ## Step 1: Create Jacknife partition:
     ### For LRG
     * NGC: python Jacknife_region_2D_keys.py -r1 $datadir/eBOSS_LRG_NGC_pip_v7_2.ran.fits -NjackRA 15 -NjackDEC 6 -ax1 RA -ax2 DEC -odir $datadir/JN/eBOSS_LRG_NGC
     * SGC: python Jacknife_region_2D_keys.py -r1 $datadir/LRG/eBOSS_LRG_SGC_pip_v7_2.ran.fits -NjackRA 9 -NjackDEC 7 -ax1 RA -ax2 DEC -RAcut 300 60 -odir $datadir/JN/eBOSS_LRG_SGC

        This will create following jacknife partition files which needs to be used in config_file
     $datadir/JN/eBOSS_LRG_SGC-zlim-0-0-Border_RA-DEC_JN-15-6_v1.txt
     $datadir/JN/eBOSS_LRG_SGC-zlim-0-0-Border_RA-DEC_JN-9-7_v1.txt

     ### For QSOs
     * NGC: python Jacknife_region_2D_keys.py -r1 $datadir/QSO/eBOSS_QSO_NGC_pip_v7_2.ran.fits -NjackRA 15 -NjackDEC 6 -ax1 RA -ax2 DEC -odir $datadir/JN/eBOSS_QSO_NGC
     * SGC: python Jacknife_region_2D_keys.py -r1 $datadir/QSO/eBOSS_QSO_SGC_pip_v7_2.ran.fits -NjackRA 9 -NjackDEC 7 -ax1 RA -ax2 DEC -RAcut 300 60 -odir $datadir/JN/eBOSS_QSO_SGC

     ### For ELGs (only the CLUSTERING==1 should be used for this exercise, words from Faizan)
     *NGC: python Jacknife_region_2D_keys.py -r1 $datadir/ELG/eBOSS_ELG_NGC_pip_v7.ran.fits -NjackRA 15 -NjackDEC 6 -ax1 RA -ax2 DEC -odir $datadir/JN/eBOSS_ELG_NGC -add_sel CLUSTERING==1
     *SGC: python Jacknife_region_2D_keys.py -r1 $datadir/ELG/eBOSS_ELG_SGC_pip_v7.ran.fits -NjackRA 30 -NjackDEC 3 -ax1 RA -ax2 DEC -RAcut 300 60 -odir $datadir/JN/eBOSS_ELG_SGC -add_sel CLUSTERING==1

 * ## Step 2: Compute angular clustering to obtain angular up-weights.
     This requires first creating the config file for angular clustering these are 
     SGC: config/config_faizain_ang_SGC.ini
     NGC: config/config_faizain_ang_NGC.ini
     #for ELG a different config file as the file names are different
     ELG SGC: config/config_faizain_ang_SGC_ELG.ini
     ELG NGC: config/config_faizain_ang_NGC_ELG.ini

     Now run angular clustering with different weight and selections:
   * Parent sample with uniform weight: python Runme_Correlation.py -config_file config/config_faizan_ang_SGC.ini -selection ALL
   * FIBER sample with  pip weight: python Runme_Correlation.py -config_file config/config_faizan_ang_SGC.ini -selection FIBER -pip True
   * FIBER sample with  uniform weight: python Runme_Correlation.py -config_file config/config_faizan_ang_SGC.ini -selection FIBER

    Finally to get create the angular upweight files run following:

    `python compute_angular_upweight.py -parent_root **root to the parent paircount** -fiber_root **root to the fiber paircount**`

    The above step should create an appropriate file with angular upweights which can be passed to the code if angular upweight needs to be applied.
    
 * ## Step 3: To compute angular clustering with different weights
    python Runme_Correlation.py -config_file config/config_faizan_ang_<SGC/NGC>.ini -selection CLUSTERING -pip True -iip True -ang_up True

 * ## Step 4: To compute wp (projected correlation function)
    python Runme_Correlation.py -config_file config/config_faizan_wp_<SGC/NGC>.ini -selection CLUSTERING -pip True -iip True -ang_up True

 * ## Step 5: To compute multipoles

   
