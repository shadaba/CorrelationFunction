#### version 1 0f the target sv3
/global/cfs/cdirs/desi/survey/catalogs/SV3/LSS/LSScats/Target4Ang/dr9v0.57.0/sv3_v1/

#The calls to create the jakknice for the SV3 target catalogs
# For v1 with catalog here: /global/cfs/cdirs/desi/survey/catalogs/SV3/LSS/LSScats/Target4Ang/dr9v0.57.0/sv3_v1/
NDECALS:  175     , 50           73.4e6,  66.2e6          0.68,    34, 10
SDECALS:  140.    , 100          109.5e6, 97.3e6,         1,      25, 20
NBMZLS:   200     , 50,         64.4e6,   57.9e6 ,         0.64,     35, 11
DES:      140 , 80 ,            57.95e6, 54.09e6,            0.57,     20,14
SDECALS_noDES: 140, 44,         48.6e6, 42.1e6,            0.43,    24,9
DES_noLMC: 140,80               54.98, 51.44,              0.528,   24,11 


- Define main data dir
$datadir=/global/cfs/cdirs/desi/survey/catalogs/SV3/LSS/LSScats/Target4Ang/dr9v0.57.0/sv3_v1/
The above can be set using: export datadir='/global/cfs/cdirs/desi/survey/catalogs/SV3/LSS/LSScats/Target4Ang/dr9v0.57.0/sv3_v1/'

#NDECALS
python Jacknife_region_2D_keys.py -r1 $datadir/NDECALS_randoms-1-0x5.fits -NjackRA 34 -NjackDEC 10 -ax1 RA -ax2 DEC -odir $datadir/JN/NDECALS_mask -add_sel MASKBITS==0

#SDECALS
python Jacknife_region_2D_keys.py -r1 $datadir/SDECALS_randoms-1-0x5.fits -NjackRA 25 -NjackDEC 20 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/SDECALS_mask -add_sel MASKBITS==0

#NBmZLS
python Jacknife_region_2D_keys.py -r1 $datadir/NBMZLS_randoms-1-0x5.fits -NjackRA 27 -NjackDEC 11 -ax1 RA -ax2 DEC -odir $datadir/JN/NBMZLS_mask -add_sel MASKBITS==0

#DES
python Jacknife_region_2D_keys.py -r1 $datadir/DES_randoms-1-0x5.fits -NjackRA 20 -NjackDEC 14 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/DES_mask -add_sel MASKBITS==0


#SDECALS_noDES
python Jacknife_region_2D_keys.py -r1 $datadir/SDECALS_noDES_randoms-1-0x5.fits -NjackRA 24 -NjackDEC 9 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/SDECALS_noDES_mask -add_sel MASKBITS==0


#DES_noLMC
python Jacknife_region_2D_keys.py -r1 $datadir/DES_noLMC_randoms-1-0x5.fits -NjackRA 24 -NjackDEC 11 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/DES_mask -add_sel MASKBITS==0







#### version 0 0f the target
/global/cfs/cdirs/desi/survey/catalogs/SV3/LSS/LSScats/Target4Ang/dr9v0.57.0/sv3/

#The calls to create the jakknice for the SV3 target catalogs
          delta RA ,delta DEC   nrand, nrand_vetomask , NJN ratio, NJN-RA, NJN-DEC
NDECALS:  175     , 50           80e6,  72.23e6          0.74,    44, 10
SDECALS:  140.    , 100          110e6, 97.75e6,         1,      30, 20
NBMZLS:   200     , 50,         70e6,   62.8e6 ,         0.64,     35, 11
DES:      140 , 80 ,            60e6, 56.0e6,            0.57,     23,15
SDECALS_noDES: 140, 44,         50e6, 43.4e6,            0.44,     19,14


- Define main data dir
$datadir=/global/project/projectdirs/desi/users/shadaba/SV3Targets/Catalogs/
The above can be set using: export datadir='/global/project/projectdirs/desi/users/shadaba/SV3Targets/Catalogs/'

#NDECALS
python Jacknife_region_2D_keys.py -r1 $datadir/NDECALS_randoms-1-0x5.fits -NjackRA 44 -NjackDEC 10 -ax1 RA -ax2 DEC -odir $datadir/JN/NDECALS_mask -add_sel MASKBITS==0

#SDECALS
python Jacknife_region_2D_keys.py -r1 $datadir/SDECALS_randoms-1-0x5.fits -NjackRA 30 -NjackDEC 20 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/SDECALS_mask -add_sel MASKBITS==0

#NBmZLS
python Jacknife_region_2D_keys.py -r1 $datadir/NBMZLS_randoms-1-0x5.fits -NjackRA 35 -NjackDEC 11 -ax1 RA -ax2 DEC -odir $datadir/JN/NBMZLS_mask -add_sel MASKBITS==0

#DES
python Jacknife_region_2D_keys.py -r1 $datadir/DES_randoms-1-0x5.fits -NjackRA 23 -NjackDEC 15 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/DES_mask -add_sel MASKBITS==0


#SDECALS_noDES
python Jacknife_region_2D_keys.py -r1 $datadir/SDECALS_noDES_randoms-1-0x5.fits -NjackRA 19 -NjackDEC 14 -ax1 RA -ax2 DEC -RAcut 260 100 -odir $datadir/JN/SDECALS_noDES_mask -add_sel MASKBITS==0
