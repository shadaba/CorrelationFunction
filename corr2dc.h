#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//performs the popcount of 1s
size_t countOnes (long n);

int cwrap(int x, int y);
int relwrap(double *dxyz, double *blen);
void maxradius(double *rlim , int samp, double *maxrad);
void init_mesh(long *ll, long *hoc, double *p, long fp, long np, 
         int nattr, int nho, double *blen, double *posmin);
void corr2d(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, double *rlim,
        int nbins0, int nbins1, int nhocells, double *blen, double *posmin, int samp, 
	int njn, int pbc, int los, int interactive);
int find_bin(double *axyz, double *bxyz, int samp, int *nsamp, 
      double *sampbound, int los, int pbc, double *blen, int *rx, int *ry);
//To evaluate iip weight
void evaluate_iip(double *pp, int nattr, long *fib, int nfib_int, int bit_perlong, long fp, long np);
//This also include pip weights
void corr2d_wpip(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2,
      double *rlim, int nbins0, int nbins1, int nhocells, double *blen,
      double *posmin, int samp, int njn, int pbc, int los, int interactive,
      long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip,
      int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up);
//This tried to do more efficiently for finding bins
void corr2d_wpip_efficient(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2,
      double *rlim, int nbins0, int nbins1, int nhocells, double *blen,
      double *posmin, int samp, int njn, int pbc, int los, int interactive,
      long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip,
      int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up);
