#!/usr/bin/env python

"""
This program computes 2d correlation function.
"""

from __future__ import print_function,division
import numpy as np
import time
import fitsio as F

import importlib
import sys
import os
import argparse

#MULTI PROCESSING
import multiprocessing as mp

import ccorr_utility as corrutility
import General_FITS_selection as GFS


#This is the c library for fast calculations in c
import ccorr

import mycosmo as mcosm
#import General_function as GF

#To add the config file functionality
import input_lib

#This converts paircount to xi
import PairCountTOxi as pcxi

#sys.path=['/global/u1/s/shadaba/Projects/SV3/CorrelationFunction/']+sys.path
#Try to imports pandas if available
try:
   import pandas as pd
   pd_flag=1
except:
   pd_flag=0
   print('''pandas not found: you can install pandas from 
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads 
            The code will work without pandas but pandas will speed up loading large text files
            ''')


__author__ = "Shadab Alam, Hongyu Zhu"
__version__ = "1.0"
__email__  = "shadaba@andrew.cmu.edu"

if __name__=="__main__":  
    parser = argparse.ArgumentParser(description='Calculate 2d correlation function:')
    #these should be input
    parser.add_argument('-config_file',default=None,help='Input can be provided using a config file or command line. A mix of both  (i.e. command line and config file) can also be used. Some inputs can only be provided in config file. If an option is provided in config file and command line then the value in config file will be used.')
    parser.add_argument('-Cinterpolation' ,default=False, type=bool, help='If the string inputs should be interpolated then set this to True')
    parser.add_argument('-plots',type=int,default=1)
    parser.add_argument('-fastpc' ,default=False, type=bool, help='Set to call the fast version of pair counterSet, This is still being developed and tested')
    parser.add_argument('-sampmode' ,type=int,default=0,
                  help='''select the sampling mode 0=rmu , 1=rpara-rperp, 2=rtheta, 3=log(rpar)-rperp (The range of r_perp in samplim should be entered in the log space), 
                  4=log(r)-theta (The range of r in samplim should be entered in the log space and theta 0-3.14)
                  5= log(r)-mu 
                  6= theta in radian ,
                  7= log10(theta) with theta in radian, the limits should be in log''') 
    parser.add_argument('-data' ,default='example/data.txt',help='''data file with columns being  X,Y,Z,weight,JN,Vx,Vy,Vz
The minimum columns should be X,Y,Z,weight. X,Y,Z should be in Mpc/h and Vx,Vy,Vz is in simulation unit used incase of RSD''')
    parser.add_argument('-rand' ,default='example/randoms.txt',help='same as data files. No velocity is used for randoms. rand=generate for periodic box generates the random and rand=norand for periodic box should use analytic RR counts')
    parser.add_argument('-noDD' ,default=False, type=bool, help='Set this to True to skip DD count: This means DD for auto and D1D2 for cross')
    parser.add_argument('-noRR' ,default=False, type=bool, help='Set this to True to skip RR count if only DD and DR counts are sufficient')
    parser.add_argument('-noDR' ,default=False, type=bool, help='Set this to True to skip DR count: This means DR for auto and D1R2 for cross')
    parser.add_argument('-noRD' ,default=False, type=bool, help='Set this to True to skip RD count: only used for cross correlation')
    parser.add_argument('-RSD'  ,type=int,default=0, help='This applies RSD (move galaxy from real space to redshift space) where needed')
    parser.add_argument('-data2' ,default='',help='second data file for cross-correlation, same structure as first data file')
    parser.add_argument('-rand2' ,default='',help='second random file for cross-correlation file')
    parser.add_argument('-nbins'  ,nargs='+',type=int,default=[32,100],help='number of bins in xi sampling')
    parser.add_argument('-samplim',nargs='+',type=float, default=[0,30,0,1],help='The range for 2d correlation function calculation depending on the modes it will take lower and higher limit for the two axis')
    parser.add_argument('-pbc'  ,type=int,default=0, help='This is to say whether the PBC should be applied or not') 
    parser.add_argument('-los'  ,type=int,default=0, help='''This is to choose a definition for line of sight. 0=los is defined as mid point of the pair, 1=los is defined along the Z axis,\n(only for cross correlation 2,3) \n2=los is define along the galaxies in the first data file, 3= second data file''') 
    parser.add_argument('-njn'  ,type=int,default=0, help='''0 no jacknife column, >0 number of jacknife regions,
                                 When pbc=1 and los=1 then jacknife can be added internally by providing njn.
                                 If njn is perfect cube then 3d jacknife is done, if njn is perfect square then 2d jacknife is done in x-y plane.''') 
    parser.add_argument('-Lbox'  ,type=float,default=0, help='box size to be used in case of pbc with eith generate random or analytic random')
    parser.add_argument('-outfile' ,default='example/PairCount/out',help="set the output file name")

    parser.add_argument('-interactive'  ,type=int, default=1, help="set 1 to print some information while running") # set zero to not print out things

    parser.add_argument('-filetype'  ,default='txt',help="The script can read few different file type: txt, polartxt, fhandle,fits") 
    parser.add_argument('-z1z2'  ,nargs='+',type=float,default=[-1,-1], help='To restrict the redshift range if RA,DEC,Redshift is provided.') 
    parser.add_argument('-randfactor'  ,type=int,default=0, help='''This is used incase random file is not provided for periodic box to generate uniform random in the box with a fixed seed. The number of randoms is set as the randomfactor times the number of data''')

    parser.add_argument('-coord',default='xyz', help='This is to decide whether cartesian coordinate inputs xx,yy,zz are available for coord=xyz or sky cor-ordinates RA,DEC,Z needs to be used with provided cosmology for coord=sky')

    parser.add_argument('-H0'  ,type=float,default=67.6, help='Hubble constant when cosmology is needed') 
    parser.add_argument('-omM'  ,type=float,default=0.315, help='matter density  when cosmology is needed') 

    parser.add_argument('-MemSplit'  ,type=int,default=0, help='Set 1 to load small amount of data in memory at a time, only works with FITS file') 
    parser.add_argument('-num_load'  ,type=int,default=1000000, help='number of points to be loaded at a time') 

    parser.add_argument('-nproc'  ,type=int,default=4, help='number of multiprocess to run') 
    parser.add_argument('-nnode'  ,type=int,default=1, help='number of nodes to run') 
    parser.add_argument('-nodeid'  ,type=int,default=0, help='nodeid for this job')
    parser.add_argument('-selection'  , default='ALL',help='''This keyword can be used to apply any kind of selection for your catalogues''')
    parser.add_argument('-jnfile'  , default='',help='''A file containing Jacknife partitions''')

    parser.add_argument('-catname'  , default=None,help='''The name of the catalog, this decides 
                                         how to load the catalogue if a catalog utility function is available.
                                         When catname module is available then LPfunc and FITS_selection
                                         is used from this module. otherwise one can provide it directly''')
    parser.add_argument('-LPfunc'  , default=None,help='LoadData_part function needs to be passed')
    parser.add_argument('-GetXYZ'  , default=None,help='returns the XYZ cor-ordinates of the data or random')
    parser.add_argument('-FITS_Selection', default=None,help='for loading selection function needs to be passed')

    parser.add_argument('-pip', type=bool, default=False, help='''set this to true if pip weight is needed, this cant be set true with iip
                                                                make sure LPfunc returns data and bitwise map''')
    parser.add_argument('-iip', type=bool, default=False, help='''set this to true if iip weight is needed, this cant be set true with pip''')
    parser.add_argument('-ang_up', type=bool, default=False, help='''set this to true if angular-up weight is needed,
                                             can be used in combination with either iip or pip. To use this you should also provide the 
                                             DD pair count for the parent sample as the function of theta using wang_up_file''')
    parser.add_argument('-nocheck_pairnorm', type=bool, default=False, help='''The normalization when pair weight is applied is an approxximation. The validity of this approxximation depends on a theta_max. The code by default check for its convergence and quit if not valid. Sometimes we might want to continue despite not convergence and hence set this key to True for continuing despite non-convergence''')
    
    parser.add_argument('-wang_up_file', default=None,help='''A 5 columns file with: theta_mid, theta_min, theta_max, DD_par/DD_fib , DR_par/DR_fib,
                                                           theta is  in radians and log bins''')
    parser.add_argument('-bit_perlong'  ,type=int,default=31, help='''Generally long (64 bit) numbers are used to store realization, 
                                           But different setup may use different number of bits and this can be set here. 
                                           For example in eBOSS pip run only 32 bit of the 64 bit per integer were used. ''')

    parser.add_argument('-theta_max'  ,type=float,default=0.017, help='''The maximum theta upto which we want to compute 
                         DD pair count with pip and iip in order to normalize pip weights. 
                         Only used for pip normalization otherwise ignored''')
    parser.add_argument('-CompgZ' ,type=bool,default=False,help="True To compute the shell estimator for gravitational redshift")
    parser.add_argument('-extra', default='',help='''any additional input can be given here''')

    args = parser.parse_args()
    #print(args)
    #merge the config file and command line input
    args=input_lib.merge_config(args)

    #basic input checks
    if(args.sampmode in [6,7]):
        #add the las entries for second dimension
        args.samplim=[args.samplim[0],args.samplim[1],0,1]
        args.nbins=[int(args.nbins),1]
        #print('angular:',args.samplim,type(args.nbins[0]),type(args.nbins[1]))
        
    assert len(args.samplim)==4
    assert len(args.nbins) ==2

    #make sure only one of pip and iip is set
    assert args.pip*args.iip==False

    if(args.MemSplit==1 and args.filetype!='fits'):
        print("***\n Error:The MemSplit can only work when input file is fits fromat")
        print("MemSplit = %d filetype= %s"%(args.MemSplit,args.filetype))
        print("** Exiting **")
        sys.exit()

    if(args.pbc==1 and args.los !=1):
        print("*** INVALID INPUT combination ***")
        print("periodic boundary condition can be applied only with los=1(along z axis)")
        sys.exit()

    if(args.los==2 or args.los==3):
        if(args.data2=='' or args.rand2==''):
            print("*** INVALID INPUT combination ***")
            print("The los=2,3 can be used only for corss correlation")
            print("Please provide second data and random file for cor-correlation")
            sys.exit()

    #sampmode to sampcode relation
    #sampcodes=['rmu','rp-pi','rtheta','logrp-pi','logr-theta']

    #rlim  = np.array(args.samplim,dtype='double')
    #nbins = np.array(args.nbins,dtype='int')
    nhocells = 200

    t0 = time.time() #start time
    print('outfile :',args.outfile)


def readSKYwithifits(datafile,randfile):
   '''This function reads file with fits format, 
   assumes SKY projected mocks read only RA,DEC and Z,
   set weight =1 and in do not support jacknife at the moment,
   Assumes cosmology '''
   H0=68; omM=0.30; omL=1-omM; #z=3.0; dz=0.25

   #fixed JN files for now
   dir='/home/shadaba/Projects/DESI/Jacknife/'
   Nfile='North-v1.0_qso_10rand-zlim-0.0-4.0-NorthBorder_RA-DEC_JN-11-4_v1.txt'
   Sfile='South-v1.0_qso_10rand-zlim-0.0-4.0-SouthBorder_RA-DEC_JN-5-4_v1.txt'
   JNfile=[dir+Nfile,dir+Sfile]

   #import fits file handler
   import fitsio as F
   findat=F.FITS(datafile,'r')
   #Zsel="ZR >= "+str(args.z1z2[0])+" && ZR <"+str(args.z1z2[1])
   if(args.z1z2[0]>0 and args.z1z2[1]>args.z1z2[0]):
       Zsel="Z >= "+str(args.z1z2[0])+" && Z <"+str(args.z1z2[1])
   else:
       print('*** No redshift slection is applied, if this is unexpected then check z1z2 entries')
       Zsel='1>0'

   indsel=findat[1].where(Zsel)

   #read RA,DEC and redshift
   RA  = findat[1]['RA'][indsel]
   DEC = findat[1]['DEC'][indsel]
   #Red = findat[1]['ZR'][indsel]
   Red = findat[1]['Z'][indsel]

   XYZ,interp=mcosm.RDZ2XYZ(RA,DEC,Red,H0,omM,omL,interp='')
   data=np.column_stack([XYZ,np.ones(XYZ.shape[0])])
   if(args.njn>0):
      JN_reg=GF.compute_JN_data(RA,DEC,JNfile)
      data=np.column_stack([data,JN_reg])

   #Working on randoms
   finran=F.FITS(randfile,'r')
   #Zsel="Z >= "+str(args.z1z2[0])+" && Z <"+str(args.z1z2[1])
   indsel=finran[1].where(Zsel)

   #read RA,DEC and redshift
   RA  = finran[1]['RA'][indsel]
   DEC = finran[1]['DEC'][indsel]
   Red = finran[1]['Z'][indsel]

   #Setup subsample random
   nrand=int(args.randfactor*data.shape[0])
   print('nrand:',RA.size,nrand)
   if(RA.size>nrand):
      idx = np.random.choice(np.arange(RA.size), nrand, replace=False)
      #subsampled RA,DEC and Red
      RA=RA[idx]; DEC=DEC[idx];Red=Red[idx] 
   print('RED:',np.min(Red) , np.max(Red), np.mean(Red))

   XYZ,interp=mcosm.RDZ2XYZ(RA,DEC,Red,H0,omM,omL,interp='')
   rand=np.column_stack([XYZ,np.ones(XYZ.shape[0])])
   if(args.njn>0):
      JN_reg=GF.compute_JN_data(RA,DEC,JNfile)
      rand=np.column_stack([rand,JN_reg])


   return data,rand


#to combine the min and max extent of data for two sample
def combine_survey(POS1_min,POS1_max,blen1,POS2_min,POS2_max,blen2):
   diffmin=np.zeros(3); diffmax=np.zeros(3)
   for ii in range(0,3):
      diffmin[ii]=np.abs(POS1_min[ii]-POS2_min[ii])
      diffmax[ii]=np.abs(POS1_max[ii]-POS2_max[ii])

      POS1_min[ii]=min(POS1_min[ii],POS2_min[ii])
      POS1_max[ii]=max(POS1_max[ii],POS2_max[ii])

      #compute the blen
      blen1[ii]=POS1_max[ii]-POS1_min[ii]

   if(args.interactive>1):
      print("Extent of the data/random combined:")
      print("MIN: ",', '.join('%8.2f'%x for x in POS1_min) )
      print("MAX: ",', '.join('%8.2f'%x for x in POS1_max) )
      print("Length: ",', '.join('%8.2f'%x for x in blen1),'\n' )

   return POS1_min,POS1_max,blen1


def GetXYZ(fH,indsel,args):
    '''Load the appropriate xyz after needed transforms'''
   
    #This is for simple case
    if(args.coord=='xyz'):
        xyz=np.column_stack([fH['fits'][1]['xx'][indsel],fH['fits'][1]['yy'][indsel],fH['fits'][1]['zz'][indsel]])
    elif(args.coord=='sky'):
        xyz, interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indsel],
            fH['fits'][1]['DEC'][indsel],fH['fits'][1]['Z'][indsel]
            ,args.H0,args.omM,1.0-args.omM,interp='')
    else:
        print("Invalid coord: ",args.coord)
        print("Exiting!!")
        sys.exit()

    return xyz

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    weights=fH['fits'][1]['wt'][indsel]
    return weights

def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in IPdict'''
    jnreg=fH['fits'][1]['jn'][indsel]
    return jnreg


def getminmax_fits(fH,fR,args):
   '''This loads the data in chunks to calculate minmax
   doesn't load more than num_load points at a time where num_load is an input number'''

   #Declare variables
   blen  = np.array([0,0,0],dtype='double')
   POS_min = np.array([0,0,0],dtype='double')
   POS_max = np.array([0,0,0],dtype='double')

   #get initial min and max from data file
   ngal1=min(fH['isel'].size,args.num_load)
   xyz=args.GetXYZ(fH,fH['isel'][:ngal1],args)
   tmin=np.min(xyz,axis=0)
   tmax=np.max(xyz,axis=0)

   for ii in range(0,3):
       POS_min[ii]=tmin[ii] 
       POS_max[ii]=tmax[ii] 

   #min max of data
   ngal2=ngal1
   while(ngal2<fH['isel'].size):
       nchunk=min(fH['isel'].size-ngal1,args.num_load)
       ngal2=ngal1+nchunk
       xyz=args.GetXYZ(fH,fH['isel'][ngal1:ngal2],args)
       tmin=np.min(xyz,axis=0)
       tmax=np.max(xyz,axis=0)
       for ii in range(0,3):
           if(POS_min[ii]>tmin[ii]):
               POS_min[ii]=tmin[ii] 
           if(POS_max[ii]<tmax[ii]):
               POS_max[ii]=tmax[ii]
       ngal1=ngal2 
   
   #min max of randoms
   nrand1=0; nrand2=0
   while(nrand2<fR['isel'].size):
       nchunk=min(fR['isel'].size-nrand1,args.num_load)
       nrand2=nrand1+nchunk
       xyz=args.GetXYZ(fR,fR['isel'][nrand1:nrand2],args)
       tmin=np.min(xyz,axis=0)
       tmax=np.max(xyz,axis=0)
       for ii in range(0,3):
           if(POS_min[ii]>tmin[ii]):
               POS_min[ii]=tmin[ii] 
           if(POS_max[ii]<tmax[ii]):
               POS_max[ii]=tmax[ii]
       nrand1=nrand2 


   for ii in range(0,3):
       blen[ii]=POS_max[ii]-POS_min[ii]

   return POS_min,POS_max, blen


def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''
    findat=F.FITS(datafile)
    indsel_dat = findat[1].where('1>0') #select everything now
    
    finran=F.FITS(randfile)
    indsel_ran = finran[1].where('1>0') #select everything now

    return findat,indsel_dat,finran,indsel_ran


def GetSumWt(fH,args):
   weights=args.GetWeights(fH,fH['isel'])
   #compute sum of weights
   if(args.njn==0):
       sumwt=np.sum(weights)
   else:
      JN_reg=args.GetJNreg(fH,fH['isel'],args)
      #JN_reg=GF.compute_JN_data(fin[1]['RA'][indsel],
      #                   fin[1]['DEC'][indsel],[JNfile])
      sumwt=np.zeros(args.njn+1)
      #all weight
      sumwt[args.njn]=np.sum(weights)
      #jn weights
      for ii in range(0,args.njn):
         ind=JN_reg==ii
         sumwt[ii]= sumwt[args.njn]-np.sum(weights[ind])
   return sumwt


def pip_ang_scale_pair_weight(args,pair_need=[],fmap={}):
    '''Calculates the DD and DR in angular bin with pip weight and with iip weight for scaling the iip weight
    This is to avoid calculating the sum of pip weight over all pairs which will be very slow
    An internal convergence test is performed and warnings are printed if it fails'''

    xc_dic={}

    if(args.pip==False and args.ang_up==False):
        for pp, pair in enumerate(pair_need):
            if(args.njn==0):
                xc_dic['scale_sum%s'%pair]=1.0
            else:
                xc_dic['scale_sum%s'%pair]=np.ones(args.njn+1)

        return xc_dic

    theta_lim_c =np.ascontiguousarray(np.array([0,args.theta_max,0,1]),dtype='double')
    nbins_theta_c =np.ascontiguousarray(np.array([25,1]),dtype='int')
    pos_min_c=np.ascontiguousarray(np.array([0,0,0]),dtype='double')
    blen_c =np.ascontiguousarray(np.array([0,0,0]),dtype='double')

    tmp_dic={'pip':False,'iip':False,'ang_up':False,'sampmode':0}
    for tkey in tmp_dic.keys():
        tmp_dic[tkey]=args.__dict__[tkey]



    for wtag in ['pair','individual']:
        xc_dic[wtag]={}

        #update args values necessary
        if(wtag=='pair'):
            args.sampmode=6
        elif(wtag=='individual'):
            args.pip=False; args.ang_up=False;  args.sampmode=6;
            if(tmp_dic['pip']==True):
                args.iip=True
            
        for pp, pair in enumerate(pair_need):
            #compute DD with pip and iip upto thetamax
            if(tmp_dic['ang_up']==False and pair in ['DR','D1R2','R1D2']):
                continue

            if(args.interactive>1):
                print('For Sumwt: %s %s...'%(wtag,pair))
            xc_dic[wtag][pair],nd1,nd2=corrutility.mp_pair_count(fmap[pair][0], fmap[pair][1], 
                    theta_lim_c, nbins_theta_c, nhocells, blen_c, pos_min_c,args)


    #changes the args to initial values
    for tkey in tmp_dic.keys():
        args.__dict__[tkey]=tmp_dic[tkey]
    #write these to a file and test for ratio convergence

    #import pylab as pl
    #estimate the cumulative ratio
    xc_dic['cum_ratio']={}
    for pp, ptype in enumerate(pair_need):
        if(args.ang_up==False and ptype in ['DR','D1R2','R1D2']):
            if(args.njn==0):
                xc_dic['scale_sum%s'%ptype]=1.0
            else:
                xc_dic['scale_sum%s'%ptype]=np.ones(args.njn+1)
            continue

        if(args.njn==0):
            xc_dic['cum_ratio'][ptype]=np.cumsum(xc_dic['pair'][ptype].flatten())/np.cumsum(xc_dic['individual'][ptype].flatten())
            xc_dic['scale_sum%s'%ptype]=xc_dic['cum_ratio'][ptype][-1]
        else:
            xc_dic['cum_ratio'][ptype]=np.zeros((xc_dic['pair'][ptype].shape[0],args.njn+1))
            xc_dic['scale_sum%s'%ptype]=np.zeros(args.njn+1)
            #pl.figure(pp)
            for ii in range(0,args.njn+1):
                xc_dic['cum_ratio'][ptype][:,ii]=np.cumsum(xc_dic['pair'][ptype][:,:,ii].flatten())/np.cumsum(xc_dic['individual'][ptype][:,:,ii].flatten())
                xc_dic['scale_sum%s'%ptype][ii]=xc_dic['cum_ratio'][ptype][-1,ii]
                #pl.plot(xc_dic['cum_ratio'][ptype][:,ii])
                #pl.plot(25,xc_dic['scale_sum%s'%ptype][ii],'kx')
            #pl.plot(xc_dic['cum_ratio'][ptype][:,args.njn],'k--')


        #check if the scaling is saturated of not for the full map with jn
        prog_scale=np.abs(xc_dic['cum_ratio'][ptype][2:,args.njn]-xc_dic['cum_ratio'][ptype][1:-1,args.njn])
        if(prog_scale[-1]>1e-4): #This means this scaling hasn't converged
            msg='The scaling to account for pair weighted normalization does not seems to be converge yet for ptype=%s'%ptype
            msg=msg+'\nPlease check the cumulative angular ratio file for issue'
            msg=msg+'\n This might be due to theta_max not being large enough and hence increasing theta_max but might take more time'''
            msg=msg+'\n Ideally keep theta_max such that the pair probability is same as product of individual probability'''
            msg=msg+'\n Current theta_max = %10.6f'%args.theta_max
            msg=msg+'\n Below printing the cum_ratio with theta'
            msg=msg+'\n  theta_max(in radian) ratio of cumulative DD with pair and individual'
            theta=np.linspace(0,args.theta_max,nbins_theta_c[0]+1)
            for ii in range(0,xc_dic['cum_ratio'][ptype][:,args.njn].size):
                msg=msg+'\n\t %10.6f %10.6f'%(theta[ii+1],xc_dic['cum_ratio'][ptype][ii,args.njn])
            print('****** Warning (pip %s scaling issue)**********\n'%ptype+msg+'\n***********Warning (Exiting)*********')
            if(args.nocheck_pairnorm==False):
                sys.exit()
    #save the normalization in a file
    norm_file=args.outfile+'-pair_normalization_approx.txt'
    theta=np.linspace(0,args.theta_max,nbins_theta_c[0]+1)
    norm_mat=None; hdr=''
    for pp, ptype in enumerate(pair_need):
        if(ptype in xc_dic['cum_ratio'].keys()):
            norm_this=xc_dic['cum_ratio'][ptype][:,args.njn]
            if(norm_mat is None):
                norm_mat=np.column_stack([theta[1:],norm_this])
                hdr=hdr+'theta_lim, cum_ratio_%s'%ptype
            else:
                norm_mat=np.column_stack([norm_mat,norm_this])
                hdr='%s, %s'%(hdr,ptype)

    np.savetxt(norm_file,norm_mat,header=hdr)
    print('Written normalization approx:',norm_file)
    
    #pl.show()

    return xc_dic

def GetSumwt_pair(args,fH,fR,fH2=None,fR2=None):
    '''This is to get pair weights for data and random with pip
       first get the data and random for DR and RR pair and then estimate the weights
    '''

    if(fH2 is None):
        single_need=['D','R']; smap={'D':fH,'R':fR}
        pair_need=['DD','DR']
        fmap={'DD':[fH,fH],'DR':[fH,fR],'RR':[fR,fR]}
        norm_need={'DD':['D','D'],'DR':['D','R'],'RR':['R','R']}
    else:
        single_need=['D1','R1','D2','R2']; smap={'D1':fH,'D2':fH2,'R1':fR,'R2':fR2}
        pair_need=['D1D2','D1R2']
        if(args.noRD==False):
            pair_need=pair_need+['R1D2']
        fmap={'D1D2':[fH,fH2],'D1R2':[fH,fR2],'R1D2':[fR,fH2]}
        norm_need={'D1D2':['D1','D2'],'D1R2':['D1','R2']}
        if(args.noRD==False):
            norm_need['R1D2']=['R1','D2']
        if(args.noRR==False):
            norm_need['R1R2']=['R1','R2']
       

    xc_dic=pip_ang_scale_pair_weight(args,pair_need=pair_need,fmap=fmap)
 
    #compute iip weight sum for data
    wt_dic={}
    jn_reg={}
    for cc, cat in enumerate(single_need):
        #populate weights array
        if(cat in ['D','D1','D2']):       
            #get the data and weights including iip
            if(args.pip+args.iip==True):
                data1_LC, fib1_LC=args.LPfunc(smap[cat],0,smap[cat]['isel'].size,args)
            else: #only angular up-weight
                data1_LC =args.LPfunc(smap[cat],0,smap[cat]['isel'].size,args)
                fib1_LC=np.array([])

            nattr=data1_LC.shape[1]
            fp1=0; np1=data1_LC.shape[0]

            if(fib1_LC.size!=0):
                nfib_int=fib1_LC.shape[1]
                #This multily the galaxy weight with its iip
                ccorr.get_iip(data1_LC, nattr, fp1, np1, fib1_LC, nfib_int, args.bit_perlong)
            wt_dic[cat]=data1_LC[:,3]
            if(args.njn>0):
                jn_reg[cat]=data1_LC[:,4]
        else:
            #random weights going in DR count
            wt_dic[cat]=args.GetWeights(smap[cat],smap[cat]['isel'])
            if(args.njn>0):
                jn_reg[cat]=args.GetJNreg(smap[cat],smap[cat]['isel'],args)

    #now estimate the weight sum and normalization
    norm_dic={}
    for nn,norm in enumerate(norm_need.keys()):
        if(norm in ['RR','R1R2'] and 'scale_sum%s'%norm not in xc_dic.keys()):
            if(args.njn==0):
                xc_dic['scale_sum%s'%norm]=1.0
            else:
                xc_dic['scale_sum%s'%norm]=np.ones(args.njn+1)

        cat1=norm_need[norm][0] #The two catalogue for this pair norm
        cat2=norm_need[norm][1]
        
        #sum of all the weights
        sum1=np.sum(wt_dic[cat1])
        sum2=np.sum(wt_dic[cat2])

        if(args.njn==0):
            norm_dic[norm+'norm']=sum1*sum2*xc_dic['scale_sum%s'%norm]
        else:
            norm_dic[norm+'norm']=np.zeros(args.njn+1)
            #all weight
            norm_dic[norm+'norm'][args.njn]=sum1*sum2*xc_dic['scale_sum%s'%norm][args.njn]
            for ii in range(0,args.njn):
                ind1=jn_reg[cat1]==ii
                ind2=jn_reg[cat2]==ii
                sum1_jn=sum1-np.sum(wt_dic[cat1][ind1])
                sum2_jn=sum2-np.sum(wt_dic[cat2][ind2])
                norm_dic[norm+'norm'][ii]=sum1_jn*sum2_jn*xc_dic['scale_sum%s'%norm][args.njn]

    print('norm_dic:',norm_dic.keys())
    return norm_dic

def GetSumwt_pair_old(args,fH,fR,fH2=None,fR2=None):
    '''This is to get pair weights for data and random with pip
       first get the data and random for DR and RR pair and then estimate the weights
    '''

    if(fH2 is None):
        pair_need=['DD','DR']
        fmap={'DD':[fH,fH],'DR':[fH,fR]}
    else:
        pair_need=['D1D2','D1R2','R1D2']
        fmap={'D1D2':[fH,fH2],'D1R2':[fH,fR2],'R1D2':[fR,fH2]}

    xc_dic=pip_ang_scale_pair_weight(args,pair_need=pair_need,fmap=fmap)

    #compute iip weight sum for data

    #populate weights array
    #get the data and weights including iip
    if(args.pip+args.iip==True):
        data1_LC, fib1_LC=args.LPfunc(fH,0,fH['isel'].size,args)
    else: #only angular up-weight
        data1_LC =args.LPfunc(fH,0,fH['isel'].size,args)
        fib1_LC=np.array([])
    
    nattr=data1_LC.shape[1]
    fp1=0; np1=data1_LC.shape[0]

    if(fib1_LC.size!=0):
        nfib_int=fib1_LC.shape[1]
        #test
        #np.savetxt('test-before.txt',data1_LC)
        #This multily the galaxy weight with its iip
        ccorr.get_iip(data1_LC, nattr, fp1, np1, fib1_LC, nfib_int, args.bit_perlong)
        #test
        #np.savetxt('test-after.txt',data1_LC)

    #random weights going in DR count
    fR['pair_type']='DR'
    Rwt_DR=args.GetWeights(fR,fR['isel'])
    
    #random weights going in RR count
    fR['pair_type']='RR'
    Rwt_RR=args.GetWeights(fR,fR['isel'])
    
    #now estimate the weight sum and normalization
    if(args.njn==0):
        sumD=np.sum(data1_LC[:,3]) #assumes third column is weight also include iip
        sumR_DR=np.sum(Rwt_DR)
        sumR_RR=np.sum(Rwt_RR)
        DDnorm=sumD*sumD*xc_dic['scale_sumDD']; 
        DRnorm=sumD*sumR_DR*xc_dic['scale_sumDR']; 
        RRnorm=np.power(sumR_RR,2)
    else:
      JN_reg=args.GetJNreg(fR,fR['isel'],args)
      DDnorm=np.zeros(args.njn+1)
      DRnorm=np.zeros(args.njn+1)
      RRnorm=np.zeros(args.njn+1)
      
      #all weight
      sumD=np.sum(data1_LC[:,3]); sumR_DR=np.sum(Rwt_DR); sumR_RR=np.sum(Rwt_RR)
      #DDnorm[args.njn]=1; DRnorm[args.njn]=sumD/sumR_DR; RRnorm[args.njn]=np.power(sumD/sumR_RR,2)
      DDnorm[args.njn]=sumD*sumD*xc_dic['scale_sumDD'][args.njn]; 
      DRnorm[args.njn]=sumD*sumR_DR*xc_dic['scale_sumDR'][args.njn]; 
      RRnorm[args.njn]=sumR_RR*sumR_RR
      #jn weights
      for ii in range(0,args.njn):
         ind_D=data1_LC[:,4]==ii
         ind_R=JN_reg==ii
         sumD_jn=np.sum(data1_LC[~ind_D,3]); 
         sumR_DR_jn=sumR_DR-np.sum(Rwt_DR[ind_R]); 
         sumR_RR_jn=sumR_RR-np.sum(Rwt_RR[ind_R])
         #DDnorm[ii]=1; DRnorm[ii]=sumD_jn/sumR_DR_jn; RRnorm[ii]=np.power(sumD_jn/sumR_RR_jn,2)
         DDnorm[ii]=sumD_jn*sumD_jn*xc_dic['scale_sumDD'][args.njn]; 
         DRnorm[ii]=sumD_jn*sumR_DR_jn*xc_dic['scale_sumDR'][args.njn]; RRnorm[ii]=sumR_RR_jn*sumR_RR_jn

    return DDnorm, DRnorm, RRnorm



def FITS_prep_data_rand(datafile,randfile,IPdict,args):
    '''This is generic Fits hanler put special details in the special functions'''
    t0=time.time()
    if(args.interactive>1):
        print("Loading files:\n "+datafile+" \n "+randfile)

    if(args.filetype=='fits'):
        #data,rand=readSKYwithifits(datafile,randfile)
        findat,indsel_dat,finran,indsel_ran =args.FITS_Selection(datafile,randfile,args,IPdict)
    else:
        print('unknown file type')
        sys.exit()

    ngal =int(indsel_dat.size)
    nrand=int(indsel_ran.size)
    if(args.interactive>=0):
        print("%10d galaxies selected from %s"%(ngal,datafile))
        print("%10d randoms selected from %s"%(nrand,randfile))
        print("\nTime:%d sec"%int(time.time() - t0))

    #make dictionaries
    fHD={'fits':findat,'isel':indsel_dat,'typein':'data','pair_type':'DD',
            'prop':IPdict,'fname':datafile} #prop can contain information for selection etc
    fHR={'fits':finran,'isel':indsel_ran,'typein':'rand','pair_type':'RR',
            'prop':IPdict,'fname':randfile} #prop can contain information for selection etc

    

    #POS_min,POS_max, blen=getminmax(data,rand=rand)
    POS_min,POS_max, blen=getminmax_fits(fHD, fHR,args)

    if(args.interactive>1):
        print("Extent of the data/random:")
        print("MIN: ",', '.join('%8.2f'%x for x in POS_min) )
        print("MAX: ",', '.join('%8.2f'%x for x in POS_max) )
        print("Length: ",', '.join('%8.2f'%x for x in blen),'\n' )


    return fHD, fHR, blen, POS_min, POS_max

def LoadData_part(fH,I1,I2,args):
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]
   #read co-ordinate and redshift
   data=np.column_stack([ fH['fits'][1]['xx'][indcut],
      fH['fits'][1]['yy'][indcut],fH['fits'][1]['zz'][indcut]])

   #if(args.mode=='sim'):#in case of sim multiply by Lbox to get in Mpc/h
   #   data=data*args.Lbox
   #set weights =1 for now
   weights=args.GetWeights(fH,indcut)
   data=np.column_stack([data,weights])


   if(args.njn>0):
      JN_reg=args.GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   return  data_c


if __name__=="__main__":
   #This dictionary store all the sume of wts for marginalization passed to be written
   sumwt_dic={}  
   ndata_min=0 #minimum number of data points to decide nhocells
   #load and prepare the data 
   if(args.filetype!='fits'):
       data_c, rand_c, blen, POS_min, POS_max, sumwt_dic['SDwt'], sumwt_dic['SRwt']=GFS.prep_data_rand(args.data,args.rand,args,RSD=args.RSD)
       ndata_min=data_c.shape[0]
       if(args.data2!='' and args.rand2!=''):
           data2_c, rand2_c, blen2, POS2_min, POS2_max, sumwt_dic['SDwt2'],sumwt_dic['SRwt2'] =GFS.prep_data_rand(args.data2,args.rand2,args,RSD=args.RSD)
           POS_min,POS_max,blen=combine_survey(POS_min,POS_max,blen,POS2_min,POS2_max,blen2)
           if(ndata_min>data2_c.shape[0]):
               ndata_min=data2_c.shape[0]


   else: #How to handle FITS
       file_path = os.path.realpath(__file__)
       my_path='/'.join(file_path.split('/')[:-1])+'/'

       print('my_path:', my_path)
       #This is to pass LoadData_part function to ccorr_utility
       if(args.catname!=None):
           module_file='catlib/%s.py'%(args.catname)
           if(os.path.isfile(my_path+module_file)):
              if(my_path not in sys.path):
                  sys.paths=[my_path]+sys.path

              catmodule=importlib.import_module("catlib.%s"%(args.catname))
              args.FITS_Selection=catmodule.FITS_Selection
              args.LPfunc=catmodule.LoadData_part
              args.GetJNreg=catmodule.GetJNreg
              args.GetWeights=catmodule.GetWeights
              if(hasattr(catmodule, 'GetXYZ')):
                  args.GetXYZ=catmodule.GetXYZ

              print('''*** Using functions from module %s
              \t*for selection: %s, 
              \t*for load parts: %s
              \t*for Weights: %s
              \t*for JNreg: %s\n'''%(catmodule,
                  args.FITS_Selection,args.LPfunc,
                  args.GetWeights,args.GetJNreg))
           else:
              print('Module %s not found, using default functions'%(module_file))  
      

       if(args.FITS_Selection==None):
           args.FITS_Selection=FITS_Selection
           args.GetJNreg=GetJNreg
           args.GetWeights=GetWeights

       if(args.LPfunc==None):
           args.LPfunc=LoadData_part

       if(args.GetXYZ==None):
           args.GetXYZ=GetXYZ
        
       #function for data selection

       IPdict={} #input dictionary
       if(args.catname!=None and args.catname in args.__dict__.keys()):
           IPdict=args.__dict__[args.catname]

       fHD1,fHR1,blen, POS_min, POS_max =FITS_prep_data_rand(args.data,args.rand,IPdict,args)
       ndata_min=fHD1['isel'].size

       if(args.data2=='' and args.rand2=='' and 0):# len(args.selection)==2):
           fHD2, fHR2,blen2, POS2_min, POS2_max =FITS_prep_data_rand(args.data,args.rand,IPdict,args)
           POS_min,POS_max,blen=combine_survey(POS_min,POS_max,blen,POS2_min,POS2_max,blen2)
           #Get the weights
           sumwt_dic['SDwt2']=GetSumWt(fHD2,args)
           sumwt_dic['SRwt2']=GetSumWt(fHR2,args)
           if(ndata_min>fHD2['isel'].size):
               ndata_min=fHD2['isel'].size
       elif(args.data2!='' or args.rand2!=''):
           IPdict={} #input dictionary
           if(args.catname!=None and args.catname+'2' in args.__dict__.keys()):
               IPdict=args.__dict__[args.catname+'2']

           fHD2, fHR2,blen2, POS2_min, POS2_max =FITS_prep_data_rand(args.data2,args.rand2,IPdict,args)
           POS_min,POS_max,blen=combine_survey(POS_min,POS_max,blen,POS2_min,POS2_max,blen2)
           if(ndata_min>fHD2['isel'].size):
               ndata_min=fHD2['isel'].size
       else:
           fHD2=None;fHR2=None

       #Get the sum weights for normalizations
       if(args.pip==True or args.iip==True or args.ang_up==True):
           norm_dic=GetSumwt_pair(args,fHD1,fHR1,fH2=fHD2,fR2=fHR2)
           for tt,tkey in enumerate(norm_dic.keys()):
               sumwt_dic[tkey]=norm_dic[tkey]
       else:
           sumwt_dic['SDwt']=GetSumWt(fHD1,args)
           sumwt_dic['SRwt']=GetSumWt(fHR1,args)
           if(fHD2!=None):
               #Get the weights
               sumwt_dic['SDwt2']=GetSumWt(fHD2,args)
               sumwt_dic['SRwt2']=GetSumWt(fHR2,args)


   rlim_c =np.ascontiguousarray(np.array(args.samplim,dtype='double'))
   blen_c =np.ascontiguousarray(blen)
   pos_min_c=np.ascontiguousarray(POS_min)


   if(args.interactive>=0):
      print("\nPrepared data and random:%d sec"%int(time.time() - t0))

  

   if(ndata_min/np.power(nhocells,3)<0.05):
      nhocells=int(np.power(ndata_min/0.05,0.33))
      print('Updating nhocells to : %d, because ndata_min=%d'%(nhocells,ndata_min)) 

   if(args.data2==''):
       if(args.filetype!='fits'):
           data_c.shape[0]/np.power(nhocells,3)
           corrutility.compute_auto(data_c, rand_c, rlim_c,
                               args.nbins, blen_c, pos_min_c,nhocells,args,sumwt_dic)

           #corrutility.compute_auto(data_c, rand_c, rlim_c, args.nbins, blen_c, pos_min_c,
           #  args.sampmode, args.njn,args.nproc, args.pbc, args.los, nhocells,args.rand, 
           #  args.interactive, args.nnode, args.nodeid ,
           #  args.noDD, args.noDR, args.noRD,args.noRR, args.filetype, sumwt_dic)
       else:
           corrutility.compute_auto(fHD1, fHR1,rlim_c, 
                               args.nbins, blen_c, pos_min_c,nhocells,args,sumwt_dic)
   else:
       if(args.filetype!='fits'):
           corrutility.compute_cross(data_c, rand_c,data2_c, rand2_c,
                             rlim_c, args.nbins, blen_c, pos_min_c,nhocells,args,sumwt_dic)
       else:
           corrutility.compute_cross(fHD1, fHR1,fHD2,fHR2, rlim_c, 
                               args.nbins, blen_c, pos_min_c,nhocells,args,sumwt_dic)


   #convert paircount to xi
   if(args.nnode==1):
      pcxi.main_call(args)
   else:
      print('only paircounts are calculated, please run PairCountTOxi to calculate correlation function after all nodes are finished')


