#include "corr2dc.h"
#include <assert.h>
#include <stdint.h>

//Counts the number of ones in binary representation of integer
size_t countOnes (long n)
{
    size_t count=0;
    while (n!=0)
    {
        n = n & (n-1);
        count++;
    }
    return count;
}


int cwrap(int x, int y) {

    // Cell/box wrap, required by periodic boundary conditions.
    //
    // Parameters:
    // x : the number to be wrapped.
    // y : the maximum/boundary of x.
    //
    // Returns:
    // modulo : the number insides the boundary.

    int modulo = x;
    if (x >= y) modulo = x - y;
    if (x < 0)  modulo = x + y;
    return modulo;
}

int relwrap(double *dxyz, double *blen) {

    // Relative wrap, required by periodic boundary conditions.
    //
    // Parameters:
    // dxyz : the number to be wrapped.
    // blen : the maximum/boundary of x.
    //
    // Returns:
    // modulo : the number insides the boundary.

    int ii;
    
    for(ii=0; ii<3; ii++) {
       if (dxyz[ii] >= blen[ii]/2) dxyz[ii] = dxyz[ii]-blen[ii];
       if (dxyz[ii] < -blen[ii]/2) dxyz[ii] = dxyz[ii]+blen[ii];
    }
    return 1;
}

void  maxradius(double *rlim , int samp, double *maxrad) {

   if(samp==0 || samp==2) { //rmu, rtheta
       maxrad[0]=rlim[1];
       maxrad[1]=maxrad[0]; maxrad[2]=maxrad[0];
   } else if (samp==1) { //rper_rpar
       double y;
       if(fabs(rlim[2])>fabs(rlim[3])) y=rlim[2];
       else y=rlim[3]; 
       maxrad[0]=sqrt(rlim[1]*rlim[1]+y*y);
       maxrad[1]=maxrad[0]; maxrad[2]=maxrad[0];
   } else if (samp==3) { //log(rper)-rpar
       maxrad[0]=sqrt(pow(10,rlim[1]*2)+rlim[3]*rlim[3]);
       maxrad[1]=maxrad[0]; maxrad[2]=maxrad[0];
   } else if (samp==4 || samp==5) { //4: log(r)-theta, 5:log(r)-mu
       maxrad[0]=pow(10,rlim[1]);
       maxrad[1]=maxrad[0]; maxrad[2]=maxrad[0];
   } else if (samp==6 || samp==7) { //6: theta in radian
       maxrad[1]=rlim[1];
       if(samp==7) maxrad[1]=pow(10,maxrad[1]); //for log scale we need linear theta max
       maxrad[2]=maxrad[1];
       maxrad[0]=0; //rr co-ordinate set to zero 
   } else {
      printf("\n****bad sampmode specified , samp=%d ****\n",samp);
      maxrad[0]=-1;maxrad[1]=-1;maxrad[2]=-1;
      exit(0);
   }

}

void init_mesh(long *ll, long *hoc, double *p, long fp, long np, int nattr, int nho, double *blen,
      double *posmin){

    // Initialize the space. Mesh the particles in p into cells.
    //
    // Parameters:
    // ll : (np,) array, linked list of particles.
    // hoc : (nhocells, nhocells, nhocells) array, mesh of particles.
    // p : (np, nattr) array, particles.
    // np : number of particles.
    // nattr: number of features of particles.
    // nho : number of cells in one dimension.
    // blen : box length in 3d or the extent of survey.
    // posmin: lower extent of the survey

    long i;
    int ix, iy, iz, ii, jj, kk;

    // Initiate hoc to -1 for not missing the last point
    for (ii = 0; ii < nho; ii++)
        for (jj = 0; jj < nho; jj++)
            for (kk = 0; kk < nho; kk++)
                hoc[ii*nho*nho+jj*nho+kk] = -1;

    for (i = fp; i < np; i++) {
        ix = floor((p[i*nattr] - posmin[0]) / blen[0] * nho);
        iy = floor((p[i*nattr+1]-posmin[1]) / blen[1] * nho);
        iz = floor((p[i*nattr+2]-posmin[2]) / blen[2] * nho);

	     if(ix==nho) ix=ix-1;
	     if(iy==nho) iy=iy-1;
	     if(iz==nho) iz=iz-1;

        ll[i] = hoc[ix*nho*nho+iy*nho+iz];
        hoc[ix*nho*nho+iy*nho+iz] = i;
    }
}


void cartesian_to_spherical_polar(double *sph, double xx, double yy, double zz){
    //converts xyz to spherical polar co-ordinate
    sph[0]=sqrt(xx*xx+yy*yy+zz*zz);
    sph[1]=acos(zz/sph[0]);
    sph[2]=atan2(yy,xx);
} 

void cartesian_to_spherical_polar_array( double *sph, double *blen, double *posmin, double *p, int nattr, long fp, long np) {
   //converts cartesian to spherical polar coordinate
    
   double *posmax = (double *)calloc(3, sizeof(double));
   double *sph_tmp = (double *)calloc(3, sizeof(double));

   int nsph_col=3;

   //initialize posmin and posmax
   for (int jj=0; jj<3; jj++){
      posmin[jj]=100000;
      posmax[jj]=-100000;
   }

   for (long ii = fp; ii < np; ii++) {
      long tt=ii*nattr;

      //convert this ixyz to sph_polar
      cartesian_to_spherical_polar(sph_tmp, p[tt],p[tt+1],p[tt+2]);
    

      long ss=(ii-fp)*nsph_col;
      //transfer r, theta and phi
      for (int jj=0; jj<3; jj++)
          sph[ss+jj]=sph_tmp[jj];
      
      //update min max r, theta, phi
      for (int jj=0; jj<3; jj++){
            if(sph[ss+jj]< posmin[jj]) posmin[jj]=sph[ss+jj];
            else if (sph[ss+jj]> posmax[jj]) posmax[jj]=sph[ss+jj];

      }

   }

   for (int jj=0; jj<3; jj++){
      blen[jj]=posmax[jj]-posmin[jj];
   }

   free(posmax);
   free(sph_tmp);
}

void init_mesh_angular(long *ll, long *hoc, double *sph, double *blen, double *posmin, double *p, long fp, long np, int nattr, int nho){
    // Initialize the space. Mesh the particles in p into cells.
    // First convert the cartesian co-ordinates to polar and then use angles to make the cells
    // Parameters:
    // ll : (np,) array, linked list of particles.
    // hoc : (nhocells, nhocells, nhocells) array, mesh of particles.
    // p : (np, nattr) array, particles.
    // np : number of particles.
    // nattr: number of features of particles.
    // nho : number of cells in one dimension.
    // blen : box length in 3d or the extent of survey.
    // posmin: lower extent of the survey

    long i,ss;
    int ix, iy, ii, jj;
    

    // get the polar co-orpdinate updated in theta and phi
    cartesian_to_spherical_polar_array( sph, blen, posmin, p, nattr, fp, np);
    

    // Initiate hoc to -1 for not missing the last point
    for (ii = 0; ii < nho; ii++)
        for (jj = 0; jj < nho; jj++)
            hoc[ii*nho+jj] = -1;

    for (i = fp; i < np; i++) {
        ss=(i-fp)*3;
        ix = floor((sph[ss+1] - posmin[1]) / blen[1] * nho);
        iy = floor((sph[ss+2] - posmin[2]) / blen[2] * nho);

	     if(ix==nho) ix=ix-1;
	     if(iy==nho) iy=iy-1;

        ll[i] = hoc[ix*nho+iy];
        hoc[ix*nho+iy] = i;
    }
    
}

void corr2d(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, 
      double *rlim, int nbins0, int nbins1, int nhocells, double *blen, 
      double *posmin, int samp, int njn, int pbc, int los, int interactive) {

    // 2d correlation function.
    //
    // Parameters:
    // xc_2d : (nbins, nbins) array, 2d correlation to be returned.
    // p1 : (np1,6/7) first group of particles.
    // np1: number of particles in first group.
    // p2 : (np2,6/7) second group of particles.
    // np2 : number of particles of the second group.
    // rlim[4] : the minimum,maximum range you are looking at in both axis.
    // nbins0,1: number of bins in both axis.
    // nhocells: number of cells.
    // blen[3]: box size in 3d or the extent of data.
    // posmin[3]: minimum value of each co-ordinate (minimum corner of box)
    // samp : to decide the sampling scheme of the correlation function
    // njn : (0/njn) to decide if jacknife subsample needs to be evaluated or not
    // pc : (0/1) to decide periodic boundary condition or not

    long iq, i;
    int pp, qq, rr, p, q, r, ix, iy, iz; //To run loops on grids and particles
    int ppmin, ppmax, qqmin, qqmax, rrmin, rrmax; //To figure of the range of grid span
    int indx , indy; //To store the index of correlation function fbin
    int check, nattr;

    int nbins[2];
    nbins[0]=nbins0; nbins[1]=nbins1;
    //Allocate some memory
    //int *ncb = (int *)calloc(3, sizeof(int));
    //double *xyzp1 = (double *)calloc(3, sizeof(double));
    //double *xyzp2 = (double *)calloc(3, sizeof(double));
   
    int ncb[3], jn1,jn2,xcind;
    double xyzp1[3], xyzp2[3], weight12;
    long nvalid_pair=0, ninvalid_pair=0;

    double *maxrad=(double *)calloc(3, sizeof(double));
    maxradius(rlim ,samp,maxrad);

    for (pp=0; pp<3;pp++)
      ncb[pp] = ceil((maxrad[pp] / blen[pp]) * (double)(nhocells)) + 1;
      //ncb[pp] = floor((maxrad / blen[pp]) * (double)(nhocells)) + 1;

    long *ll = (long *)calloc(np2, sizeof(long));
    long *hoc = (long *)calloc(nhocells*nhocells*nhocells, sizeof(long));

    // Add weight or not
    if (njn > 0)  nattr =5 ;
    else  nattr = 4;

    // initialization
    init_mesh(ll, hoc, p2, fp2, np2, nattr, nhocells, blen, posmin);

    for (iq = fp1; iq < np1; iq++) {
        if (iq % 100000 == 0 && interactive>=0) printf(".");

        //read in first particle co-ordinate
        xyzp1[0] = p1[iq*nattr]; xyzp1[1] = p1[iq*nattr+1]; xyzp1[2] = p1[iq*nattr+2];

        ix = floor((xyzp1[0]-posmin[0]) / blen[0] * nhocells);
        iy = floor((xyzp1[1]-posmin[1]) / blen[1] * nhocells);
        iz = floor((xyzp1[2]-posmin[2]) / blen[2] * nhocells);

        
	ppmin=ix-ncb[0]; ppmax=ix+ncb[0];
	qqmin=iy-ncb[1]; qqmax=iy+ncb[1];
	rrmin=iz-ncb[2]; rrmax=iz+ncb[2];
	
	if(pbc!=1) { //pbc can be applied only with los=1 along z axis
           if(ppmin<0) ppmin=0;
           if(qqmin<0) qqmin=0;
           if(rrmin<0) rrmin=0;

           if(ppmax>nhocells) ppmax=nhocells;
           if(qqmax>nhocells) qqmax=nhocells;
           if(rrmax>nhocells) rrmax=nhocells;
	}

        for (pp = ppmin; pp < ppmax; pp++) {
            p = cwrap(pp, nhocells);
            for (qq = qqmin; qq < qqmax; qq++) {
                q = cwrap(qq, nhocells);
                for (rr = rrmin; rr < rrmax; rr++) {
                    r = cwrap(rr, nhocells);
                    if (hoc[p*nhocells*nhocells+q*nhocells+r] != -1) {
                        i = hoc[p*nhocells*nhocells+q*nhocells+r];
                        while (1) { //while follow the chains
                            //read in second particle co-ordinate
                            xyzp2[0]=p2[i*nattr]; 
               			    xyzp2[1]=p2[i*nattr+1]; 
			                   xyzp2[2]=p2[i*nattr+2];
                            weight12=p1[iq*nattr+3]*p2[i*nattr+3];
			     
                            check=find_bin(xyzp1, xyzp2,samp,nbins,rlim, 
				                los,pbc,blen,&indx,&indy);

                            if(check!=-1 && 
				                 indx< nbins[0] && indy < nbins[1] &&
				                 indx>=0 && indy>=0) {
			                      if(njn==0) { //Taking care of jacknife regions
				                       xcind=indx*nbins[1]+indy;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      }
			                      else { //else-jacknife
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+njn;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 1
				                       jn1=p1[iq*nattr+4]; 
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn1;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 2
				                       jn2=p2[i*nattr+4]; 
                                   xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn2;
				                       if(jn1!=jn2) xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      } //end else-jacknife
			                      nvalid_pair++;
			                   } //end if-check
			                   else
			                      ninvalid_pair++;

                            if (ll[i] != -1) {
                               i = ll[i];
                            }
                            else break; //This breaks the while
                        } //This is end of while(1)
                    } //This is the end of hoc conditions
                } //This is the end of rr for-loop
            } //This is the end of qq for-loop
        }//This is the end of pp for-loop
    } //This is the end of iq for-loop
    free(ll);
    free(hoc);
    if(interactive>1)
       printf("\nnvalid_pair= %ld , invalid_pair= %ld \n",nvalid_pair, ninvalid_pair);
} //This is the end of function corr2d


double angle_between(double *axyz, double *bxyz ) {
   //calculates the angle between two cartesian co-ordinates
   double xsep; 
   xsep=axyz[0]*bxyz[0]+axyz[1]*bxyz[1]+axyz[2]*bxyz[2]; // dot product a*b*costheta
   xsep=xsep/sqrt(axyz[0]*axyz[0]+axyz[1]*axyz[1]+axyz[2]*axyz[2]);//divide by the magnitude of first
   xsep=xsep/sqrt(bxyz[0]*bxyz[0]+bxyz[1]*bxyz[1]+bxyz[2]*bxyz[2]);//divide by the magnitude of second galaxy

   xsep=acos(xsep); //inverse of cosine rule

   return xsep;
}

double angle_between_unit_noicos(double *axyz, double *bxyz) {
   //calculates the angle between two cartesian co-ordinates
   //assumes inputs are on unit sphere and retursn co thetha to avoid inverse cos
   double xsep; 
   xsep=axyz[0]*bxyz[0]+axyz[1]*bxyz[1]+axyz[2]*bxyz[2]; // dot product a*b*costheta

   return xsep;
}

double angle_between_noicos(double *axyz, double *bxyz) {
   //calculates the angle between two cartesian co-ordinates
   //retursn co thetha^2 to avoid inverse cos
   double xsep2; 
   xsep2=axyz[0]*bxyz[0]+axyz[1]*bxyz[1]+axyz[2]*bxyz[2]; // dot product a*b*costheta
   xsep2=xsep2*xsep2;
   xsep2=xsep2/(axyz[0]*axyz[0]+axyz[1]*axyz[1]+axyz[2]*axyz[2]);//divide by the magnitude of first
   xsep2=xsep2/(bxyz[0]*bxyz[0]+bxyz[1]*bxyz[1]+bxyz[2]*bxyz[2]);//divide by the magnitude of second galaxy

   return xsep2;
}

//assumes bins are in increasing order
static inline size_t find_dist_bin(double dist, double *restrict dbin,size_t  n) {
  size_t li, ui,ii;
  if(dist<dbin[0]) return SIZE_MAX;
  li = 0;
  ui = n-1 ;
  while (li <= ui) {
    ii = (li + ui) >> 1;
    if (dbin[ii + 1] <= dist) li = ii + 1;
    else if (dbin[ii] > dist) ui = ii - 1;
    else return ii;
  }
  return SIZE_MAX;
}

//assumes binning has been reverse to conform to increasing order
static inline size_t find_dist_bin_reverse(double dist, double *restrict dbin,size_t  n) {
  size_t li, ui,ii;
  if(dist<dbin[0]) {
     //printf("debug below min %12.8lf %12.8lf",dist,dbin[0]);
     return SIZE_MAX;
  }
  li = 0;
  ui = n-1 ;
  while (li <= ui) {
    ii = (li + ui) >> 1;
    if (dbin[ii + 1] <= dist) li = ii + 1;
    else if (dbin[ii] > dist) ui = ii - 1;
    else{//printf("count: %zu ",count);
       //exit(-1);
       return n-ii-1;
    } 
    //printf("%zu %zu %zu %zu %lf \n",count,li,ui,ii,dist);
  }
  return SIZE_MAX;
}

void get_los(int los, double *axyz, double *bxyz, double *vlos){
    //determine the vector for los
    //los=0 : mid-point, 1: along z-axis only used with pbc, los=2: first galaxy los=3: second galaxy
    if(los==0) {
        vlos[0]=axyz[0]+bxyz[0];
        vlos[1]=axyz[1]+bxyz[1];
        vlos[2]=axyz[2]+bxyz[2];
    }else if(los==1){
        vlos[0]=0;vlos[1]=0;vlos[2]=1;
    }else if(los==2){
        vlos[0]=axyz[0];vlos[1]=axyz[1];vlos[2]=axyz[2];
    }else if(los==3){
        vlos[0]=bxyz[0];vlos[1]=bxyz[1];vlos[2]=bxyz[2];
    }
}

//function to find the bin efficiently
size_t find_bin_efficient(double *axyz, double *bxyz, int samp, double *restrict dxbin, double *restrict dybin, size_t *restrict nsamp, double *restrict sampbound, int los, int pbc, double *blen,size_t *rx, size_t *ry) {
  

   if(samp==6 || samp==7){
       double xsep;
       //determine the angle
       xsep=angle_between_unit_noicos(axyz, bxyz);
       //find the angular bin number
       *ry=0;
       *rx=find_dist_bin_reverse(xsep, dxbin, nsamp[0]);
       //printf("xsep: %12.8lf %zu\n",xsep,*rx); 
   }else if(pbc==0){
       double xsep2, ysep2, diff[3],r2diff;
       double *vlos = (double *)calloc(3, sizeof(double));
       diff[0]=axyz[0]-bxyz[0]; diff[1]=axyz[1]-bxyz[1]; diff[2]=axyz[2]-bxyz[2];
       r2diff=diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2];
       //get the los
       get_los(los, axyz, bxyz, vlos);

       ysep2=diff[0]*vlos[0]+diff[1]*vlos[1]+diff[2]*vlos[2];
       if(samp==3 || samp==1 ){
           if(ysep2<0) ysep2=-ysep2*ysep2;
           else ysep2=ysep2*ysep2;
           ysep2=ysep2/(vlos[0]*vlos[0]+vlos[1]*vlos[1]+vlos[2]*vlos[2]);
           xsep2=r2diff-ysep2;
       }else{
          xsep2=r2diff;
          if(ysep2<0 && (samp==0 || samp==5)) ysep2=-ysep2*ysep2;
          else ysep2=ysep2*ysep2;
          ysep2=ysep2/xsep2;
       }
       *rx=find_dist_bin(xsep2, dxbin, nsamp[0]);
       if(samp==2 || samp==4) *ry=find_dist_bin(ysep2, dybin, nsamp[1]);
       else *ry=find_dist_bin_reverse(ysep2, dybin, nsamp[1]);
       free(vlos);
   }else if(pbc==1 && los==1){//pbc can only be aplied for los==1 z-axis
       double xsep2, ysep2;
       double diff[3];
       diff[0]=axyz[0]-bxyz[0]; diff[1]=axyz[1]-bxyz[1]; diff[2]=axyz[2]-bxyz[2];
       //periodic boundary conditions
       relwrap(diff, blen);
       if(samp==3 || samp==1){
           ysep2=diff[2]*diff[2];
           if(diff[2]<0) ysep2=-ysep2;
           xsep2=diff[0]*diff[0]+diff[1]*diff[1];
        }else{
           xsep2=diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2];
           ysep2=diff[2]*diff[2]/xsep2;
           if(diff[2]<0)
              if(samp==2 || samp==4) ysep2=-ysep2;
        }
       *rx=find_dist_bin(xsep2, dxbin, nsamp[0]);
       if(samp==2 || samp==4) *ry=find_dist_bin(ysep2, dybin, nsamp[1]);
       else *ry=find_dist_bin_reverse(ysep2, dybin, nsamp[1]);
   }else{
      printf("\n Not implemented samp=%d, los=%d\n; Please unset fastpc or implement this for fastpc",samp,los);
      exit(-1);
   }
 

   if(*rx==SIZE_MAX ||*ry==SIZE_MAX) return SIZE_MAX;
   else return 0;
}

//function to define the sampling and the bin
int find_bin(double *axyz, double *bxyz, int samp, int *nsamp, double *sampbound, 
      int los, int pbc, double *blen,int *rx, int *ry) {
   double difxyz[3], difmag=0, difDOTlos=0;
   double losxyz[3], losmag=0;
   double xsep=0,ysep=0, xcoord=0, ycoord=0;
   int ii;

   //Defining the line of sight
   if (los==0 || los==2 || los==3) {// the los is along the mid/first/second galaxy co-ordinate
      for (ii=0;ii<3;ii++){
         difxyz[ii]=axyz[ii]-bxyz[ii];
	 if(los==0) losxyz[ii]=0.5*(axyz[ii]+bxyz[ii]); //los is along the mid point
	 else if(los==2) losxyz[ii]=axyz[ii];//los is along the first galaxy
	 else losxyz[ii]=bxyz[ii]; // los is along the second galaxy
         losmag=losmag+losxyz[ii]*losxyz[ii];
         difmag=difmag+difxyz[ii]*difxyz[ii];
         difDOTlos=difDOTlos+difxyz[ii]*losxyz[ii];
      }
      difmag=sqrt(difmag);
      losmag=sqrt(losmag);
      ycoord=difDOTlos/losmag;  
      xcoord=sqrt(difmag*difmag-ycoord*ycoord);
   }
   else if(los==1) {//the los is along the Z axis
      for (ii=0;ii<3;ii++)
         difxyz[ii]=axyz[ii]-bxyz[ii];
      if(pbc==1) { //pbc can be applied only with los=1 along z axis
	 relwrap(difxyz, blen); 
      }
      ycoord=difxyz[2];
      xcoord=sqrt(difxyz[0]*difxyz[0]+difxyz[1]*difxyz[1]);
   }

   //Using a given sampling definition for 2d correlation function
   if(samp==0) { //sampling in rmu space
      xsep=sqrt(xcoord*xcoord+ycoord*ycoord);
      ysep=fabs(ycoord/xsep); //consider positive and negative mu together
   }
   else if (samp==1) { //rpar-rper
      ysep=ycoord;
      xsep=xcoord;
   }
   else if (samp==2) { //r theta
      xsep=sqrt(xcoord*xcoord+ycoord*ycoord);
      ysep=acos(ycoord/xsep);
   }
   else if (samp==3) { //log rperp- rpar
      ysep=ycoord;
      xsep=log10(xcoord);
   }
   else if (samp==4) { //log(r)-theta
      xsep=sqrt(xcoord*xcoord+ycoord*ycoord);
      ysep=acos(ycoord/xsep);
      xsep=log10(xsep);
   }
   else if (samp==5) { //log(r)-mu
      xsep=sqrt(xcoord*xcoord+ycoord*ycoord);
      ysep=fabs(ycoord/xsep);
      xsep=log10(xsep);
   }
   else if (samp==6 || samp==7) { //costheta rpar
      ysep=0.5;//ycoord; the clustering id independent of y-sep only angular
      xsep=angle_between(axyz, bxyz);
      if(samp==7) xsep=log10(xsep);
      //printf("xsep: %lf ysep: %lf\n",xsep,ysep);
   }
   else {
      printf("\ninvalid sampling mode %d\n",samp);
      exit(0);
   }

   //Determine the bin in which the pair lies
   if(xsep > sampbound[0] && xsep < sampbound[1] && ysep >sampbound[2] && ysep <sampbound[3])
   {
      int xbin_n = floor((xsep-sampbound[0])*nsamp[0]/(sampbound[1]-sampbound[0]));
      int ybin_n = floor((ysep-sampbound[2])*nsamp[1]/(sampbound[3]-sampbound[2]));
      if(xbin_n == nsamp[0]) xbin_n -= 1;
      if(ybin_n == nsamp[1]) ybin_n -= 1;
      *rx=xbin_n;
      *ry=ybin_n;
      return 0;
   }
   else {
      *rx=-1;
      *ry=-1;
      return -1;
   }

   return -1;
}



void evaluate_iip(double *pp, int nattr, long *fib, int nfib_int, int bit_perlong, long fp, long np) {
    int max_count=0;
    for (long ii=fp; ii<np;ii++){
       int gcount=0;
       for (int fib_ii=0; fib_ii<nfib_int; fib_ii++){
          gcount=gcount+countOnes (fib[ii*nfib_int+fib_ii]);	     
       }
       double iip=(bit_perlong*nfib_int)/gcount;
       //just testing sqrt for time being because pip in case of independence should be same as iip
      // iip=sqrt(iip); On thinking harder this shouldn't be true
       pp[ii*nattr+3]=pp[ii*nattr+3]*iip;

       if(max_count<gcount) max_count=gcount;
    }

    if(max_count<0.9*bit_perlong*nfib_int || max_count> bit_perlong*nfib_int){
       if(max_count<0.9*bit_perlong*nfib_int) 
          printf("Warning: We are expecting that atleast 90 percent of relization will always contain one of the galaxy\n");
       else if(max_count>bit_perlong*nfib_int) 
          printf("Warning: We are expecting that max_count not to be more than total number of realization\n");
       printf("Total number of realization; %d\n",bit_perlong*nfib_int);
       printf("Maximum number of time a galaxy appear: %d\n", max_count);
       printf("Please asses the bitwiese weight and check if relevant inputs are set appropriately\n");
       printf("Relevant variables:\n\t bit_perlong=%d\n\t nfib_int=%d\n",bit_perlong,nfib_int);
       
       if(max_count>bit_perlong*nfib_int)  exit(1);
    }
    /*for (int fib_ii=0; fib_ii<nfib_int; fib_ii++) printf("%d %d\n",fib_ii,countOnes(fib[fib_ii]));
    printf("size of int %lu\n",sizeof(int));
    printf("size of long %lu\n",sizeof(long));
    exit(1);*/
}


double get_ang_up_weight(double *axyz, double *bxyz, double *log_theta_lim, int nbin_log_theta, double *wang_up) {
    //calculates the angular upweight for the pair of galaxies with given cartesian co-ordinates
    double theta_pair=angle_between(axyz, bxyz);
    int theta_ind=(log10(theta_pair)-log_theta_lim[0])/log_theta_lim[2];

    if(theta_ind<0) theta_ind=0;
    else if(theta_ind>=nbin_log_theta) theta_ind=nbin_log_theta-1;

    return wang_up[theta_ind];
}

double get_ang_up_weight_efficient(int samp,double *axyz, double *bxyz, double *cos_theta_lim, size_t nbin_log_theta, double *wang_up) {
    //assumes the cartesian co-ordinates are in unit sphere and sampling in cosing thetas
    //calculates the angular upweight for the pair of galaxies with given cartesian co-ordinates
    double costheta_pair;
    size_t theta_ind;
    if(samp==6 || samp==7) costheta_pair=angle_between_unit_noicos(axyz, bxyz);
    else costheta_pair=angle_between_noicos(axyz, bxyz);

    if(costheta_pair>cos_theta_lim[nbin_log_theta]) theta_ind=0;
    else if(costheta_pair<cos_theta_lim[0]) theta_ind = nbin_log_theta-1;
    else theta_ind=find_dist_bin_reverse(costheta_pair, cos_theta_lim, nbin_log_theta);

    return wang_up[theta_ind];
}

void corr2d_wpip(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, 
      double *rlim, int nbins0, int nbins1, int nhocells, double *blen, 
      double *posmin, int samp, int njn, int pbc, int los, int interactive,
      long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip, 
      int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up) {

    // 2d correlation function.
    //
    // Parameters:
    // xc_2d : (nbins, nbins) array, 2d correlation to be returned.
    // p1 : (np1,6/7) first group of particles.
    // np1: number of particles in first group.
    // p2 : (np2,6/7) second group of particles.
    // np2 : number of particles of the second group.
    // rlim[4] : the minimum,maximum range you are looking at in both axis.
    // nbins0,1: number of bins in both axis.
    // nhocells: number of cells.
    // blen[3]: box size in 3d or the extent of data.
    // posmin[3]: minimum value of each co-ordinate (minimum corner of box)
    // samp : to decide the sampling scheme of the correlation function
    // njn : (0/njn) to decide if jacknife subsample needs to be evaluated or not
    // pc : (0/1) to decide periodic boundary condition or not
    // fib1 : (np1,nfibreal) contains interger whose binary representation gives whether a galaxy got fibre or not, fib2 is for second catalog
    // nfib_int1/2 : number of integer needed to store the fibre realization, generally fibre realization will be nfib_int*bit_perlong (assuming 32 bit integer is used to store them with one for sign). If nfib_int1/2 is zeros means for that sample a fibre realization is not needed for example randoms or no pip, nfib_in1/2 must be equal or 0. If only one of the nfib_int1/2 is non-zero then iip weight is used.'
    //need_iip: this should be set to 0, when set to one even with both input in only iip will be used
    //need_angup: set 1 if angular upweight is needed need to also provide log_theta_lim, int nbin_log_theta, double *wang_up 
    //*log_theta_lim : array with minimum of log_theta, maximum of log_theta, d_log_theta
    //nbin_log_theta: number of bins for wang_up
    //*wang_up : Array storing the angular up_weight
    

    long iq, i;
    int pp, qq, rr, p, q, r, ix, iy, iz; //To run loops on grids and particles
    int ppmin, ppmax, qqmin, qqmax, rrmin, rrmax; //To figure of the range of grid span
    int indx , indy; //To store the index of correlation function fbin
    int check, nattr=4;

    int need_angular=0; //This decides whether angular clusterin is needed
    if(samp==6 || samp==7) need_angular=1;

    int nbins[2];
    nbins[0]=nbins0; nbins[1]=nbins1;
    //Allocate some memory
    //int *ncb = (int *)calloc(3, sizeof(int));
    //double *xyzp1 = (double *)calloc(3, sizeof(double));
    //double *xyzp2 = (double *)calloc(3, sizeof(double));
   
    int  jn1,jn2,xcind;
    double xyzp1[3], xyzp2[3], weight12;
    long nvalid_pair=0, ninvalid_pair=0;

    int need_pip=0; //this should be set to 1 if pip weight is needed

    // Add weight or not
    if (njn > 0)  nattr =5 ;
    else  nattr = 4;


    //make sure if nfib_int1/2 is not zeros then equal
    if(nfib_int1>0 && nfib_int2>0) // This case is for pip
    {    assert(nfib_int1==nfib_int2);
         if(need_iip==1){
             evaluate_iip(p2, nattr, fib2, nfib_int2,bit_perlong, fp2, np2);
             evaluate_iip(p1, nattr, fib1, nfib_int1,bit_perlong, fp1, np1);
             need_pip=0;
         }else{
            need_pip=1;
         }
    }
    else if(nfib_int1==0 && nfib_int2>0) //update second weight for iip
    {    evaluate_iip(p2, nattr, fib2, nfib_int2, bit_perlong,fp2, np2);}   
    else if(nfib_int1>0 && nfib_int2==0) //update second weight for iip
    {    evaluate_iip(p1, nattr, fib1, nfib_int1, bit_perlong,fp1, np1);}
    //else if(nfib_int1==0 and nfib_int2==0) //no pip or iip is needed
    //{    evaluate_iip(p1, fib1, int nfib_int1, fp1, np1);}

    

    long *ll = (long *)calloc(np2, sizeof(long));
    long *hoc = (long *)calloc(nhocells*nhocells*nhocells, sizeof(long));
    long hoc_id;
    double *sph, *posmin_sph, *blen_sph, *sph_tmp;

    // initialization
    
    
    if(need_angular==0){
       init_mesh(ll, hoc, p2, fp2, np2, nattr, nhocells, blen, posmin);
    } else if(need_angular==1){
        // for angular clustering
        sph = (double *)calloc(3*(np2-fp2), sizeof(double));
        blen_sph = (double *)calloc(3, sizeof(double));
        posmin_sph = (double *)calloc(3, sizeof(double));
        
        //This is to store single co-ordinate
        sph_tmp = (double *)calloc(3, sizeof(double));

        init_mesh_angular(ll, hoc, sph, blen_sph, posmin_sph, p2, fp2, np2, nattr, nhocells);
    }

    //linked-list cells to scan
    double *maxrad=(double *)calloc(3, sizeof(double));
    maxradius(rlim ,samp,maxrad);
    int ncb[3];

    //printf("samp bound %lf %lf %lf %lf\n",rlim[0],rlim[1],rlim[2],rlim[3]);
    for (pp=0; pp<3;pp++){
       if(need_angular==0) ncb[pp] = ceil((maxrad[pp] / blen[pp]) * (double)(nhocells)) + 1;
       else                ncb[pp] = ceil((maxrad[pp] / blen_sph[pp]) * (double)(nhocells)) + 1;
       //printf("ncb [%d] = %d mrad=%lf blen=%lf\n",pp,ncb[pp], maxrad[pp],blen_sph[pp]);
       //ncb[pp] = floor((maxrad / blen[pp]) * (double)(nhocells)) + 1;
    }

    for (iq = fp1; iq < np1; iq++) {
        if (iq % 100000 == 0 && interactive>=0) printf(".");

        //read in first particle co-ordinate
        xyzp1[0] = p1[iq*nattr]; xyzp1[1] = p1[iq*nattr+1]; xyzp1[2] = p1[iq*nattr+2];

        if(need_angular==0){
            ix = floor((xyzp1[0]-posmin[0]) / blen[0] * nhocells);
            iy = floor((xyzp1[1]-posmin[1]) / blen[1] * nhocells);
            iz = floor((xyzp1[2]-posmin[2]) / blen[2] * nhocells);

	         ppmin=ix-ncb[0]; ppmax=ix+ncb[0];
	         qqmin=iy-ncb[1]; qqmax=iy+ncb[1];
	         rrmin=iz-ncb[2]; rrmax=iz+ncb[2];
        } else if(need_angular==1){//angular clustering 2d linked list
      
            cartesian_to_spherical_polar(sph_tmp, xyzp1[0], xyzp1[1],xyzp1[2]);
            ix = 0;// floor((sph_tmp[0]-posmin_sph[0]) / blen_sph[0] * nhocells);
            iy = floor((sph_tmp[1]-posmin_sph[1]) / blen_sph[1] * nhocells);
            iz = floor((sph_tmp[2]-posmin_sph[2]) / blen_sph[2] * nhocells);

	         ppmin=0; ppmax=1; //for angular clustering only 2d is needed ix-ncb[0]; ppmax=ix+ncb[0];
	         qqmin=iy-ncb[1]; qqmax=iy+ncb[1];
	         rrmin=iz-ncb[2]; rrmax=iz+ncb[2];
        } 
	
	     if(pbc!=1) { //pbc can be applied only with los=1 along z axis
            if(ppmin<0) ppmin=0;
            if(qqmin<0) qqmin=0;
            if(rrmin<0) rrmin=0;

            if(ppmax>nhocells) ppmax=nhocells;
            if(qqmax>nhocells) qqmax=nhocells;
            if(rrmax>nhocells) rrmax=nhocells;
	     }     

        for (pp = ppmin; pp < ppmax; pp++) {
            p = cwrap(pp, nhocells);
            for (qq = qqmin; qq < qqmax; qq++) {
                q = cwrap(qq, nhocells);
                for (rr = rrmin; rr < rrmax; rr++) {
                    r = cwrap(rr, nhocells);

                    if(need_angular==0) hoc_id=p*nhocells*nhocells+q*nhocells+r;
                    else hoc_id= q*nhocells+r;

                    if (hoc[hoc_id] != -1) {
                        i = hoc[hoc_id];
                        while (1) { //while follow the chains
                            //read in second particle co-ordinate
                            xyzp2[0]=p2[i*nattr]; 
               			    xyzp2[1]=p2[i*nattr+1]; 
			                   xyzp2[2]=p2[i*nattr+2];
                            weight12=p1[iq*nattr+3]*p2[i*nattr+3];

                            //Angular up-weight
                            if(need_angup==1){
                                //calculates the angular upweight for the pair of galaxies with given cartesian co-ordinates
                                weight12=weight12*get_ang_up_weight(xyzp1,xyzp2,log_theta_lim, nbin_log_theta, wang_up);

                            }

                            if(need_pip==1) {
                                //evaluate the pip weight
                                int popcount=0;
                                for (int fib_ii=0;fib_ii<nfib_int1; fib_ii++)
                                {   long f12= fib1[iq*nfib_int1+fib_ii] & fib2[i*nfib_int1+fib_ii];
                                    popcount=popcount+countOnes (f12);
                                }
                                //This is where pipweight is incoporated in the calculation
                                weight12=weight12*(bit_perlong*nfib_int1)/popcount;
                            }//end of need pip loop
			     
                            check=find_bin(xyzp1, xyzp2,samp,nbins,rlim,los,pbc,blen,&indx,&indy);

                            if(check!=-1 && 
				                 indx< nbins[0] && indy < nbins[1] &&
				                 indx>=0 && indy>=0) {
			                      if(njn==0) { //Taking care of jacknife regions
				                       xcind=indx*nbins[1]+indy;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      }
			                      else { //else-jacknife
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+njn;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 1
				                       jn1=p1[iq*nattr+4]; 
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn1;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 2
				                       jn2=p2[i*nattr+4]; 
                                   xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn2;
				                       if(jn1!=jn2) xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      } //end else-jacknife
			                      nvalid_pair++;
			                   } //end if-check
			                   else
			                      ninvalid_pair++;

                            if (ll[i] != -1) {
                               i = ll[i];
                            }
                            else break; //This breaks the while
                        } //This is end of while(1)
                    } //This is the end of hoc conditions
                } //This is the end of rr for-loop
            } //This is the end of qq for-loop
        }//This is the end of pp for-loop
    } //This is the end of iq for-loop
    free(ll);
    free(hoc);
    if(samp==6 || samp==7){//angular clustering 2d linked list
       free(sph);
       free(posmin_sph);
       free(blen_sph);
       free(sph_tmp);
    }

    if(interactive>1)
       printf("\nnvalid_pair= %ld , invalid_pair= %ld \n",nvalid_pair, ninvalid_pair);
} //This is the end of function corr2d_wpip

static inline void reverse_array(double *restrict array, size_t nbin){
   //reverses an array in place
   size_t ii=0;
   size_t jj=nbin-1;
   while (ii < jj) 
   {
      double Temp = array[ii];
      array[ii] = array[jj];
      array[jj] = Temp;
      ii++;             
      jj--;         
   }
}

//This setups the bin limit to place things in appropriate bins
void setup_bins(double *dxbin,double *dybin, size_t *restrict nsamp, int samp,double *restrict sampbound){
    //The dxbin and dybin must always be in increasing order for each case
    //In case the natural binning is decreasing then reorganize dxbin and dybin and call find_dist_bin_reverse

    double dx=(sampbound[1]-sampbound[0])/nsamp[0];
    double dy=(sampbound[3]-sampbound[2])/nsamp[1];
    //create bin edges for the x and y-direction
    for(size_t ii=0;ii<nsamp[0]+1;ii++){
       dxbin[ii]=sampbound[0]+ii*dx;
       //printf("%zu %lf %lf %lf\n",ii,dxbin[ii],pow(10,dxbin[ii]),cos(pow(10,dxbin[ii])));
    }
    
    for(size_t ii=0;ii<nsamp[1]+1;ii++){
       dybin[ii]=sampbound[2]+ii*dy;
    }


    //now convert them to different values based on the sampling needed to avoid expensive calculations
    if(samp==6 || samp==7){//angular binning need to take costheta to avoid inverse cos for pairs and also antilog for log case

       for(size_t ii=0;ii<nsamp[0]+1;ii++){
          if(samp==6) dxbin[ii]=cos(dxbin[ii]);
          else if(samp==7) dxbin[ii]=cos(pow(10,dxbin[ii]));
       }
       //reverse the bining to make it in increasing order as cosing decreases from zero upward
       reverse_array(dxbin, nsamp[0]+1);
    }else if(samp==3 || samp==4){//sampling in log rperp and rpar
       for(size_t ii=0;ii<nsamp[0]+1;ii++){
          if(samp==3) dxbin[ii]=pow(10,2*dxbin[ii]);
          else dxbin[ii]=pow(dxbin[ii],2);
         }
       for(size_t ii=0;ii<nsamp[1]+1;ii++){
          if(dybin[ii]<0) dybin[ii]=-dybin[ii]*dybin[ii];
          else dybin[ii]=dybin[ii]*dybin[ii];
         }
    }else if(samp==0 || samp==2 || samp==4 || samp==5){
       for(size_t ii=0;ii<nsamp[0]+1;ii++){
          if(samp==0 || samp==2) dxbin[ii]=pow(dxbin[ii],2);
          else dxbin[ii]=pow(10,2*dxbin[ii]);
       }
       for(size_t ii=0;ii<nsamp[1]+1;ii++){
          if(samp==0 || samp==5) dybin[ii]=pow(dybin[ii],2);
          else if(dybin[ii]<0)  dybin[ii]=-pow(cos(dybin[ii]),2);
          else  dybin[ii]=pow(cos(dybin[ii]),2);
       }
       if(samp==2 || samp==4) 
           //reverse the bining to make it in increasing order as cosing decreases from zero upward
           reverse_array(dybin, nsamp[1]+1);

    }

} //end of function setup_bins

    
void setup_angup_bin(int samp, double *coslog_theta_lim,double *log_theta_lim,size_t nbin_cos_log_theta){
   for(size_t ii=0; ii<nbin_cos_log_theta+1; ii++){
      coslog_theta_lim[ii]=log_theta_lim[0]+log_theta_lim[2]*ii;
      coslog_theta_lim[ii]=cos(pow(10,coslog_theta_lim[ii]));
   }
   //reverse this array as the cos-theta is decreasing function
   reverse_array(coslog_theta_lim, nbin_cos_log_theta+1);
   if(samp!=6 || samp!=7){//take square of cos_theta
      for(size_t ii=0; ii<nbin_cos_log_theta+1; ii++){
         coslog_theta_lim[ii]=coslog_theta_lim[ii]*coslog_theta_lim[ii];
      }
   }
} // end of function setup_angup_bin


///Thus version tries to avoid unneccessary calculation to make things more efficient
void corr2d_wpip_efficient(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, 
      double *rlim, int nbins0, int nbins1, int nhocells, double *blen, 
      double *posmin, int samp, int njn, int pbc, int los, int interactive,
      long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip, 
      int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up) {

    // 2d correlation function.
    //
    // Parameters:
    // xc_2d : (nbins, nbins) array, 2d correlation to be returned.
    // p1 : (np1,6/7) first group of particles.
    // np1: number of particles in first group.
    // p2 : (np2,6/7) second group of particles.
    // np2 : number of particles of the second group.
    // rlim[4] : the minimum,maximum range you are looking at in both axis.
    // nbins0,1: number of bins in both axis.
    // nhocells: number of cells.
    // blen[3]: box size in 3d or the extent of data.
    // posmin[3]: minimum value of each co-ordinate (minimum corner of box)
    // samp : to decide the sampling scheme of the correlation function
    // njn : (0/njn) to decide if jacknife subsample needs to be evaluated or not
    // pc : (0/1) to decide periodic boundary condition or not
    // fib1 : (np1,nfibreal) contains interger whose binary representation gives whether a galaxy got fibre or not, fib2 is for second catalog
    // nfib_int1/2 : number of integer needed to store the fibre realization, generally fibre realization will be nfib_int*bit_perlong (assuming 32 bit integer is used to store them with one for sign). If nfib_int1/2 is zeros means for that sample a fibre realization is not needed for example randoms or no pip, nfib_in1/2 must be equal or 0. If only one of the nfib_int1/2 is non-zero then iip weight is used.'
    //need_iip: this should be set to 0, when set to one even with both input in only iip will be used
    //need_angup: set 1 if angular upweight is needed need to also provide log_theta_lim, int nbin_log_theta, double *wang_up 
    //*log_theta_lim : array with minimum of log_theta, maximum of log_theta, d_log_theta
    //nbin_log_theta: number of bins for wang_up
    //*wang_up : Array storing the angular up_weight
    

    long iq, i;
    int pp, qq, rr, p, q, r, ix, iy, iz; //To run loops on grids and particles
    int ppmin, ppmax, qqmin, qqmax, rrmin, rrmax; //To figure of the range of grid span
    size_t indx , indy, check; //To store the index of correlation function fbin
    int  nattr=4;

    int need_angular=0; //This decides whether angular clusterin is needed
    if(samp==6 || samp==7) need_angular=1;

    size_t nbins[2];
    nbins[0]=nbins0; nbins[1]=nbins1;
    //Allocate some memory
    //int *ncb = (int *)calloc(3, sizeof(int));
    //double *xyzp1 = (double *)calloc(3, sizeof(double));
    //double *xyzp2 = (double *)calloc(3, sizeof(double));
   
    int  jn1,jn2;
    size_t xcind;
    double xyzp1[3], xyzp2[3], weight12;
    long nvalid_pair=0, ninvalid_pair=0;

    int need_pip=0; //this should be set to 1 if pip weight is needed

    // Add weight or not
    if (njn > 0)  nattr =5 ;
    else  nattr = 4;
 
    printf("************ using fast pc ************\n");

    //make sure if nfib_int1/2 is not zeros then equal
    if(nfib_int1>0 && nfib_int2>0) // This case is for pip
    {    assert(nfib_int1==nfib_int2);
         if(need_iip==1){
             evaluate_iip(p2, nattr, fib2, nfib_int2,bit_perlong, fp2, np2);
             evaluate_iip(p1, nattr, fib1, nfib_int1,bit_perlong, fp1, np1);
             need_pip=0;
         }else{
            need_pip=1;
         }
    }
    else if(nfib_int1==0 && nfib_int2>0) //update second weight for iip
    {    evaluate_iip(p2, nattr, fib2, nfib_int2, bit_perlong,fp2, np2);}   
    else if(nfib_int1>0 && nfib_int2==0) //update second weight for iip
    {    evaluate_iip(p1, nattr, fib1, nfib_int1, bit_perlong,fp1, np1);}
    //else if(nfib_int1==0 and nfib_int2==0) //no pip or iip is needed
    //{    evaluate_iip(p1, fib1, int nfib_int1, fp1, np1);}

   
    //setup the bin limits as array
    double *dxbin=(double *)calloc(nbins0+1, sizeof(double));
    double *dybin=(double *)calloc(nbins1+1, sizeof(double));
    setup_bins(dxbin,dybin,nbins,samp,rlim);
   
    //To get the efficient angular upweight
    size_t nbin_cos_log_theta=nbin_log_theta;
    if(nbin_cos_log_theta<1) nbin_cos_log_theta=2;
    double *coslog_theta_lim=(double *)calloc(nbin_cos_log_theta+1, sizeof(double));
    if(need_angup==1) setup_angup_bin(samp,coslog_theta_lim,log_theta_lim,nbin_cos_log_theta);

    //to debug
    //printf("bins: \n");
    //for(size_t ii=0;ii<nbins[0]+1;ii++) printf("%zu %12.9lf\n",ii,dxbin[ii]);

    long *ll = (long *)calloc(np2, sizeof(long));
    long *hoc = (long *)calloc(nhocells*nhocells*nhocells, sizeof(long));
    long hoc_id;
    double *sph, *posmin_sph, *blen_sph, *sph_tmp;

    // initialization
    
    
    if(need_angular==0){
       init_mesh(ll, hoc, p2, fp2, np2, nattr, nhocells, blen, posmin);
    } else if(need_angular==1){
        // for angular clustering
        sph = (double *)calloc(3*(np2-fp2), sizeof(double));
        blen_sph = (double *)calloc(3, sizeof(double));
        posmin_sph = (double *)calloc(3, sizeof(double));
        
        //This is to store single co-ordinate
        sph_tmp = (double *)calloc(3, sizeof(double));

        init_mesh_angular(ll, hoc, sph, blen_sph, posmin_sph, p2, fp2, np2, nattr, nhocells);
    }

    //linked-list cells to scan
    double *maxrad=(double *)calloc(3, sizeof(double));
    maxradius(rlim ,samp,maxrad);
    int ncb[3];

    //printf("samp bound %lf %lf %lf %lf\n",rlim[0],rlim[1],rlim[2],rlim[3]);
    for (pp=0; pp<3;pp++){
       if(need_angular==0) ncb[pp] = ceil((maxrad[pp] / blen[pp]) * (double)(nhocells)) + 1;
       else                ncb[pp] = ceil((maxrad[pp] / blen_sph[pp]) * (double)(nhocells)) + 1;
       //printf("ncb [%d] = %d mrad=%lf blen=%lf\n",pp,ncb[pp], maxrad[pp],blen_sph[pp]);
       //ncb[pp] = floor((maxrad / blen[pp]) * (double)(nhocells)) + 1;
    }

    for (iq = fp1; iq < np1; iq++) {
        if (iq % 100000 == 0 && interactive>=0) printf(".");

        //read in first particle co-ordinate
        xyzp1[0] = p1[iq*nattr]; xyzp1[1] = p1[iq*nattr+1]; xyzp1[2] = p1[iq*nattr+2];

        if(need_angular==0){
            ix = floor((xyzp1[0]-posmin[0]) / blen[0] * nhocells);
            iy = floor((xyzp1[1]-posmin[1]) / blen[1] * nhocells);
            iz = floor((xyzp1[2]-posmin[2]) / blen[2] * nhocells);

	         ppmin=ix-ncb[0]; ppmax=ix+ncb[0];
	         qqmin=iy-ncb[1]; qqmax=iy+ncb[1];
	         rrmin=iz-ncb[2]; rrmax=iz+ncb[2];
        } else if(need_angular==1){//angular clustering 2d linked list
      
            cartesian_to_spherical_polar(sph_tmp, xyzp1[0], xyzp1[1],xyzp1[2]);
            ix = 0;// floor((sph_tmp[0]-posmin_sph[0]) / blen_sph[0] * nhocells);
            iy = floor((sph_tmp[1]-posmin_sph[1]) / blen_sph[1] * nhocells);
            iz = floor((sph_tmp[2]-posmin_sph[2]) / blen_sph[2] * nhocells);

	         ppmin=0; ppmax=1; //for angular clustering only 2d is needed ix-ncb[0]; ppmax=ix+ncb[0];
	         qqmin=iy-ncb[1]; qqmax=iy+ncb[1];
	         rrmin=iz-ncb[2]; rrmax=iz+ncb[2];
        } 
	
	     if(pbc!=1) { //pbc can be applied only with los=1 along z axis
            if(ppmin<0) ppmin=0;
            if(qqmin<0) qqmin=0;
            if(rrmin<0) rrmin=0;

            if(ppmax>nhocells) ppmax=nhocells;
            if(qqmax>nhocells) qqmax=nhocells;
            if(rrmax>nhocells) rrmax=nhocells;
	     }     

        for (pp = ppmin; pp < ppmax; pp++) {
            p = cwrap(pp, nhocells);
            for (qq = qqmin; qq < qqmax; qq++) {
                q = cwrap(qq, nhocells);
                for (rr = rrmin; rr < rrmax; rr++) {
                    r = cwrap(rr, nhocells);

                    if(need_angular==0) hoc_id=p*nhocells*nhocells+q*nhocells+r;
                    else hoc_id= q*nhocells+r;

                    if (hoc[hoc_id] != -1) {
                        i = hoc[hoc_id];
                        while (1) { //while follow the chains
                            //read in second particle co-ordinate
                            xyzp2[0]=p2[i*nattr]; 
               			    xyzp2[1]=p2[i*nattr+1]; 
			                   xyzp2[2]=p2[i*nattr+2];
                            weight12=p1[iq*nattr+3]*p2[i*nattr+3];

                            //Angular up-weight
                            if(need_angup==1){
                                //calculates the angular upweight for the pair of galaxies with given cartesian co-ordinates
                                //weight12=weight12*get_ang_up_weight(xyzp1,xyzp2,log_theta_lim, nbin_log_theta, wang_up);
                                weight12=weight12*get_ang_up_weight_efficient(samp,xyzp1,xyzp2,coslog_theta_lim, nbin_cos_log_theta, wang_up);
                            }

                            if(need_pip==1) {
                                //evaluate the pip weight
                                size_t popcount=0;
                                for (int fib_ii=0;fib_ii<nfib_int1; fib_ii++)
                                {   long f12= fib1[iq*nfib_int1+fib_ii] & fib2[i*nfib_int1+fib_ii];
                                    popcount=popcount+countOnes (f12);
                                }
                                //This is where pipweight is incoporated in the calculation
                                weight12=weight12*(bit_perlong*nfib_int1)/popcount;
                            }//end of need pip loop
			     
                            //check=find_bin(xyzp1, xyzp2,samp,nbins,rlim,los,pbc,blen,&indx,&indy);
                            check=find_bin_efficient(xyzp1, xyzp2,samp,dxbin,dybin,nbins,rlim,los,pbc,blen,&indx,&indy);

                            //printf("%zu %zu %zu; %zu %zu\n",check,indx,indy,nbins[0],nbins[1]);
                            //if(ninvalid_pair>1000) exit(-1);

                            if(check==0) {
			                      if(njn==0) { //Taking care of jacknife regions
				                       xcind=indx*nbins[1]+indy;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      }
			                      else { //else-jacknife
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+njn;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 1
				                       jn1=p1[iq*nattr+4]; 
				                       xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn1;
			                          xc_2d[xcind] = xc_2d[xcind]+weight12;
				                       //Jacknife region of particle 2
				                       jn2=p2[i*nattr+4]; 
                                   xcind=indx*nbins[1]*(njn+1)+indy*(njn+1)+jn2;
				                       if(jn1!=jn2) xc_2d[xcind] = xc_2d[xcind]+weight12;
			                      } //end else-jacknife
			                      nvalid_pair++;
			                   } //end if-check
			                   else
			                      ninvalid_pair++;

                            if (ll[i] != -1) {
                               i = ll[i];
                            }
                            else break; //This breaks the while
                        } //This is end of while(1)
                    } //This is the end of hoc conditions
                } //This is the end of rr for-loop
            } //This is the end of qq for-loop
        }//This is the end of pp for-loop
    } //This is the end of iq for-loop
    free(ll);
    free(hoc);
    if(samp==6 || samp==7){//angular clustering 2d linked list
       free(sph);
       free(posmin_sph);
       free(blen_sph);
       free(sph_tmp);
    }


    //free the bining setup
    free(dxbin);
    free(dybin);

    //The angular upweight limits
    free(coslog_theta_lim);

    if(interactive>1)
       printf("\nnvalid_pair= %ld , invalid_pair= %ld \n",nvalid_pair, ninvalid_pair);
} //This is the end of function corr2d_wpip_efficient
