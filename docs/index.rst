.. CorrelationFunction documentation master file, created by
   sphinx-quickstart on Wed Nov  4 16:12:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CorrelationFunction's documentation!
===============================================

This package performs efficient computation of Correlation Function.
Its main advantages are:
 * Large number of binning options
 * paiwise weight, angular upweight and individual galaxy weights can be applied
 * on the fly jackknife covariance
 * efficient backend written in c
 * Especially useful feature to look at environment dependent clustering
 * multiprocessing for single node parallellization
 * Easy to handle the input file in python interface
 * can use multiple nodes by splitting the catalogue but basic parallelization


Dependencies:
===============
 * numpy
 * cython
 * configobj (Only if you want to use configuration file for input, https://pypi.org/project/configobj/ )

Installation
============
git clone 
python setup.py build_ext --inplace

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Correlation Function

   Installation
