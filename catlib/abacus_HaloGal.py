import numpy as np
import pylab as pl
import mycosmo as mcosm
import fitsio as F

#utility function
import util
import General_FITS_selection as GFS



def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''

    
    findat=F.FITS(datafile)
    if('halo' in IPdict['tracer']):
        tspl=IPdict['tracer'].split('_')
        logmass_min=float(tspl[1])
        logmass_max=float(tspl[2])
        logmass=np.log10(findat[1]['mvir'][:])
      
        isel=(logmass>=logmass_min)*(logmass<=logmass_max)
        indsel_dat=np.where(isel)[0]
        print('Applied halo mass selection (%f,%f) : %d/%d'%(logmass_min,logmass_max,indsel_dat.size,isel.size))
    else:
        indsel_dat=findat[1].where('1>0')
    

    if(randfile in ['norand','generate']):
        finran=None
        if(randfile=='norand'):
            indsel_ran=np.arange(10)
        else:
            nrand_need=args.randfactor*indsel_dat.size
            indsel_ran=np.arange(nrand_need)
    else:
        finran=F.FITS(randfile)

        indsel_ran=finran[1].where('1>0')
  
        if(args.randfactor>0 and indsel_ran.size>args.randfactor*indsel_dat.size):
            #need to subsample the random
            need_rand=args.randfactor*indsel_dat.size
            np.random.seed(0)
            indsel_ran=np.random.choice(indsel_ran,need_rand,replace=False)
            #sort the indices
            indsel_ran.sort()

    return findat,indsel_dat,finran,indsel_ran


def is_perfect_power(number,power):
    """
    Indicates (with True/False) if the provided number is a perfect power.
    """
    number = abs(number)  # Prevents errors due to negative numbers
    return round(number ** (1 / power)) ** power == number


def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in prop dic in fH'''
   

    if(is_perfect_power(args.njn,3)):
        jntype='3d'
        NJNx=int(np.round(args.njn ** (1. / 3)))
        NJNy=NJNx;NJNz=NJNx
        print('Using 3d Jacknife:', NJNx, NJNy, NJNz)
    elif(is_perfect_power(args.njn,2)):
        jntype='2d'
        NJNx=int(np.sqrt(args.njn))
        NJNy=int(args.njn/NJNx)
        print('Using 2d Jacknife:', NJNx, NJNy)
    else:
        print('''error: njn must be either a perfect square or perfect cube''')
        sys.exit()

    #get the xyz
    mat=GetXYZ(fH,indsel,args)

    #adding jacknife regions
    if(args.njn>0 and args.los==1):
        indx=np.floor(NJNx*mat[:,0]/args.Lbox)
        indy=np.floor(NJNy*mat[:,1]/args.Lbox)
      
        #apply modulo operation on x an y index
        indx=np.mod(indx,NJNx)
        indy=np.mod(indy,NJNy)
        if(jntype=='2d'):
            jnreg=NJNy*indx+indy
        elif(jntype=='3d'):
            indz=np.floor(NJNz*mat[:,2]/args.Lbox)
            indz=np.mod(indz,NJNz)
            jnreg=NJNz*(NJNy*indx+indy)+indz

        #convert index to integers
        jnreg=jnreg.astype(int)
        #print('jnreg:',jnreg.min(),jnreg.max(),jnreg.mean(),jnreg)

    return jnreg

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    #weights=fH['fits'][1]['wt'][indsel]
    
    if(fH['prop']['wtag'] in ['w1','w1-pip','w1-iip']):
        weights=np.ones(indsel.size)
    elif(fH['prop']['wtag'] in ['wsys', 'wsys-pip','wsys-iip']):
        weights=fH['fits'][1]['WEIGHT_SYSTOT'][indsel]*fH['fits'][1]['WEIGHT_NOZ'][indsel]
        weights=weights*fH['fits'][1]['WEIGHT_NOZ']['WEIGHT_FKP'][indsel]
        if(fH['typein']=='data'):
            weights=weights*fH['fits'][1]['WEIGHT_CP'][indsel]

    return weights

def GetXYZ(fH,indsel,args):
    '''Load the appropriate xyz after needed transforms'''
    
    if(fH['typein']=='data'):
        if('halo' in fH['prop']['tracer']):
            xyz=fH['fits'][1]['x_L2com'][indsel]
        else:
            xyz=np.column_stack([fH['fits'][1]['x'][indsel],
              fH['fits'][1]['y'][indsel],fH['fits'][1]['z'][indsel]])
    elif(fH['fname'] in ['generate','norand']):
        nrand=indsel.size
        xyz=np.random.random((nrand,3))*args.Lbox

    return xyz


def LoadData_part(fH,I1,I2,args):
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]

   xyz=GetXYZ(fH,indcut,args)
       
   #set weights =1 for now
   data=np.column_stack([xyz,np.ones(xyz.shape[0])])


   if(args.njn>0):
      JN_reg=GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   #if pip weight is needed then evaluate pip wieght
   if(args.pip==True or args.iip==True):
       if(fH['typein']=='data'):
           fib_full=fH['fits'][1]['WEIGHT_BW'][:]
           #fib_full=fib_full.astype(int)
           #apply cut
           fib=np.ascontiguousarray(fib_full[indcut],dtype=np.int)
       else:#(fH['typein']=='rand' or 'pip'):
           fib=np.ascontiguousarray(np.array([]),dtype=np.int)

       return data_c, fib

   return  data_c
