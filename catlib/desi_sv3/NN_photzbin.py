#Author: Shadab Alam : 30 April 2021
#This assign photozbin for sv3 target samples

import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
import pickle


def get_X(tdic,feature_list=[]):
    '''returns the X'''

    for ff,feat in enumerate(feature_list):
        if('FLUX' in feat):
            tval=tdic[feat]
        elif('MAG' in feat):
            tval=22.5-2.5*np.log10(tdic['FLUX_'+feat[4:]])
        else:
            print('Invalid feature (in function get_XY): %s'%feat)
            return

        if(ff==0):
            X=tval
        else:
            X=np.column_stack([X,tval])

    return X

def get_X_fits(fin,indsel,feature_list=[]):
    '''returns the X'''

    for ff,feat in enumerate(feature_list):
        if('FLUX' in feat):
            tval=fin[1][feat][indsel]
        elif('MAG' in feat):
            tval=22.5-2.5*np.log10(fin[1]['FLUX_'+feat[4:]][indsel])
        else:
            print('Invalid feature (in function get_XY): %s'%feat)
            return

        if(ff==0):
            X=tval
        else:
            X=np.column_stack([X,tval])

    return X


def get_Y(tdic,red_splits=[]):
    red=tdic['redshift']
    Y=np.zeros(red.size)
    ymap={}
    for ss in range(0,red_splits.size-1):
        ind=(red>=red_splits[ss])*(red<red_splits[ss+1])
        Y[ind]=ss

    return Y

def get_ymap(red_splits=[]):
    ymap={}
    for ss in range(0,red_splits.size-1):
        ymap[ss]='%4.2f-%4.2f'%(red_splits[ss],red_splits[ss+1])
    return ymap


def get_XY(tdic,feature_list=[],red_splits=[]):

    X=get_X(tdic,feature_list=feature_list)
    Y=get_Y(tdic,red_splits=red_splits)
    ymap=get_ymap(red_splits=red_splits)

    return X , Y , ymap


def best_fit_MLP(X,y):
    clf_dic={}
    score_dic={}
    activation_list=['identity', 'logistic', 'tanh', 'relu']
    alpha_list=[1e-4,1e-3,1e-2,1]

    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y,
                                                    random_state=1)

    for act in activation_list:
        clf_dic[act]={}
        score_dic[act]={}
        for alpha in alpha_list:
            clf_dic[act][alpha] = MLPClassifier(random_state=1, max_iter=300,activation=act,alpha=alpha).fit(X_train, y_train)
            score_dic[act][alpha]=clf_dic[act][alpha].score(X_test, y_test)
            print(' ** %s, alpha=%f , score=%f '%(act,alpha,score_dic[act][alpha]))

    return clf_dic,score_dic

def get_redsplit(tracer):
    redsplit_dic={'LRG':np.array([0,0.4,0.6,0.8,0.9,1.0,2.0]),
           'BGS_ANY':np.array([0,0.2,0.3,0.4,0.5]),
           'ELG':np.array([0,1.0,1.4,2.0]),
           'QSO':np.array([0,0.8,1.2,1.6,2.1]),
          }

    return redsplit_dic[tracer]

def get_zbin_pred(model,flux_dic):
    X=get_X(flux_dic,feature_list=model['feat'])
    ymap=get_ymap(red_splits=model['red_split'])

    #use the Neural-Network model to predict quantities
    ypred=model['NNmodel'].predict(X)

    return ypred, ymap

def select_zbin(model_file,fin,indsel,zbin=0):
    '''selects objects for a particular photoz-bin'''

    #load the model
    model=pickle.load(open(model_file, 'rb'))

    #generate the X for predictions
    X=get_X_fits(fin,indsel,feature_list=model['feat'])

    #generate prediction
    ypred=model['NNmodel'].predict(X)

    ind=ypred==zbin

    return indsel[ind]


