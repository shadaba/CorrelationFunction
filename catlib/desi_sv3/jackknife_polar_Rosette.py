#Author: Shadab Aalm
#contains functions to assign Rosette and generate jackknife regions 
import numpy as np

def load_tile(tile_file):
    
    dtype_dic={'int':['TILEID', 'PASS' ],
               'bool':['IN_DESI',    'AVAILABLE'],
              'float':['RA', 'DEC','PRIORITY','EBV_MED','DESIGNHA', 'DONEFRAC'],
              'str':['PROGRAM','STATUS']}
    
    tlines=open(tile_file).readlines()
    tile={}
    for tt,tline in enumerate(tlines):
        if(tline[0]=='#'):
            continue

        if(tile=={}):
            head=tline.split()
            for hh in head:
                tile[hh]=[]
            continue

        tspl=tline.split()
        for hh,th in enumerate(head):
            if(th in dtype_dic['int']):
                tile[th].append(np.int(tspl[hh]))
            elif(th in dtype_dic['bool']):
                tile[th].append(np.bool(tspl[hh]))
            elif(th in dtype_dic['float']):
                tile[th].append(np.float(tspl[hh]))
            else:
                tile[th].append(tspl[hh])

    for hh,th in enumerate(head):
        tile[th]=np.asarray(tile[th])

    return tile

def club_tiles_by_Rosette(tiles,program='DARK'):
    '''Algo: select all the tiles in pass0,
    This define the unique Rosette,\n",
    find all tiles within few small seperation (0.5) of tile in pass0
    They all belong to same Rosette'''


    #get xyz for all the tikes
    XYZ_tiles=RADEC_to_XYZ(tiles['RA'],tiles['DEC'],radius=1)

    #choose a program
    indprog=np.where(tiles['PROGRAM']==program)[0]

    indsel=np.where(tiles['PASS']==0)[0]

    rosette={}
    rose_id=-1
    for ii in range(0,indsel.size):
        if(indsel[ii] not in indprog):
            continue
        rose_id=rose_id+1   
        rosette[rose_id]={'tiles':[tiles['TILEID'][indsel[ii]]],'tiles_index':[indsel[ii]]}
        if(True): #actual angular distance
            xyz_this=XYZ_tiles[indsel[ii],:]
            diff=np.arccos(np.sum(XYZ_tiles*xyz_this[None,:],axis=1))
            ind_this=np.where(diff<0.5*np.pi/180.0)[0] #distance cut at 0.5 degree
        else: #approximate distance
            diff=np.sqrt(np.power(tiles['RA']-tiles['RA'][indsel[ii]],2)+np.power(tiles['DEC']-tiles['DEC'][indsel[ii]],2))
            ind_this=np.where(diff<0.5)[0]

        for jj in ind_this:
            if(jj==indsel[ii] or jj not in indprog):
                continue
            else:
                rosette[rose_id]['tiles'].append(tiles['TILEID'][jj])
                rosette[rose_id]['tiles_index'].append(jj)
                
        #determine the mean RA and DEC of each Rosette
        for tt,tkey in enumerate(rosette.keys()):
            tile_in=rosette[tkey]['tiles_index']
            rosette[tkey]['RA']=np.mean(tiles['RA'][tile_in])
            rosette[tkey]['DEC']=np.mean(tiles['DEC'][tile_in])
            rosette[tkey]['XYZ']=RADEC_to_XYZ(rosette[tkey]['RA'],rosette[tkey]['DEC'],radius=1)
            

    return rosette


def RADEC_to_XYZ(RA,DEC,radius=1):
    ngal=RA.size
    XYZ=np.zeros(ngal*3).reshape(ngal,3)


    theta=np.pi*(90-DEC)/180
    phi  =np.pi*RA/180

    XYZ[:,0]=radius*np.sin(theta)*np.cos(phi)
    XYZ[:,1]=radius*np.sin(theta)*np.sin(phi)
    XYZ[:,2]=radius*np.cos(theta)

    if(ngal==1):
        XYZ=XYZ[0,:]
        
    return XYZ


def broad_cast_split(nload,max_load=50000):
    '''splits the load in chunks'''
    split_dic={}
    if(nload<max_load):
        split_dic[0]=[0,nload]
    else:
        spl=np.int(0)
        while(True):
            imin=np.int(spl*max_load)
            imax=min(np.int((spl+1)*max_load),nload)
            split_dic[spl]=[imin,imax]
            spl=spl+1
            
            if(imax>=nload):
                break
                
    return split_dic

def assign_rosette(fin,indsel,rosette,max_load=50000):
    '''max_load decided the boradcasting limit'''
    XYZ=RADEC_to_XYZ(fin[1]['RA'][indsel],fin[1]['DEC'][indsel],radius=1)

    #collect the xyz of rosettes
    nrose=len(rosette.keys())
    for ii in range(0,nrose):
        if(ii==0):
            xyz_rose=np.zeros((nrose,3))
        xyz_rose[ii,:]=rosette[ii]['XYZ']

    #compute the theta by broadcasting
    split_dic=broad_cast_split(XYZ.shape[0],max_load=max_load)
    theta=np.zeros((XYZ.shape[0],nrose))
    for spl in range(0,len(split_dic.keys())):
        imin=split_dic[spl][0]
        imax=split_dic[spl][1]
        theta[imin:imax,:]=np.arccos(np.sum(XYZ[imin:imax,None,:]*xyz_rose[None,:,:],axis=2))
        
    #find the rose index
    ind_rose=np.argmin(theta,axis=1)

    return ind_rose

def generate_jackknife(frand,indsel,rosette,njn_per_rose=4,ind_rose=None):
    '''This generate the jacknife
    1) Assign the rosette number for each random
    2) For each rosetter split the jacknife based on polar angle'''
    
    if(ind_rose is None):
        ind_rose=assign_rosette(frand,indsel,rosette)
    
    #percentile needed for the jn edged
    ptiles=[0]
    for ii in range(1,njn_per_rose+1):
        ptiles.append(ii*(100.0/njn_per_rose))

    nrose=len(rosette.keys())
    
    rose_jn_dic={}
    
    jnid=np.zeros(indsel.size)-1
    nvalid_rose=0
    for rose_id in range(0,nrose):
        ind_this=np.where(ind_rose==rose_id)[0]
        if(ind_this.size==0):
            continue

        dRA=frand[1]['RA'][indsel[ind_this]]-rosette[rose_id]['RA']
        dDEC=frand[1]['DEC'][indsel[ind_this]]-rosette[rose_id]['DEC']

        phi=np.arctan2(dDEC,dRA)# dDEC[ind_nozero]/np.sqrt(ddist[ind_nozero]))
                        
        phi_bins=np.percentile(phi,ptiles)
        #change the limits to make it complete circle
        phi_bins[0]=-1.001*np.pi
        phi_bins[-1]=1.001*np.pi

        rose_jn_dic[rose_id]={'phib_bins':phi_bins,'jnid':np.zeros(phi_bins.size-1,dtype=int)}
        for tjn in range(0,njn_per_rose):
            ind_jn=(phi>=phi_bins[tjn])*(phi<phi_bins[tjn+1])
            
            t_jnid=nvalid_rose*njn_per_rose+tjn
            jnid[ind_this[ind_jn]]=t_jnid
            rose_jn_dic[rose_id]['jnid'][tjn]=t_jnid
        nvalid_rose=nvalid_rose+1

    return jnid, rose_jn_dic


def assign_jackknife(fin,indsel,rosette,rose_jn_dic,ind_rose=None):
    '''This assigns the jackknife regions the jacknife
    1) For each rosetter split the jacknife based on polar angle
    '''
    
    if(ind_rose is None):
        ind_rose=assign_rosette(fin,indsel,rosette)
        
    nrose=len(rosette.keys())
    jnid=np.zeros(indsel.size)-1
    cur_jn=0
    
    for rose_id in range(0,nrose):
        ind_this=np.where(ind_rose==rose_id)[0]
        if(ind_this.size==0):
            continue
        dRA=fin[1]['RA'][indsel[ind_this]]-rosette[rose_id]['RA']
        dDEC=fin[1]['DEC'][indsel[ind_this]]-rosette[rose_id]['DEC']

        phi=np.arctan2(dDEC,dRA)# dDEC[ind_nozero]/np.sqrt(ddist[ind_nozero]))
                        
        phi_bins=rose_jn_dic[rose_id]['phib_bins']
        
        for tjn in range(0,phi_bins.size-1):
            ind_jn=(phi>=phi_bins[tjn])*(phi<phi_bins[tjn+1])
            jnid[ind_this[ind_jn]]=rose_jn_dic[rose_id]['jnid'][tjn]
    
    return jnid

def plot_rosette(fin,indsel,rose_id=0,ind_rose=None,sel_id=None,outfile=''):
    import pylab as pl
    dcols=['r','b','g','purple','magenta','cyan','yellow','violet','pink','skyblue']
    ncolor=len(dcols)

    if(ind_rose is None):
        ind_rose=assign_rosette(fin,indsel,rosette)

    indsel_rose=np.where(ind_rose==rose_id)[0]
    if(indsel.size==0):
        return

    if(sel_id is None):
        pl.plot(fin[1]['RA'][indsel_rose],fin[1]['DEC'][indsel_rose],'.',color=dcols[rose_id%ncolor],alpha=0.01)
    else:
        this_sel=sel_id[indsel_rose]
        usel=np.unique(this_sel)

        ra=fin[1]['RA'][indsel_rose]
        dec=fin[1]['DEC'][indsel_rose]

        for uu, uv in enumerate(usel):
            tid=this_sel==uv
            tcol=dcols[(rose_id+uu)%ncolor]
            pl.plot(ra[tid],dec[tid],'.',color=tcol,alpha=0.01)

    pl.plot(rosette[rose_id]['RA'],rosette[rose_id]['DEC'],'ko',markersize=2)
    if(outfile!=''):
        pl.savefig(outfile)

    return

