import numpy as np
import pylab as pl
import mycosmo as mcosm
import fitsio as F

import time
#need for ckdtree calculations to get mask
from scipy import spatial

#utility function
import util

#impot desi_mask
from desitarget.sv3.sv3_targetmask import desi_mask

import os
import sys

sys.path=['/global/homes/s/shadaba/Projects/shadab_pythonlib/']+sys.path
import dump_dictionary as dumpdic

import importlib

def cutphotmask(fin,indsel,bits):
    t1 = time.time()
    keep = (fin[1]['NOBS_G'][indsel]>0) & (fin[1]['NOBS_R'][indsel]>0) & (fin[1]['NOBS_Z'][indsel]>0)
    print('time Nobs_G:',np.int(time.time() - t1))
    
    maskbit=fin[1]['MASKBITS'][indsel]
    for biti in bits:
        keep &= ((maskbit & 2**biti)==0)
        
    print('time vetomask:',np.int(time.time() - t1))
    print('Befor after Imaging veto: %d/%d '%(keep.size,np.sum(keep)))
    
    return indsel[keep] 


def get_split_mask(datafile,findat,indsel_dat,randfile,finran,indsel_ran,args,IPdict):
    '''split the footprint in 4 parts and remove the large scale clustering'''
    
    t1 = time.time()
    
    mask_set=None
    for mask  in ['mask00','mask10','mask01','mask11']:
        if(mask in IPdict['mask']):
            mask_set=mask
            break
    assert(mask_set is not None)

    #apply mask in data
    maskfile=datafile+'_mask'
    mask_dic=dumpdic.dump_load_dic_fits(maskfile,verbose=0,tabin='',nodic=0)
    indsel_dat=indsel_dat[mask_dic[mask_set][indsel_dat]]
    

    #apply mask in random
    maskfile=randfile+'_mask'
    mask_dic=dumpdic.dump_load_dic_fits(maskfile,verbose=0,tabin='',nodic=0)
    indsel_ran=indsel_ran[mask_dic[mask_set][indsel_ran]]
    print('time applying the mask:',np.int(time.time() - t1))
    
    print('Warning: masking field with %s '%mask_set)
   
    #need to reset the njn for masked cases
    JNfile=[args.jnfile ]
    jnreg_data=util.compute_JN_data(findat[1]['RA'][indsel_dat],findat[1]['DEC'][indsel_dat],JNfile)
    
    jnreg=remap_jnreg(jnreg_data,args,overwrite=True)
    print('Updating njn from %d to %d'%(args.njn,jnreg.max()+1)) 
    args.njn=jnreg.max()+1
    print('time after remapping jnreg:',np.int(time.time() - t1))


    #if subsampling is needed
    if('subrand' in IPdict['mask']):
        print('subsampling randoms')

        #first get the jnreg for randoms
        jnreg_ran=util.compute_JN_data(finran[1]['RA'][indsel_ran],finran[1]['DEC'][indsel_ran],JNfile)
        indsel_ran,reg_sel=subsamp_patches(indsel_ran,reg_rand=jnreg_ran,reg_data=jnreg_data,randfactor=args.randfactor)
        print('time after patch subsampling:',np.int(time.time() - t1))

    return indsel_dat,indsel_ran

def subsamp_patches(index_rand,reg_rand=None,reg_data=None,randfactor=1.0):
    '''subsample the random patch by patch
    1) unique regions in data
    2) make histogram of data unique region
    3) make histogram of randoms
    4) take the ratio and find the maximum randoms we can have
    5) subsample the randoms'''

    uid_data=np.unique(reg_data)
    uid_rand=np.unique(reg_rand)

    bins=np.linspace(0,uid_data.max()+1,uid_data.max()+2)-0.5

    hc_data,_=np.histogram(reg_data,bins=bins)
    hc_rand,_=np.histogram(reg_rand,bins=bins)


    in_nozero=hc_data>0
    max_rfact=np.min(hc_rand[in_nozero]/hc_data[in_nozero])

    if(max_rfact<randfactor):
        randfactor=max_rfact
        print('Can only use %4.2f as randfactor'%randfactor)


    trand=np.random.random(index_rand.size)
    fsub_arr=np.zeros(index_rand.size)

    for uu,uid in enumerate(uid_data):
        need_rand=randfactor*hc_data[uid]
        fsub=1.0*need_rand/hc_rand[uid]
        tind=reg_rand==uid

        fsub_arr[tind]=fsub

    #print(fsub_arr)
    tind=trand<fsub_arr

    index_sel=index_rand[tind]
    reg_sel=reg_rand[tind]

    return index_sel,reg_sel

def get_quant(quant,flux_dic={},fin=None,indsel=None,usefits=False):
    if('MAG' in quant):
        band=quant.split('_')[1]
        qneed=['FLUX_'+band]
        if('MAGE' in quant):#extinction correction is also applied
            qneed=qneed+['MW_TRANSMISSION_'+band]
    elif('color' in quant):
        if('colorE' in quant):
            tcl=quant[6:].split('-')
        else:
            tcl=quant[5:].split('-')
        qneed=['FLUX_'+tcl[0]]
        if('colorE' in quant): #for extinction correction
            qneed=qneed+['MW_TRANSMISSION_'+tcl[0]]
        if(len(tcl)>1):
            qneed=qneed+['FLUX_'+tcl[1]]
            if('colorE' in quant): #for extinction correction
                qneed=qneed+['MW_TRANSMISSION_'+tcl[1]]
    else:
        qneed=[quant]


    qout=[]
    for qq in qneed:
        if(usefits):
            if(qq in fin[1][0].dtype.names):
                tq=fin[1][qq][indsel]
            else:
                print('Error:Invalid feature (in function get_quant: fits): %s'%qq)
        else:
            if(qq in flux_dic.keys()):
                tq=flux_dic[qq]
            else:
                print('Error:Invalid feature (in function get_quant: flux_dic): %s'%qq)

        qout.append(tq)

    if('MAG' in quant or ('color' in quant and len(qout)==1)):
        if('MAGE' in quant or 'colorE' in quant):
            flux=qout[0]/qout[1] #extinction coorection
        else:
            flux=qout[0]
        qout=22.5-2.5*log10_arr(flux,tag=quant)
    elif('color' in quant):
        if('colorE' in quant):
            qout=-2.5*(log10_arr(qout[0]/qout[1],tag=quant) -log10_arr(qout[2]/qout[3],tag=quant) )
        else:
            qout=-2.5*(log10_arr(qout[0],tag=quant) -log10_arr(qout[1],tag=quant) )
    elif(quant=='PHOTSYS'):
        indN=qout[0]=='N'
        qout=np.zeros(indN.size,dtype=int)
        qout[indN]=1
    else:
        qout=qout[0]

    return qout


def log10_arr(arrin,verbose=True,tag=''):
    '''Takes the log of array after setting'''
    ind=arrin<=0
    arrout=np.zeros(ind.size)
    arrout[~ind]=np.log10(arrin[~ind])
    arrout[ind]=arrout[~ind].mean()
    if(verbose and ind.sum()>0):
        print('%s %d/%d set to mean due to negative values'%(tag,ind.sum(),ind.size))

    return arrout




def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''

    t1 = time.time()
    #mask bits to be applied to data and randoms
    bits=[1,5,6,7,8,9,11,12,13]
    selkey='1>0'

    findat=F.FITS(datafile)
    indsel_dat=findat[1].where(selkey)

    if(args.selection!='full'):
        #apply magnitude based selection
        tspl=args.selection.split('-')
        quant=tspl[0]
        if(len(tspl)==2):
            quant_lim=np.float(tspl[1])
        else:
            quant_lim=[np.float(tspl[1]),np.float(tspl[2])]

        qq=get_quant(quant,flux_dic={},fin=findat,indsel=indsel_dat,usefits=True)
        if(len(quant_lim)==2):
           ind=(qq>quant_lim[0])*(qq<quant_lim[1])
        else:   
           ind=(qq>quant_lim[0])
        ngal_bef=indsel_dat.size
        indsel_dat=indsel_dat[ind]
        print('selection: %s bef/after %d/%d'%(args.selection,ngal_bef,indsel_dat.size))


    finran=F.FITS(randfile)
    indsel_ran = finran[1].where(selkey) #select everything now
   
    #Applying Imaging veto mask
    indsel_dat=cutphotmask(findat,indsel_dat,bits) #imaging veto mask
    print('time data cutphotomask:',np.int(time.time() - t1))
    indsel_ran = cutphotmask(finran,indsel_ran,bits) #imaging veto mask
    print('time random cutphotomask:',np.int(time.time() - t1))

    #apply photz redshift selection
    if('tomo_' in args.selection or 'tomo' in IPdict['mask']):
        if('tomo_' in args.selection):
            tspl=args.selection.split('_')
        else:
            tspl=IPdict['mask'].split('_')
        tomo_sel=np.int(tspl[2])
        tomoversion=tspl[1]
        tomo_file=datafile+'_SAtomo/tomobin_%s.fits'%(tomoversion)
        with F.FITS(tomo_file) as fin:
            zbin=fin[1]['tomobin'][indsel_dat]
            
        print('Loaded tomographic bins from: ',tomo_file)
        ngal_bef=indsel_dat.size

        indsel_dat=indsel_dat[zbin==tomo_sel]
        print('Applying photz_bin selection with zbin:%d'%(tomo_sel))
        print('ngal Before/After photoz selection: %d/%d'%(ngal_bef,indsel_dat.size))


    #print('mask: ',IPdict['mask'])
    #if(IPdict['mask']!=None and IPdict['mask']!=''):
    #    indsel_dat,indsel_ran=get_split_mask(datafile,findat,indsel_dat,randfile,finran,indsel_ran,args,IPdict)
    #    print('time applying masks:',np.int(time.time() - t1))
    
    need_rand=args.randfactor*indsel_dat.size
    if(args.randfactor>0 and indsel_ran.size>need_rand):
        #need to subsample the random
        np.random.seed(0)
        indsel_ran=np.random.choice(indsel_ran,need_rand,replace=False)
        #sort the indices
        indsel_ran.sort()

    print('time random subsample:',np.int(time.time() - t1))

    return findat,indsel_dat,finran,indsel_ran

def remap_jnreg(jnreg,args,overwrite=False):
    '''This remaps the jnreg considering it not contiguous and also handles a -1 case'''

    jnmap_file=args.outfile+'.jnremap'
    if(not os.path.isfile(jnmap_file or overwrite==True)):
        ujn=np.unique(jnreg)
        ujn=np.sort(ujn)

        if(ujn[0]==-1):#incsase some point do not have a valid jn region
            ujn=ujn[1:]

        #save this map so that random call can use the same map
        jnmap_arr=np.column_stack([ujn,np.arange(0,ujn.size)])
        np.savetxt(jnmap_file,jnmap_arr,fmt='%d')
    else:
        print('only loading: ',jnmap_file)
        jnmap_arr=np.loadtxt(jnmap_file,dtype=int)

    #generate new array handle -1 and then remap everything else
    new_jn=np.zeros(jnreg.size,dtype=int)
    new_jn[jnreg==-1]=-1
    
    for ii in range(0,jnmap_arr.shape[0]):
        tid=jnreg==jnmap_arr[ii,0]
        new_jn[tid]=jnmap_arr[ii,1]

    return new_jn

def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in prop dic in fH'''
    
    #takes a list of jn files: list is important
    JNfile=[args.jnfile ]
    jnreg=util.compute_JN_data(fH['fits'][1]['RA'][indsel],fH['fits'][1]['DEC'][indsel],JNfile)
  
    if(fH['prop']['mask']!=None and fH['prop']['mask']!=''):
       jnreg=remap_jnreg(jnreg,args)

    return jnreg

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    #weights=fH['fits'][1]['wt'][indsel]
    
    if(fH['prop']['wtag'] in ['w1','w1-pip','w1-iip'] or fH['typein']=='rand'):
        weights=np.ones(indsel.size)
    elif(fH['prop']['wtag'] in ['EdWsysv0','MrWsysv0']):
        if('EdWsys' in fH['prop']['wtag']):
            wname='EdWsys'
        elif('MrWsys' in fH['prop']['wtag']):
            wname='MrWsys'

        sysversion=fH['prop']['wtag'][6:]
        wsys_file=fH['fname']+'_%s/wsys_%s.fits'%(wname,sysversion)
        with F.FITS(wsys_file) as fin:
            weights=fin[1]['wsys'][indsel]
        print('Loaded systematic weights from: ',wsys_file)
    elif(fH['prop']['wtag'] in ['wsys', 'wsys-pip','wsys-iip']):
        weights=fH['fits'][1]['WEIGHT_SYSTOT'][indsel]*fH['fits'][1]['WEIGHT_NOZ'][indsel]
        weights=weights*fH['fits'][1]['WEIGHT_NOZ']['WEIGHT_FKP'][indsel]
        if(fH['typein']=='data'):
            weights=weights*fH['fits'][1]['WEIGHT_CP'][indsel]

    return weights

def GetXYZ(fH,indsel,args):
    '''Load the appropriate xyz after needed transforms'''

    zcat=np.random.random(indsel.size)+0.1 #setting all redshift to 1 as these are imaging cat before redshift
    xyz, interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indsel],
            fH['fits'][1]['DEC'][indsel],zcat,
            args.H0,args.omM,1.0-args.omM,interp='')
    return xyz

def LoadData_part(fH,I1,I2,args):
   t1 = time.time()
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]

   #read co-ordinate and redshift
   cat_z=np.random.random(indcut.size)+0.1

   #checking if redshift is nan
   ind_nan=cat_z!=cat_z
   if(np.sum(ind_nan)>0):
       print(' Warning: Found %d redshift as nan, setting them to mean redshift,ok for angular cluatering not for 3d'%ind_nan.sum())
       cat_z[ind_nan]=cat_z[~ind_nan].mean()
       assert(args.sampmode in [6,7]) #must be angular clustering

   #check for negative redshift
   ind_neg=cat_z<=0
   if(ind_neg.sum()>0):
       print(' Warning: Found %d negative redshift, setting them to mean redshift, ok for angular clustering not for 3d'%ind_neg.sum())
       cat_z[ind_neg]=cat_z[~ind_neg].mean()
       assert(args.sampmode in [6,7])  #must be angular clustering

   XYZ,interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indcut],fH['fits'][1]['DEC'][indcut],
           cat_z,args.H0,args.omM,1-args.omM,interp='')

   print('time reading RA,DEC and getting xyz:',np.int(time.time() - t1))

   #set weights =1 for now
   weights=GetWeights(fH,indcut)
   data=np.column_stack([XYZ,weights])


   if(args.njn>0):
      JN_reg=GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   #if pip weight is needed then evaluate pip wieght
   if(args.pip==True or args.iip==True):
       if(fH['typein']=='data'):
           fib_full=fH['fits'][1]['WEIGHT_BW'][:]
           #fib_full=fib_full.astype(int)
           #apply cut
           fib=np.ascontiguousarray(fib_full[indcut],dtype=np.int)
       else:#(fH['typein']=='rand' or 'pip'):
           fib=np.ascontiguousarray(np.array([]),dtype=np.int)

       return data_c, fib
   
   print('time reading Finished loadpart :',np.int(time.time() - t1))

   return  data_c
