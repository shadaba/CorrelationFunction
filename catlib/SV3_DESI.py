import numpy as np
import mycosmo as mcosm
import fitsio as F

#utility function
import util


def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''

    if(args.z1z2[0]>0 and args.z1z2[1]>args.z1z2[0]):
        Zsel="Z_COSMO >= "+str(args.z1z2[0])+" && Z_COSMO <"+str(args.z1z2[1])
    else:
        print('*** No redshift slection is applied, if this is unexpected then check z1z2 entries')
        Zsel='1>0'



    tile_center,tile_rad2=get_tile_centers(args)
    findat=F.FITS(datafile)
    finran=F.FITS(randfile)

    for ff,fin in enumerate([findat,finran]):
        indsel = fin[1].where(Zsel)
        print(indsel.size)
        if(ff==1 and False):#subsampling for randoms
            print(indsel_dat.size)
            if(args.randfactor>0 and indsel.size>args.randfactor*indsel_dat.size):
                #need to subsample the random
                need_rand=args.randfactor*indsel_dat.size
                np.random.seed(0)
                indsel=np.random.choice(indsel,need_rand,replace=False)
                #sort the indices
                indsel.sort()

        if(True):
            ra_dec=np.column_stack([fin[1]['RA'][indsel],fin[1]['DEC'][indsel]])
            indAll=np.zeros(ra_dec.shape[0],dtype=bool)
            for tt in range(0,tile_center.shape[0]):
                this_tile=tile_center[tt,:]
                dRA_DEC=ra_dec-this_tile[None,:]
                ind_this=np.sum(np.power(dRA_DEC,2),axis=1)<tile_rad2
                indAll=indAll+ind_this

                print(ff,tt,ind_this.sum())
        else:
            for tt in range(0,tile_center.shape[0]):
                this_tile=tile_center[tt,:]
                tsel='(RA-%f)*(RA-%f)+(DEC-%f)*(DEC-%f) < %f'%(this_tile[0],this_tile[0],this_tile[1],this_tile[1],tile_rad2)
                this_sel=fin[1].where(Zsel+' && '+tsel)
                if(tt==0):
                    indsel=this_sel
                else:
                    indsel=np.append(indsel,this_sel)
                print(tt,this_sel.size)

        print('All',indAll.sum(),indAll.sum())
        if(ff==0):
            indsel_dat=indsel[indAll]
            indsel_dat.sort()
        else:
            indsel_ran=indsel[indAll]
            indsel_ran.sort()


    return findat,indsel_dat,finran,indsel_ran

def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in prop dic in fH'''

    tile_center,tile_rad2=get_tile_centers(args)

    #takes a list of jn files: list is important
    ra_dec=np.column_stack([fH['fits'][1]['RA'][indsel],fH['fits'][1]['DEC'][indsel]])
    jnreg=np.zeros(indsel.size,dtype=int)

    for tt in range(0,tile_center.shape[0]):
        this_tile=tile_center[tt,:]
        dRA_DEC=ra_dec-this_tile[None,:]
        ind_this=np.sum(np.power(dRA_DEC,2),axis=1)<tile_rad2

        q1=(dRA_DEC[:,0]>=0)*(dRA_DEC[:,1]>=0)
        q2=(dRA_DEC[:,0]>0)*(dRA_DEC[:,1]<0)
        q3=(dRA_DEC[:,0]<0)*(dRA_DEC[:,1]>0)
        q4=(dRA_DEC[:,0]<0)*(dRA_DEC[:,1]<0)

        jnreg[ind_this*q1]=4*tt+0
        jnreg[ind_this*q2]=4*tt+1
        jnreg[ind_this*q3]=4*tt+2
        jnreg[ind_this*q4]=4*tt+3

    return jnreg

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    #weights=fH['fits'][1]['wt'][indsel]

    if(fH['prop']['wtag'] in ['w1','w1-pip','w1-iip']):
        weights=np.ones(indsel.size)
    elif(fH['prop']['wtag'] in ['wsys', 'wsys-pip','wsys-iip']):
        weights=fH['fits'][1]['WEIGHT_SYSTOT'][indsel]*fH['fits'][1]['WEIGHT_NOZ'][indsel]
        weights=weights*fH['fits'][1]['WEIGHT_NOZ']['WEIGHT_FKP'][indsel]
        if(fH['typein']=='data'):
            weights=weights*fH['fits'][1]['WEIGHT_CP'][indsel]

    return weights

def LoadData_part(fH,I1,I2,args):
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]

   #read co-ordinate and redshift
   cat_z=fH['fits'][1]['Z_COSMO'][indcut]

   #checking if redshift is nan
   ind_nan=cat_z!=cat_z
   if(np.sum(ind_nan)>0):
       print(' Warning: Found %d redshift as nan, setting them to mean redshift,ok for angular cluatering not for 3d'%ind_nan.sum())
       cat_z[ind_nan]=cat_z[~ind_nan].mean()
       assert(args.sampmode in [6,7]) #must be angular clustering

   #check for negative redshift
   ind_neg=cat_z<=0
   if(ind_neg.sum()>0):
       print(' Warning: Found %d negative redshift, setting them to mean redshift, ok for angular clustering not for 3d'%ind_neg.sum())
       cat_z[ind_neg]=cat_z[~ind_neg].mean()
       assert(args.sampmode in [6,7])  #must be angular clustering

   XYZ,interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indcut],fH['fits'][1]['DEC'][indcut],
           cat_z,args.H0,args.omM,1-args.omM,interp='')

   #set weights =1 for now
   weights=GetWeights(fH,indcut)
   data=np.column_stack([XYZ,weights])


   if(args.njn>0):
      JN_reg=GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   #if pip weight is needed then evaluate pip wieght
   if(args.pip==True or args.iip==True):
       if(fH['typein']=='data'):
           fib_full=fH['fits'][1]['WEIGHT_BW'][:]
           #fib_full=fib_full.astype(int)
           #apply cut
           fib=np.ascontiguousarray(fib_full[indcut],dtype=np.int)
       else:#(fH['typein']=='rand' or 'pip'):
           fib=np.ascontiguousarray(np.array([]),dtype=np.int)

       return data_c, fib

   return  data_c
