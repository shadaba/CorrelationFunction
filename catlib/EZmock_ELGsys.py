import numpy as np
import pylab as pl
import mycosmo as mcosm
import fitsio as F

#need for ckdtree calculations to get mask
from scipy import spatial

#utility function
import util

import sys
sys.path=['/global/homes/s/shadaba/Projects/shadab_pythonlib/']+sys.path
import dump_dictionary as dumpdic

def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''

    if(args.z1z2[0]>0 and args.z1z2[1]>args.z1z2[0]):
        Zsel="Z >= "+str(args.z1z2[0])+" && Z <"+str(args.z1z2[1])
    else:
        print('*** No redshift slection is applied, if this is unexpected then check z1z2 entries')
        Zsel='1>0'


    if(IPdict['sky']=='NDECALS'):
         selkey=Zsel+" && (ra>=80 && ra<=300 && dec<34.7) "
    elif(IPdict['sky']=='SDECALS'):
         selkey=Zsel+" && ( (ra<80 || ra>300) && dec<34.7) "
    elif(IPdict['sky']=='NBMZLS'):
         selkey=Zsel+" && (ra>=80 && ra<=300 && dec>34.7) "
    else:
        print('Error: Invalid sky :',IPdict['sky'])
        sys.exit()

    findat=F.FITS(datafile)
    indsel_dat=findat[1].where(selkey)
    
    if(args.selection=='sub'):
        ngal_before=indsel_dat.size
        print('using sub_file:',args.sub_file)
        sub_dic=dumpdic.dump_load_dic_fits(args.sub_file)
        #sub_dic=np.load(args.subfile, allow_pickle=True)
        #apply index selection
        indsel_dat=indsel_dat[sub_dic['bool_index'][indsel_dat]]
        print('Before/After sub:',ngal_before,indsel_dat.size)

    finran=F.FITS(randfile)
    indsel_ran = finran[1].where(selkey) #select everything now

    need_rand=args.randfactor*indsel_dat.size
    if('subrand' in IPdict['mask']):
        need_rand=np.int(1.5*need_rand)
   
    if(args.randfactor>0 and indsel_ran.size>need_rand):
        #need to subsample the random
        np.random.seed(0)
        indsel_ran=np.random.choice(indsel_ran,need_rand,replace=False)
        #sort the indices
        indsel_ran.sort()


    #applying the mask
    if(IPdict['mask']!=''):
        mask_set=None
        for mask  in ['mask00','mask10','mask01','mask11']:
            if(mask in IPdict['mask']):
                mask_set=mask
                break
        assert(mask_set is not None)

        print('Warning: masking 3 degree field but 6 degree will get the cross-pair')

        #apply the mask to remove cross-pairs
        ind_this,reg_data=remove_cross_pair(findat[1]['RA'][indsel_dat],findat[1]['DEC'][indsel_dat],
                mask=mask_set,delta_ra=5,delta_dec=5,distance_upper_bound=50,eps=0,nproc=args.nproc,randfactor=args.randfactor)
        indsel_dat=indsel_dat[ind_this]
        
        indsel_dat,reg_data=remove_edges(indsel_dat,reg_data,cutfrac=0.6)

        #repeat for the randoms
        if('subrand' not in IPdict['mask']):
            reg_data=None
        else:
            print('Subsampling random in patches')
        ind_this,reg_rand=remove_cross_pair(finran[1]['RA'][indsel_ran],finran[1]['DEC'][indsel_ran],reg_data=reg_data,
                mask=mask_set,delta_ra=5,delta_dec=5,distance_upper_bound=50,eps=0,nproc=args.nproc,randfactor=args.randfactor)

        indsel_ran=indsel_ran[ind_this]

    return findat,indsel_dat,finran,indsel_ran

def remove_cross_pair(ra,dec,mask='00',delta_ra=3,delta_dec=3,reg_data=None,
                      distance_upper_bound=50,eps=0,nproc=4,randfactor=1.0):

    '''if randoms needs to be subsampled based on patches then provid reg_data'''

    nra=np.int(360/delta_ra)
    ndec=np.int(180/delta_dec)

    ra_bin=np.linspace(0,360,nra)
    dec_bin=np.linspace(-90,90,ndec)

    ra_2d,dec_2d=np.meshgrid(ra_bin,dec_bin)

    tree = spatial.cKDTree(np.column_stack([ra_2d.flatten(),dec_2d.flatten()]),leafsize=50)

    dd,II=tree.query(np.column_stack([ra,dec]), k=1, eps=0, p=2,
                distance_upper_bound=distance_upper_bound,n_jobs=nproc)


    ind_ra=II%nra
    ind_dec=(II-ind_ra)/nra

    if(mask=='mask00'):
        ind=(ind_ra%2==0)*(ind_dec%2==0)
    elif(mask=='mask10'):
        ind=(ind_ra%2==0)*(ind_dec%2!=0)
    elif(mask=='mask01'):
        ind=(ind_ra%2!=0)*(ind_dec%2==0)
    elif(mask=='mask11'):
        ind=(ind_ra%2!=0)*(ind_dec%2!=0)

    indsel=np.where(ind)[0]
    regsel=II[ind]

    if(reg_data is not None):
       indsel,regsel=subsamp_patches(index_rand=indsel,reg_rand=regsel,reg_data=reg_data,randfactor=randfactor)


    return indsel, regsel

def remove_edges(index,reg,cutfrac=0.5):
    '''remove the region with too few galaxies to reduce the edge effect'''

    uid_data=np.unique(reg)

    bins=np.linspace(0,uid_data.max()+1,uid_data.max()+2)-0.5

    hc_data,_=np.histogram(reg,bins=bins)
    min_count=1

    count=0
    while(True):
        ind_above_min=hc_data>min_count
        mean_count=hc_data[ind_above_min].mean()
        min_count=cutfrac*mean_count
        ind_new=hc_data[ind_above_min]<min_count
        #print(ind_new.sum())


        count=count+1
        if(False):
            pl.figure()
            pl.plot(hc_data[ind_above_min])
            pl.plot(np.zeros(ind_above_min.sum())+0.5*mean_count,'k--')
        if(count>3 or ind_new.sum()==0):
            #print(min_count)
            break

    index_valid=np.where((hc_data<min_count)*(hc_data>1))[0]

    indall=np.ones(index.size,dtype=bool)
    for rr,tr in enumerate(index_valid):
        tind=reg==tr
        indall[tind]=False


    return index[indall], reg[indall]


def subsamp_patches(index_rand=None,reg_rand=None,reg_data=None,randfactor=1.0):
    '''subsample the random patch by patch
    1) unique regions in data
    2) make histogram of data unique region
    3) make histogram of randoms
    4) take the ratio and find the maximum randoms we can have
    5) subsample the randoms'''
    
    uid_data=np.unique(reg_data)
    uid_rand=np.unique(reg_rand)
    
    bins=np.linspace(0,uid_data.max()+1,uid_data.max()+2)-0.5
    
    hc_data,_=np.histogram(reg_data,bins=bins)
    hc_rand,_=np.histogram(reg_rand,bins=bins)
    
    
    in_nozero=hc_data>0
    max_rfact=np.min(hc_rand[in_nozero]/hc_data[in_nozero])
    
    if(max_rfact<randfactor):
        randfactor=max_rfact
        print('Can only use %4.2f as randfactor'%randfactor)
        
    
    trand=np.random.random(index_rand.size)
    fsub_arr=np.zeros(index_rand.size)
    
    for uu,uid in enumerate(uid_data):
        need_rand=randfactor*hc_data[uid]
        fsub=1.0*need_rand/hc_rand[uid]
        tind=reg_rand==uid
        
        fsub_arr[tind]=fsub
        
    #print(fsub_arr)
    tind=trand<fsub_arr
    
    index_sel=index_rand[tind]
    reg_sel=reg_rand[tind]

    return index_sel,reg_sel

def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in prop dic in fH'''
    
    #takes a list of jn files: list is important
    JNfile=[args.jnfile ]
    jnreg=util.compute_JN_data(fH['fits'][1]['RA'][indsel],fH['fits'][1]['DEC'][indsel],JNfile)
    return jnreg

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    #weights=fH['fits'][1]['wt'][indsel]
    
    if(fH['prop']['wtag'] in ['w1','w1-pip','w1-iip']):
        weights=np.ones(indsel.size)
    elif(fH['prop']['wtag'] in ['wsys', 'wsys-pip','wsys-iip']):
        weights=fH['fits'][1]['WEIGHT_SYSTOT'][indsel]*fH['fits'][1]['WEIGHT_NOZ'][indsel]
        weights=weights*fH['fits'][1]['WEIGHT_NOZ']['WEIGHT_FKP'][indsel]
        if(fH['typein']=='data'):
            weights=weights*fH['fits'][1]['WEIGHT_CP'][indsel]

    return weights

def GetXYZ(fH,indsel,args):
    '''Load the appropriate xyz after needed transforms'''

    xyz, interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indsel],
            fH['fits'][1]['DEC'][indsel],fH['fits'][1]['Z_COSMO'][indsel]
            ,args.H0,args.omM,1.0-args.omM,interp='')
    return xyz

def LoadData_part(fH,I1,I2,args):
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]

   #read co-ordinate and redshift
   cat_z=fH['fits'][1]['Z_COSMO'][indcut]

   #checking if redshift is nan
   ind_nan=cat_z!=cat_z
   if(np.sum(ind_nan)>0):
       print(' Warning: Found %d redshift as nan, setting them to mean redshift,ok for angular cluatering not for 3d'%ind_nan.sum())
       cat_z[ind_nan]=cat_z[~ind_nan].mean()
       assert(args.sampmode in [6,7]) #must be angular clustering

   #check for negative redshift
   ind_neg=cat_z<=0
   if(ind_neg.sum()>0):
       print(' Warning: Found %d negative redshift, setting them to mean redshift, ok for angular clustering not for 3d'%ind_neg.sum())
       cat_z[ind_neg]=cat_z[~ind_neg].mean()
       assert(args.sampmode in [6,7])  #must be angular clustering

   XYZ,interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indcut],fH['fits'][1]['DEC'][indcut],
           cat_z,args.H0,args.omM,1-args.omM,interp='')

   #set weights =1 for now
   weights=GetWeights(fH,indcut)
   data=np.column_stack([XYZ,weights])


   if(args.njn>0):
      JN_reg=GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   #if pip weight is needed then evaluate pip wieght
   if(args.pip==True or args.iip==True):
       if(fH['typein']=='data'):
           fib_full=fH['fits'][1]['WEIGHT_BW'][:]
           #fib_full=fib_full.astype(int)
           #apply cut
           fib=np.ascontiguousarray(fib_full[indcut],dtype=np.int)
       else:#(fH['typein']=='rand' or 'pip'):
           fib=np.ascontiguousarray(np.array([]),dtype=np.int)

       return data_c, fib

   return  data_c
