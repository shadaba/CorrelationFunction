import numpy as np
import pylab as pl
import mycosmo as mcosm
import fitsio as F

#utility function
import util
#impot desi_mask
#from desitarget.sv3.sv3_targetmask import desi_mask, bgs_mask

#desi jacknife library
import importlib
module_name='catlib.desi_sv3.jackknife_polar_Rosette'
rosejn=importlib.import_module(module_name)
import pickle

def FITS_Selection(datafile,randfile,args,IPdict):
    '''All the special selction should be based on input dictionary IPdict and written here'''

    if(args.z1z2[0]>0 and args.z1z2[1]>args.z1z2[0]):
        Zsel="Z >= "+str(args.z1z2[0])+" && Z <"+str(args.z1z2[1])
    else:
        print('*** No redshift slection is applied, if this is unexpected then check z1z2 entries')
        Zsel='1>0'

    selkey=''

    findat=F.FITS(datafile)
    if(IPdict['sky'] in ['N','S'] and args.selection=='TARGET'):
        selkey="PHOTSYS=='%s'"%(IPdict['sky'])
    
    #anyselection can be applied here
    if(selkey!=''):
        indsel_dat = findat[1].where(Zsel+' && ' +selkey) #select everythin
    else:
        indsel_dat = findat[1].where(Zsel) #select everythin
    
    #Now apply the tracer selection using desi_mask
    if(args.selection!='CLUSTERING'):
        if('BGS' in IPdict['tracer'] and IPdict['tracer']!='BGS_ANY'):
            sv3_bgs_target=findat[1]['SV3_BGS_TARGET'][indsel_dat]
            sel1 = (sv3_bgs_target & bgs_mask[IPdict['tracer']]) > 0
            sv3_desi_target=findat[1]['SV3_DESI_TARGET'][indsel_dat]
            sel2 = (sv3_desi_target & desi_mask['BGS_ANY']) > 0
            sel=sel1*sel2
        else:
            sv3_desi_target=findat[1]['SV3_DESI_TARGET'][indsel_dat]
            sel = (sv3_desi_target & desi_mask[IPdict['tracer']]) > 0
        indsel_dat=indsel_dat[sel]

    finran=F.FITS(randfile)
    if(selkey!=''):
        indsel_ran=finran[1].where(selkey)
    else:
        indsel_ran=finran[1].where(Zsel)
    #indsel_ran = finran[1].where(Zsel) #select everything now


    #setup the jackknife process
    if(args.jnfile=='' and 'notuseforjn' not in IPdict.keys()):
        #load tile
        tile=rosejn.load_tile(tile_file=args.tile_file)
        rosette=rosejn.club_tiles_by_Rosette(tile,program=IPdict['PROGRAM'])
        #assign Rosette
        ind_rose=rosejn.assign_rosette(finran,indsel_ran,rosette)

        #create_jackniffe def
        jnid,rose_jn_dic=rosejn.generate_jackknife(finran,indsel_ran,rosette,
                njn_per_rose=args.njn_per_rose,ind_rose=ind_rose)
        args.njn=np.unique(jnid).size
        print('assigned njn= %d'%args.njn)
        
        #Now save the results in jnfile
        with open(jnfile_name(args),'wb') as fout:
            pickle.dump({'rose_jn_dic':rose_jn_dic,'rosette':rosette},fout)

    if(args.randfactor>0 and indsel_ran.size>args.randfactor*indsel_dat.size):
        #need to subsample the random
        need_rand=args.randfactor*indsel_dat.size
        np.random.seed(0)
        indsel_ran=np.random.choice(indsel_ran,need_rand,replace=False)
        #sort the indices
        indsel_ran.sort()

    return findat,indsel_dat,finran,indsel_ran

def jnfile_name(args):
    if(args.jnfile==''):
        jnfile=args.outfile+'.pkl.jn'
    return jnfile

def GetJNreg(fH,indsel,args):
    '''Returns the JN region number for each sample type based in prop dic in fH'''
    
    with open(jnfile_name(args),'rb') as fin:
        jn_dic= pickle.load(fin)

    #takes a list of jn files: list is important
    JNfile=[args.jnfile ]
    jnreg=rosejn.assign_jackknife(fH['fits'],indsel,jn_dic['rosette'],
                     jn_dic['rose_jn_dic'],ind_rose=None)
    return jnreg

def GetWeights(fH,indsel):
    '''Returns the weights based on IPdict'''
    #weights=fH['fits'][1]['wt'][indsel]
    
    if(fH['prop']['wtag'] in ['w1','w1-pip','w1-iip']):
        weights=np.ones(indsel.size)
    elif(fH['prop']['wtag'] in ['wcomp','wcompEdWsys']):
        weights=fH['fits'][1]['WEIGHT'][indsel]
        if(fH['typein']=='data' and fH['prop']['wtag'] in ['wcompEdWsys']):
            wsys_file=fH['fname']+'_EdWsys/wsys_v0.fits'
            with F.FITS(wsys_file) as fin:
                weights=weights*fin[1]['wsys'][indsel]
            print('Loaded systematic weights from: ',wsys_file)
    elif(fH['prop']['wtag'] in ['wcompR1']):
        if(fH['typein']=='data'):
            weights=fH['fits'][1]['WEIGHT'][indsel]
        else:
            weights=np.ones(indsel.size)
    return weights

def LoadData_part(fH,I1,I2,args):
   #load data based on the coordinates
   indcut=fH['isel'][I1:I2]

   #read co-ordinate and redshift
   if(args.sampmode in [6,7]):
       cat_z=np.ones(indcut.size)
   else:
       cat_z=fH['fits'][1]['Z'][indcut]

   #checking if redshift is nan
   ind_nan=cat_z!=cat_z
   if(np.sum(ind_nan)>0):
       print(' Warning: Found %d redshift as nan, setting them to mean redshift,ok for angular cluatering not for 3d'%ind_nan.sum())
       cat_z[ind_nan]=cat_z[~ind_nan].mean()
       assert(args.sampmode in [6,7]) #must be angular clustering

   #check for negative redshift
   ind_neg=cat_z<=0
   if(ind_neg.sum()>0):
       print(' Warning: Found %d negative redshift, setting them to mean redshift, ok for angular clustering not for 3d'%ind_neg.sum())
       cat_z[ind_neg]=cat_z[~ind_neg].mean()
       assert(args.sampmode in [6,7])  #must be angular clustering

   XYZ,interp=mcosm.RDZ2XYZ(fH['fits'][1]['RA'][indcut],fH['fits'][1]['DEC'][indcut],
           cat_z,args.H0,args.omM,1-args.omM,interp='')

   #set weights =1 for now
   weights=GetWeights(fH,indcut)
   data=np.column_stack([XYZ,weights])


   if(args.njn>0):
      JN_reg=GetJNreg(fH,indcut,args)
      data=np.column_stack([data,JN_reg])


   #convert arrays to contiguous array
   if(args.njn==0):
      data_c = np.ascontiguousarray(data[:,[0,1,2,3]], dtype=np.float64)
   else:
      data_c = np.ascontiguousarray(data[:,[0,1,2,3,4]], dtype=np.float64)
      if(np.max(data[:,4]) != args.njn-1):
        print("number of jacknife given and in the file do not match")
        print("file njn :  %d  given njn: %d"
          %(np.max(data[:,4]),args.njn))
        #This exits if not cross and max jacknife is not  same as given
        #if(len(args.selection)!=2 or selection=='All'):
        #   sys.exit()


   #if pip weight is needed then evaluate pip wieght
   if(args.pip==True or args.iip==True):
       if(fH['typein']=='data'):
           #The c code uses long hence the 64 bit needs to be divided in 32 bits
           #assumes input bitweight is 64 bit numbers and split it in two 32 bit numbers
           low31=get_low31()
           #fib_tmp=np.ones(fH['fits'][1]['BITWEIGHTS'][:].size,dtype=int)-10
           fib_tmp=fH['fits'][1]['BITWEIGHTS'][:]
           if(len(fib_tmp.shape)==1):
               fib_tmp=fib_tmp.reshape(fib_tmp.size,1)

           for ii in range(0,fib_tmp.shape[1]):
               fib1=np.int32(fib_tmp[indcut,ii] & low31)
               fib2=np.int32(fib_tmp[indcut,ii]>>31 & low31)
               if(ii==0):
                   fib_full=np.column_stack([fib1,fib2])
               else:
                   fib_full=np.column_stack([fib_full,fib1,fib2])
                   
           #set the first bit to 1: A temporary hack
           print('Warning: setting the first bit to always 1 for the actual realization')
           fib_full[:,0]=fib_full[:,0] | 1
           
           #put in contguous array for cython
           fib=np.ascontiguousarray(fib_full,dtype=np.int)
           #print('min,max:',fib.min(),fib.max(),fib.dtype)
       else:#(fH['typein']=='rand' or 'pip'):
           fib=np.ascontiguousarray(np.array([]),dtype=np.int)

       return data_c, fib

   return  data_c

def get_low31():
    '''returns a binary number with first 31 bit set to 1'''
    low31=0
    for ii in range(0,31):
        low31+=np.power(2,ii)

    return low31
