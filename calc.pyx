# f.pyx: numpy arrays -> extern from "fc.h"
# 3 steps:
# cython calc.pyx  -> f.c
# link: python f-setup.py build_ext --inplace  -> f.so, a dynamic library
# py test-f.py: import f gets f.so, f.fpy below calls fc()

from __future__ import division
import numpy as np
cimport numpy as np
from matplotlib import pyplot as plt

cdef extern from "./corr2dc.h" nogil:
    void corr2d(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, double *rlim,
            int nbins0, int nbins1, int nhocells, double *blen,  
	    double *posmin, int samp, int njn, int pbc, int los, int interactive);
    
    void corr2d_wpip(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, double *rlim,
            int nbins0, int nbins1, int nhocells, double *blen,  
	    double *posmin, int samp, int njn, int pbc, int los, int interactive,
            long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip,
            int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up);

    void corr2d_wpip_efficient(double *xc_2d, double *p1, long fp1, long np1, double *p2, long fp2, long np2, double *rlim,
            int nbins0, int nbins1, int nhocells, double *blen,  
	    double *posmin, int samp, int njn, int pbc, int los, int interactive,
            long *fib1, long *fib2, int nfib_int1, int nfib_int2, int bit_perlong, int need_iip,
            int need_angup, double *log_theta_lim, int nbin_log_theta, double *wang_up);

    void evaluate_iip(double *pp, int nattr, long *fib, int nfib_int, int bit_perlong, long fp, long np1);



def corr2dpy(np.ndarray[np.double_t,ndim=2] p1, fp1,np1,
        np.ndarray[np.double_t,ndim=2] p2, fp2, np2, 
        np.ndarray[np.double_t,ndim=1] rlim, 
	nbins0,nbins1, nhocells, 
	np.ndarray[np.double_t,ndim=1] blen, 
	np.ndarray[np.double_t,ndim=1] posmin, samp,njn,pbc,los,interactive):
    """
    Calculate the 2d correlation function.

    Parameters
    ----------
    p1: (np1,6/7) array of double
        Particle x, y, z, vx, vy, vz, (weight) of Group 1.
    p2: (np1,6/7) array of double
        Particle x, y, z, vx, vy, vz, (weight) of Group 2.
    rlim[4]: double, two dimensional correlation function sampling
        minimum,Maximum of first and second co-ordinate range to look at.
    nbins0,nbins1: int, two dimensional correlation function sampling
        Number of bins in the estimator.
    nhocells: int
        Number of cells.
    blen[3]: double, 3d box size or the extent of data
        Box length.
    posmin[3]: double, minimum value of x,y,z co-ordinate
        Box begining.
    samp : int
        To decide the sampling of the correlation function
    njn : int 0 , >0 (number of jacknife regions)
        To decide whether the number of jacknife regions
    interactive:0 can be used to print information

    Returns
    -------
    xc: (nbins,nbins) array of long
        2d correlation function.

    """
    cdef np.ndarray[np.double_t,ndim=2] xc = np.zeros((nbins0, nbins1), 
	                                dtype=np.float64)

    cdef np.ndarray[np.double_t,ndim=3] xc_jn = np.zeros((nbins0, nbins1,njn+1),
	                                          dtype=np.float64)
	                               
    nattr = 5 if njn > 0 else 4
    assert p1.shape[1] == p2.shape[1] == nattr
    #np1, np2 = p1.shape[0], p2.shape[0]

    if(njn==0):
       corr2d(<double*> xc.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive) 

       return xc
    else:
       corr2d(<double*> xc_jn.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive) 

       return xc_jn

#This also takes pip weights
def corr2dpy_wpip(np.ndarray[np.double_t,ndim=2] p1, fp1,np1,
        np.ndarray[np.double_t,ndim=2] p2, fp2, np2, 
        np.ndarray[np.double_t,ndim=1] rlim, 
	nbins0,nbins1, nhocells, 
	np.ndarray[np.double_t,ndim=1] blen, 
	np.ndarray[np.double_t,ndim=1] posmin, samp,njn,pbc,los,interactive,
        np.ndarray[np.int64_t,ndim=2] fib1, np.ndarray[np.int64_t,ndim=2] fib2, nfib_int1, nfib_int2, bit_perlong,need_iip,
        need_angup, np.ndarray[np.double_t,ndim=1] log_theta_lim, nbin_log_theta, np.ndarray[np.double_t,ndim=1] wang_up,eff):
    """
    Calculate the 2d correlation function.

    Parameters
    ----------
    p1: (np1,6/7) array of double
        Particle x, y, z, vx, vy, vz, (weight) of Group 1.
    p2: (np1,6/7) array of double
        Particle x, y, z, vx, vy, vz, (weight) of Group 2.
    rlim[4]: double, two dimensional correlation function sampling
        minimum,Maximum of first and second co-ordinate range to look at.
    nbins0,nbins1: int, two dimensional correlation function sampling
        Number of bins in the estimator.
    nhocells: int
        Number of cells.
    blen[3]: double, 3d box size or the extent of data
        Box length.
    posmin[3]: double, minimum value of x,y,z co-ordinate
        Box begining.
    samp : int
        To decide the sampling of the correlation function
    njn : int 0 , >0 (number of jacknife regions)
        To decide whether the number of jacknife regions
    interactive:0 can be used to print information

    pip weighted related things
    fib1: fibre allocation of galaxies (np1,nfib_int)
    fib2: fibre allocation of galaxies (np1,nfib_int)
    nfib_int: number of integer needed to store all the realizations
    need_iip; when set to zero then iip weight is used in place of pip weight

    #Angular upweighting
    need_angup : set 1 if angular upweighting is needed
    log_theta_lim: array to provide theta limits in order minimum value of log_theta, maximum value of log theta, bin width of log theta
    nbin_log_theta: number of bins in angular up weight
    wang_up: The angular upweight values for this pair

    eff: set this to True to call the effcient version of pair count under development and not well tested

    Returns
    -------
    xc: (nbins,nbins) array of long
        2d correlation function.

    """
    cdef np.ndarray[np.double_t,ndim=2] xc = np.zeros((nbins0, nbins1), 
	                                dtype=np.float64)

    cdef np.ndarray[np.double_t,ndim=3] xc_jn = np.zeros((nbins0, nbins1,njn+1),
	                                          dtype=np.float64)
	                               
    nattr = 5 if njn > 0 else 4
    assert p1.shape[1] == p2.shape[1] == nattr
    #np1, np2 = p1.shape[0], p2.shape[0]

    if(eff==0):
       if(njn==0):
          corr2d_wpip(<double*> xc.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive, 
            <long*> fib1.data, <long*> fib2.data, nfib_int1,nfib_int2, bit_perlong, need_iip,
            need_angup, <double*> log_theta_lim.data, nbin_log_theta, <double*> wang_up.data) 

          return xc
       else:
          corr2d_wpip(<double*> xc_jn.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive,
            <long*> fib1.data, <long*> fib2.data, nfib_int1, nfib_int2, bit_perlong, need_iip,
            need_angup, <double*> log_theta_lim.data, nbin_log_theta, <double*> wang_up.data) 

          return xc_jn
    else:
       if(njn==0):
          corr2d_wpip_efficient(<double*> xc.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive, 
            <long*> fib1.data, <long*> fib2.data, nfib_int1,nfib_int2, bit_perlong, need_iip,
            need_angup, <double*> log_theta_lim.data, nbin_log_theta, <double*> wang_up.data) 

          return xc
       else:
          corr2d_wpip_efficient(<double*> xc_jn.data, <double*> p1.data, fp1, np1, <double*> p2.data, fp2, np2,
            <double*> rlim.data, nbins0,nbins1, nhocells, <double*> blen.data, 
	    <double*> posmin.data,samp, njn, pbc, los,interactive,
            <long*> fib1.data, <long*> fib2.data, nfib_int1, nfib_int2, bit_perlong, need_iip,
            need_angup, <double*> log_theta_lim.data, nbin_log_theta, <double*> wang_up.data) 
          return xc_jn



def get_iip(np.ndarray[np.double_t,ndim=2] p1, nattr, fp1, np1, np.ndarray[np.int64_t,ndim=2] fib, nfib_int, bit_perlong):
    """
    Calculate the iip for the data.
    """

    evaluate_iip(<double*> p1.data, nattr, <long*> fib.data, nfib_int, bit_perlong, fp1, np1)
    
    return p1[:,3]

