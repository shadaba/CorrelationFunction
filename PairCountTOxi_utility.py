#Author: Shadab Alam, March 2015
#This program takes the pair count and normalization from 
#PairCount's code output and compute 2d correlation and
#monopole and quadrupole moment of corrlation


from __future__ import print_function,division

import numpy as np
import os
import sys
import io

__author__ = "Shadab Alam"
__version__ = "1.0"
__email__  = "salam@roe.ac.uk"



f_write=[]
f_plot=[]

def check_directory(dirin):
    '''checks if directory exists, and whether we need to create on'''

    if(not os.path.isdir(dirin)):
        print('***************\n   Need to create following directory\n   %s'%(dirin))
        res=input('enter yes to create directory or no to quit:')
        if(res.lower()=='yes'):
            os.mkdir(dirin)
        else:
            sys.exit()
    return

def update_inputs(args):
    '''This fill in the needed input if they were not provided regarding file paths based on config_file'''
  
    #check if these input is available 
    for troot in ['pcroot', 'xi2droot','xi02root']:
        if(troot not in args.__dict__.keys()):
            args.__dict__[troot]=''

    #sort the output root
    if(args.pcroot==''):
         args.pcroot=args.outfile

    if(args.xi2droot==''):
         out_comp=args.outfile.split('/')
         #change the directory
         out_comp[-2]='XI2D'
         args.xi2droot='/'.join(out_comp)

         check_directory('/'.join(out_comp[:-1]))

    if(args.xi02root==''):
         out_comp=args.outfile.split('/')
         #change the directory sampmode based directory names
         for tt,tkey in enumerate( dir_sampmode_dic.keys()):
             if(args.sampmode in dir_sampmode_dic[tkey]):
                 out_comp[-2]=tkey
                 break

         args.xi02root='/'.join(out_comp)
         #This checks if directory exists or not
         check_directory('/'.join(out_comp[:-1]))

    #add a few info
    if(args.pip or args.iip or args.ang_up or args.ang_up):#this is for pip
         args.usepip = 1
    else:
         args.usepip = 0


    if( (args.noDR or args.noRD) and args.noRR):
         args.RRtype='analytic_RR'
    elif(args.noRR and (not args.noDR)):
         args.RRtype='noRR'
    else:
         args.RRtype=''

 
    return args

def load_PairCount_output(root,RRtype='',samp='',Lbox=0.0,usepip=0):
   #load sbins
   lines=open(root+'-DD.dat').readlines()
   sbins=np.array([float(x) for x in lines[0].split()])
   ns=sbins.size-1
   #load mu bins
   mubins=np.array([float(x) for x in lines[1].split()])
   nmu=mubins.size-1

   #load wieghts
   lines=open(root+'-norm.dat').readlines()
   if(usepip==0):
      Wsum_data=float(lines[0].split()[-1])
      Wsum_rand=float(lines[1].split()[-1])
      DDnorm=np.power(Wsum_data,2)
      DRnorm=Wsum_data*Wsum_rand
      RRnorm=Wsum_rand*Wsum_rand
   else:
      DDnorm=float(lines[0].split()[-1])
      DRnorm=float(lines[1].split()[-1])
      if(RRtype==''):
          RRnorm=float(lines[2].split()[-1])

   #load pair counts
   DD=np.loadtxt(root+'-DD.dat',skiprows=2)

   if(RRtype=='analytic_RR' and samp in ['rmu','rp-pi','logrp-pi'] and Lbox!=0): #dont and DR and RR count estimate RR analytically
      #Wrat=Wsum_data/np.power(Lbox,3.0/2.0)
      DR=np.copy(DD)*0.0
      RR=np.copy(DD)
      RRnorm=DDnorm
      ndens=Wsum_data/np.power(Lbox,3)
      #estimate the volume element for RR count
      if(samp=='rmu'):
         dr3=(2.0*np.pi/3.0)*(np.power(sbins[1:],3)-np.power(sbins[:-1],3))
         for mm, muthis in enumerate(mubins[:-1]):
            #estimate the volumes for rmu sampling
            RR[:,mm]=dr3*(mubins[mm+1]-mubins[mm])
      elif(samp=='rp-pi'):
         dr3=np.pi*(np.power(sbins[1:],2)-np.power(sbins[:-1],2))
         for mm, muthis in enumerate(mubins[:-1]):
            #estimate the volumes for rmu sampling
            RR[:,mm]=dr3*(mubins[mm+1]-mubins[mm])
         #print('################',samp,RR,sbins) 
      elif(samp=='logrp-pi'):
         rpbins=np.power(10,sbins)
         dr3=np.pi*(np.power(rpbins[1:],2)-np.power(rpbins[:-1],2))
         for mm, muthis in enumerate(mubins[:-1]):
            #estimate the volumes for rmu sampling
            RR[:,mm]=dr3*(mubins[mm+1]-mubins[mm])
      #convert the volume to numbers of RR 
      RR=RR*ndens*(Wsum_data-1)
      #print('################',samp,RR,sbins)
   else:
      DR=np.loadtxt(root+'-DR.dat',skiprows=2)
      if(RRtype==''):
         RR=np.loadtxt(root+'-RR.dat',skiprows=2)
      else:
         RR=np.array([])
         #RR=np.loadtxt(root[:-4]+'0001-RR.dat',skiprows=2)

   #reshape the arrays
   if(nmu==1):
       DD=DD.reshape(DD.size,1)
       DR=DR.reshape(DR.size,1)
       RR=RR.reshape(RR.size,1)
    
   #if(usepip==0):
   #   DR=DR*Wrat
   #   RRnorm=1./np.power(Wrat,2)
   #   #combined all output in a dictionary and return
   #   pcdict={'sbins': sbins, 'ns': ns, 'mubins':mubins, 'nmu':nmu, 
   #      'DD': DD, 'DR': DR,
   #      'Wsum_data': Wsum_data, 'Wsum_rand': Wsum_rand, 'Wrat':Wrat}
   #normalize DD
   DD=DD/DDnorm
   pcdict={'sbins': sbins, 'ns': ns, 'mubins':mubins, 'nmu':nmu, 
         'DD': DD,'DDnorm':DDnorm}
   if(RRtype in ['','noRR']):  
      DR=DR/DRnorm
      pcdict['DR']=DR
      pcdict['DRnorm']= DRnorm
   

   if(RRtype in ['','analytic_RR']):
      pcdict['RRnorm']=RRnorm
      pcdict['RR']=RR/RRnorm


   return pcdict

def load_PairCount_output_cross(root,RRtype='',samp='',Lbox=0.0,usepip=0):
   #load sbins
   lines=open(root+'-D1D2.dat').readlines()
   sbins=np.array([float(x) for x in lines[0].split()])
   ns=sbins.size-1
   #load mu bins
   mubins=np.array([float(x) for x in lines[1].split()])
   nmu=mubins.size-1
   
   norm_dic={}
   #load wieghts
   lines=open(root+'-norm.dat').readlines()
   if(usepip==0):
      Wsum_data1=float(lines[0].split()[-1])
      Wsum_rand1=float(lines[1].split()[-1])
      Wsum_data2=float(lines[2].split()[-1])
      Wsum_rand2=float(lines[3].split()[-1])
      norm_dic['D1D2']=Wsum_data1*Wsum_data2
      norm_dic['D1R2']=Wsum_data1*Wsum_rand2
      if(RRtype==''):
          norm_dic['R1D2']=Wsum_rand1*Wsum_data2
          norm_dic['R1R2']=Wsum_rand1*Wsum_rand2
   else:
      norm_dic['D1D2']=float(lines[0].split()[-1])
      norm_dic['D1R2']=float(lines[1].split()[-1])
      if(RRtype==''):
          norm_dic['R1D2']=float(lines[2].split()[-1])
          norm_dic['R1R2']=float(lines[3].split()[-1])

   #combined all output in a dictionary and return
   pcdict={'sbins': sbins, 'ns': ns, 'mubins':mubins, 'nmu':nmu} 
   
   #load and normalize pair counts
   for pp,ptype in enumerate(norm_dic.keys()):
       if(RRtype=='analytic_RR' and ptype in ['R1D2','R1R2']):
           continue

       pcthis=np.loadtxt('%s-%s.dat'%(root,ptype),skiprows=2)
       pcdict[ptype]=pcthis/norm_dic[ptype]
       pcdict[ptype+'norm']=norm_dic[ptype]

   return pcdict





def combine_count(root1,root2):
  #load the count for each of the root
  sbins1, ns1, mubins1, nmu1, DD1,DR1,RR1, Wsum_data1,Wsum_rand1=load_PairCount_output(root1)
  sbins2, ns2, mubins2, nmu2, DD2,DR2,RR2, Wsum_data2,Wsum_rand2=load_PairCount_output(root2)

  Wrat=(Wsum_data1+Wsum_data2)/(Wsum_rand1+Wsum_rand2)
  #giving normalized pair counts
  DD=DD1+DD2
  DR=(DR1+DR2)*Wrat
  RR=(RR1+ RR2)*Wrat*Wrat

  if(np.sum(sbins1==sbins2)==sbins1.size and ns1==ns2 
     and np.sum(mubins1==mubins2)==mubins1.size and nmu1==nmu2):
     return sbins1, ns1, mubins1, nmu1, DD,DR,RR
  else:
     return 'error'


def comb_pc(pcdict1,pcdict2):
   pcomb={}
   pair_list=['DD','DR','RR','D1D2','D1R2','R1D2','R1R2']
   pair_norm_list=[]
   for pair in pair_list:
      pair_norm_list.append(pair+'norm')   

   for tt,tkey in enumerate(pcdict1.keys()):
      if(tkey not in pair_list and tkey not in pair_norm_list):
         t_comp=pcdict1[tkey]==pcdict2[tkey]
         if(isinstance(t_comp,list) or isinstance(t_comp,np.ndarray)):
            assert(t_comp.sum()==t_comp.size)
         else:
            assert(t_comp)
         pcomb[tkey]=pcdict1[tkey]
      elif(tkey in pair_list):
         #unnormalize- sum- normalize
         pc1=pcdict1[tkey]*pcdict1[tkey+'norm']
         pc2=pcdict2[tkey]*pcdict2[tkey+'norm']
         pcomb[tkey+'norm']=pcdict1[tkey+'norm']+pcdict1[tkey+'norm']
         pcomb[tkey]=(pc1+pc2)/pcomb[tkey+'norm']
      elif(tkey in pair_norm_list):
         continue

   return pcomb

#This combines many roots
def combine_Manypcdict(inroots=[],xitype='auto',RRtype='',samp='',Lbox=0.0,usepip=0):
   nroots=len(inroots)
   if(xitype=='auto'):
      pcomb=load_PairCount_output(inroots[0],RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
      for ii in range(1,nroots):
         pcomb_2=load_PairCount_output(inroots[ii],RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
         pcomb_12=comb_pc(pcomb,pcomb_2)
         #copy to pcomb
         pcomb=pcomb_12.copy()
   elif(xitype in ['cross']):
      pcomb=load_PairCount_output_cross(inroots[0],RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
      for ii in range(1,nroots):
         pcomb_2=load_PairCount_output_cross(inroots[ii],RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
         pcomb_12=comb_pc(pcomb,pcomb_2)
         #copy to pcomb
         pcomb=pcomb_12.copy()
   elif(xitype=='autovtf' or xitype=='crossvtf'):
      pcdict=load_Nikhil_output_vtf(root,xitype=xitype)
      if(root2==''):
         return pcdict
      else:
         pcdict2=load_Nikhil_output(root2)
         print('*************Error***** code is incomplete for this case')
         sys.exit()


   return pcomb


def get_xitype(RRtype='',xitype=''):
    '''Decide which paircount combinations are available and hence how to evaluate clustering'''
   
    if(xitype=='auto'):
      if(RRtype=='analytic_RR'):
          xi_estimator='RR'
          pclist=['DD','RR']
      elif(RRtype=='noRR'):
          xi_estimator='DR'
          pclist=['DD','DR']
      else:
          xi_estimator='RR'
          pclist=['DD','DR','RR']
    elif(xitype=='cross'):
      pclist=['D1D2']
      tmp_dic={'D1D2':'DD','D1R2':'DR','R1D2':'RD','R1R2':'RR'}
      for tkey in ['D1R2','R1D2','R1R2']:
          if(tmp_dic[tkey] in RRtype):
              continue
          pclist.append(tkey)

      if(len(pclist)==4):
          xi_estimator='LS'
      elif(len(pclist)==2):
          if('D1R2' in pclist):
              xi_estimator='D1R2'
          elif('R1D2' in pclist):
              xi_estimator='R1D2'
          elif('R1R2' in pclist):
              xi_estimator='R1R2'
      else:
          print('Error: invalid estimator from pclist')
          print('RRtype: ',RRtype)
          print('pclist: ',pclist)
          sys.exit()
    
    return pclist, xi_estimator


def estimate_xi(xitype,xi_estimator,pcbin):
    '''use appropriate 2 pt function estimator based on available pair count'''
    if(xitype=='auto'):
        if(xi_estimator=='LS'):
            xi2d=(pcbin['DD']-2*pcbin['DR']+pcbin['RR'])/pcbin['RR']
        else:
            xi2d=(pcbin['DD']/pcbin[xi_estimator])-1.0
    elif(xitype=='cross'):
        if(xi_estimator=='LS'):
            xi2d=(pcbin['D1D2']-pcbin['D1R2']-pcbin['R1D2']+pcbin['R1R2'])/pcbin['R1R2']
        else:
            xi2d=(pcbin['D1D2']/pcbin[xi_estimator])-1.0

    return xi2d


def compute_xi02(root,pcdict,xitype='auto', nscomb=1,write=0,RRtype=''):
   if(write):
      outfile=root+'-xi2D.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')
   
   pclist, xi_estimator=get_xitype(RRtype=RRtype,xitype=xitype)

   #for rebinning
   nsrebins=int(np.ceil(pcdict['ns']/float(nscomb)))
   srebins=np.zeros(nsrebins)
   nmurebins=pcdict['nmu']
   murebins=np.zeros(nmurebins)

   xi2drebin=np.zeros(nsrebins*nmurebins).reshape(nsrebins,nmurebins)

   rebin=-1
   for ii in range(0,pcdict['ns'],nscomb):
      ss=np.mean(pcdict['sbins'][ii:ii+nscomb+1])
      rebin=rebin+1
      srebins[rebin]=ss
      for jj in range(0,nmurebins):
         mu=np.mean(pcdict['mubins'][jj:jj+2])
         if(rebin==0):
            murebins[jj]=mu

         pcbin={} 
         for pctype in pclist:
            pcbin[pctype]=np.sum(pcdict[pctype][ii:ii+nscomb,jj])

         xi2d=estimate_xi(xitype,xi_estimator,pcbin)

         xi2drebin[rebin,jj]=xi2d
         if(write):
            fout.write('%12.8lf %12.8lf %12.8lf' %(ss,mu,xi2d))
            for pctype in pclist:
               fout.write('%12.8lf '%pcbin[pctype])
            fout.write('\n')

   if(write):
      fout.close()

   #print srebins, murebins
   return srebins, murebins, xi2drebin

#need to work on this function
#Assume sampling in rp-rpi computes xi2D and wp
def compute_xi2D_rppi(xi2droot,wproot,pcdict,xitype='auto',nscomb=1,write=0,RRtype=''):
   if(write):
      outfile=xi2droot+'-xi2D.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')

      outfile=wproot+'-wp.dat'
      f_write.append(outfile)
      fwp=open(outfile,'w')
      
   pclist, xi_estimator=get_xitype(RRtype=RRtype,xitype=xitype)

   wp=np.zeros(pcdict['nper'])
   #for rebinning
   nper_rebins=pcdict['nper']
   npar_rebins=int(np.ceil(pcdict['npar']/float(nscomb)))
   rper_rebins=np.zeros(nper_rebins)
   rpar_rebins=np.zeros(npar_rebins)

   xi2drebin=np.zeros(nper_rebins*npar_rebins).reshape(nper_rebins,npar_rebins)

   for ii in range(0,pcdict['nper'],1):
      ss=np.mean(pcdict['rper'][ii:ii+2])
      rper_rebins[ii]=ss
      rebin=-1
      for jj in range(0,pcdict['npar'],nscomb):
         mu=np.mean(pcdict['rpar'][jj:jj+nscomb+1])
         rebin=rebin+1
         rpar_rebins[rebin]=mu

         pcbin={} 
         for pctype in pclist:
            pcbin[pctype]=np.sum(pcdict[pctype][ii,jj:jj+nscomb])

         xi2d=estimate_xi(xitype,xi_estimator,pcbin)

         xi2drebin[ii,rebin]=xi2d
         if(write):
            fout.write('%12.8lf %12.8lf %12.8lf' %(ss,mu,xi2d))
            for pctype in pclist:
                fout.write('%12.8lf '%pcbin[pctype])
            fout.write('\n')

      dr_par=np.mean(rpar_rebins[1:]-rpar_rebins[:-1])
      wp[ii]=np.sum(xi2drebin[ii,:])*dr_par
      if(write):
         fwp.write('%12.8lf %12.8lf\n'%(ss,wp[ii]))

   if(write):
      fout.close()
      fwp.close()

   return rper_rebins, rpar_rebins, xi2drebin,wp
#end of compute wp and xi2d with rprpi sampling

def compute_angular(wproot,pcdict,samp='angular',xitype='auto',write=0,RRtype=''):
   '''computes the angular clutering'''

   if(xitype=='auto'):
      pclist=['DD', 'DR','RR']
   elif(xitype=='cross'):
      pclist=['D1D2', 'D1R2','R1D2','R1R2']

   wtheta_LS=np.array([])
   if(samp=='angular'):
       theta=0.5*(pcdict['rper'][:-1]+pcdict['rper'][1:])
   elif(samp=='logangular'):
       theta=0.5*(np.power(10,pcdict['rper'][:-1])+np.power(10,pcdict['rper'][1:]))

   if(xitype=='auto'):
       wtheta_DP=(pcdict['DD']/pcdict['DR'])-1.0
       if(RRtype==''):
           wtheta_LS=(pcdict['DD']-2*pcdict['DR']+pcdict['RR'])/pcdict['RR']
   elif(xitype=='cross'):
       wtheta_DP=(pcdict['D1D2']/pcdict['D1R2'])-1.0
       if(RRtype==''):
           wtheta_LS=(pcdict['D1D2']-pcdict['D1R2']-pcdict['R1D2']+pcdict['R1R2'])/pcdict['R1R2']

   if(write):
       hdr='theta (in radian), wtheta_DP'
       outmat=np.column_stack([theta,wtheta_DP])
       if(RRtype==''):
           hdr=hdr+', wtheta_LS'
           outmat=np.column_stack([outmat,wtheta_LS])
       for pct in pclist:
           hdr=hdr+', %s'%(pct)
           outmat=np.column_stack([outmat,pcdict[pct]])
    
       outfile=wproot+'-ang.dat'
       np.savetxt(outfile,outmat,header=hdr)

   return theta, wtheta_DP, wtheta_LS

#a function to call for pair count to xi2d and xi02 in polar co-ordinate
def xi2d_xi02_froot(root,xi2droot,xi02root,xitype='auto',nscomb=1,samp='rmu',write=0,RRtype='',Lbox=0,usepip=0):
   #compute the correlation functions
   if(xitype=='auto'):
      pcdict=load_PairCount_output(root,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
   elif(xitype=='cross'):
      pcdict=load_PairCount_output_cross(root,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)

   if(samp=='logr-theta'):
      pcdict['sbins']=np.power(10,pcdict['sbins'])

   srebins, murebins, xi2drebin=compute_xi02(xi2droot,pcdict,xitype=xitype,nscomb=nscomb,
         write=write,RRtype=RRtype)
   xi02=Xi_Legendre(xi02root,srebins, murebins, xi2drebin,samp=samp,write=write)

   return srebins,murebins,xi2drebin,xi02

#a function to call for pair count to xi2d and wp in cartesian co-ordinate
def xi2d_wp_froot(root,xi2droot,wproot,xitype='auto',nscomb=1,samp='log',write=0,RRtype='',Lbox=0,usepip=0):
   #compute the correlation functions
   #rper,nper,rpar,npar, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root)
   if(xitype=='auto'):
      pcdict=load_PairCount_output(root,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
   elif(xitype=='cross'):
      pcdict=load_PairCount_output_cross(root,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)

   #change dictionary key for cartesian coordinate
   keyreplace={'rper': 'sbins','nper': 'ns','rpar':'mubins','npar':'nmu'}
   for key in keyreplace.keys():
      pcdict[key]=pcdict.pop(keyreplace[key])

   if(samp=='logrp-pi'):
      pcdict['rper']=np.power(10,pcdict['rper'])



   if(samp in ['angular','logangular']):
       theta, wtheta, wtheta_LS=compute_angular(wproot,pcdict,samp=samp,xitype=xitype,write=write,RRtype=RRtype)
       return theta, wtheta, wtheta_LS
   else:
       rper_rebins,rpar_rebins,xi2drebin,wp=compute_xi2D_rppi(xi2droot,wproot,
                   pcdict,xitype=xitype,nscomb=nscomb,write=write,RRtype=RRtype)
       return rper_rebins, rpar_rebins, xi2drebin,wp

#Many roots: a function to call for pair count to xi2d and xi02 in polar co-ordinate
def xi2d_xi02_Manyfroot(inroots,xi2droot,xi02root,xitype='auto',nscomb=1,samp='rmu',write=0,
        RRtype='',Lbox=0,usepip=0):
   #compute the correlation functions for a list of roots combined
   pcdict=combine_Manypcdict(inroots=inroots,xitype=xitype,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)

   if(samp=='logr-theta'):
      pcdict['sbins']=np.power(10,pcdict['sbins'])

   srebins, murebins, xi2drebin=compute_xi02(xi2droot,pcdict,xitype=xitype,nscomb=nscomb,write=write)
   xi02=Xi_Legendre(xi02root,srebins, murebins, xi2drebin,samp=samp,write=write)

   return srebins,murebins,xi2drebin,xi02


#Many roots a function to call for pair count to xi2d and wp in cartesian co-ordinate
def xi2d_wp_Manyfroot(inroots,xi2droot,wproot,xitype='auto',nscomb=1,samp='log',write=0,
        RRtype='',Lbox=0,usepip=0):
   #compute the correlation functions for many roots
   pcdict=combine_Manypcdict(inroots=inroots,xitype=xitype,RRtype=RRtype,samp=samp,Lbox=Lbox,usepip=usepip)
#   pcdict=combine_Manypcdict(inroots,xitype=xitype)

   #change dictionary key for cartesian coordinate
   keyreplace={'rper': 'sbins','nper': 'ns','rpar':'mubins','npar':'nmu'}
   for key in keyreplace.keys():
      pcdict[key]=pcdict.pop(keyreplace[key])

   if(samp=='logrp-pi'):
      pcdict['rper']=np.power(10,pcdict['rper'])

   if(samp in ['angular','logangular']):
       theta, wtheta, wtheta_LS=compute_angular(wproot,pcdict,samp=samp,xitype=xitype,write=write,RRtype=RRtype)
       return theta, wtheta, wtheta_LS
   else:
       rper_rebins,rpar_rebins,xi2drebin,wp=compute_xi2D_rppi(xi2droot,wproot,
                   pcdict,xitype=xitype,nscomb=nscomb,write=write,RRtype=RRtype)

   return rper_rebins, rpar_rebins, xi2drebin,wp

#This takes a list of roots as inputs and combines them
def xi2d_wp_xi02_Manyfroot_jn(inroots=[],NJNs=[],xi2droot='',outroot='',xitype='auto',
        nscomb=1,samp='logrp-pi',compgZ=False,RRtype='',Lbox=0,usepip=0):
   polar_coord=['rmu','rtheta','logr-theta','logrmu']
   cart_coord=['rp-pi','logrp-pi']
   #figure out number of roots and cumulative jacknife regions
   nroot=len(NJNs)
   NJN=0; rootAll=''
   for ii,njnthis in enumerate(NJNs):
       NJN=NJN+njnthis
       rootAll=rootAll+inroots[ii]+':njn:%d;*;'%(NJNs[ii])
   #files to write the compiled xi2d and wp functions from jackife
   xi2dfile=xi2droot+'-'+samp+'-NJN-%d.txt'%NJN

   
   rcount=0; njncum=0
   for ii in range(0,NJN+1):
      rootlist=[]
      if(NJN==0):
         for rr,rthis in enumerate(inroots):
             rootlist.append(rthis+'-'+samp)
      else:
         for rr,rthis in enumerate(inroots):
             rootlist.append(rthis+'-All-'+samp)

         if(ii<NJN):
            if(ii<(njncum+NJNs[rcount])):
               rootlist[rcount]=inroots[rcount]+'-'+samp+'-JNdir/jk-%d'%(ii-njncum)

            if(ii==(njncum+NJNs[rcount]-1) and rcount<nroot):
               njncum=njncum+NJNs[rcount]
               rcount=rcount+1



      if(samp in polar_coord):
         srebins,murebins,xi2drebin,xi02=xi2d_xi02_Manyfroot(rootlist,
                            '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,
                            RRtype=RRtype,Lbox=Lbox,usepip=usepip)
         if(compgZ):
            gZ=compute_gZ('',srebins, murebins, xi2drebin,samp=samp,write=0)
      elif(samp in ang_coord):
         theta, wtheta_DP, wtheta_LS=xi2d_wp_Manyfroot(rootlist,
                           '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,
                           RRtype=RRtype,Lbox=Lbox,usepip=usepip)
      elif(samp in cart_coord):
         rper_rebins, rpar_rebins, xi2drebin,wp=xi2d_wp_Manyfroot(rootlist,
                           '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,
                           RRtype=RRtype,Lbox=Lbox,usepip=usepip)
      else:
         print( 'Invalid sampling: %s'%samp)
         sys.exit()

      if(ii==0):
         if(samp not in ang_coord): 
            n2d=xi2drebin.size
            xi2dmat=np.zeros(n2d*(NJN+1)).reshape(n2d,NJN+1)

         if(samp in polar_coord):
            nxi02=xi02.shape[0]
            rxi02=xi02[:,0]
            xi0mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
            xi2mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
            xi1mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1) #dipole usefule only for rtheta
            if(compgZ):
               gZmat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
         elif(samp in cart_coord):
            nwp=rper_rebins.size
            wpmat = np.zeros(nwp*(NJN+1)).reshape(nwp,NJN+1)
         elif(samp in ang_coord):
            ntheta=theta.size
            wtheta_DPmat = np.zeros(ntheta*(NJN+1)).reshape(ntheta,NJN+1)
            if(wtheta_LS.size!=0):
                wtheta_LSmat = np.zeros(ntheta*(NJN+1)).reshape(ntheta,NJN+1)

      if(samp not in ang_coord): 
         xi2dmat[:,ii]=xi2drebin.reshape(n2d)
      
      if(samp in polar_coord):
         xi0mat[:,ii]=xi02[:,1]
         xi2mat[:,ii]=xi02[:,2]
         xi1mat[:,ii]=xi02[:,3]
         if(compgZ):
            gZmat[:,ii]=gZ[:,1]
      elif(samp in cart_coord):
         wpmat[:,ii]=wp
      elif(samp in ang_coord):
         wtheta_DPmat[:,ii]=wtheta_DP.reshape(wtheta_DP.size)
         if(wtheta_LS.size!=0):
             wtheta_LSmat[:,ii]=wtheta_LS.reshape(wtheta_LS.size)


   if(samp in polar_coord):
      xi2daxes=[srebins,murebins]
      col1=rxi02
      outtags=['xi0','xi2']
      outmats=[xi0mat,xi2mat]
      if(samp=='rmu' or samp=='logr-mu'):
         xi2dtag=['r','mu']
      elif(samp=='logrmu'):
         xi2dtag=['logr','mu']
      elif(samp=='rtheta' or samp=='logr-theta'):
         xi2dtag=['r','theta']
         outmats.append(xi1mat)
         outtags.append('xi1')
      else:
         print( 'Invalid sampling: %s'%samp)

      if(compgZ):
         outtags.append('gZ')
         outmats.append(gZmat)

   elif(samp in cart_coord):
      xi2daxes=[rper_rebins,rpar_rebins]
      xi2dtag=['rper','rpar']
      outmats=[wpmat]
      col1=rper_rebins
      outtags=['wp']
   elif(samp in ang_coord):
      xi2daxes=[]
      xi2dtag=[]

      outmats=[wtheta_DPmat]
      col1=theta
      outtags=['wtheta_DP']
      if(wtheta_LS.size!=0):
         outmats.append(wtheta_LSmat)
         outtags.append('wtheta_LS')

   #compute the mean and std err for xi2d and write to file
#   meanxi2d=np.mean(xi2dmat[:,:NJN],axis=1)
#   errxi2d=np.sqrt(NJN-1)*np.std(xi2dmat[:,:NJN],axis=1)
#   xi2dwrite=np.column_stack([meanxi2d,errxi2d,
#                                   xi2dmat[:,NJN],xi2dmat[:,:NJN]])
#   with io.FileIO(xi2dfile,'w') as fxi2d:
#      #fxi2d.write('#xi2d with jacknife, root:%s NJN=%d\n'%(rootAll,NJN))
#      np.savetxt(fxi2d,['#xi2d with jacknife, root:%s NJN=%d'%(rootAll,NJN)],fmt='%s')
#      for ww,arr in enumerate(xi2daxes):
#         w_str='#%s: '%xi2dtag[ww]
#         for rr in arr:
#            w_str="%s %10.5f"%(w_str,rr)
#         #fxi2d.write(w_str+'\n')
#         np.savetxt(fxi2d,[w_str],fmt='%s')
#
#      #fxi2d.write('#meanxi2d sigmaxi2d Allxi2d jacknifecolumns\n')
#      np.savetxt(fxi2d,['#meanxi2d sigmaxi2d Allxi2d jacknifecolumns'],fmt='%s')
#      np.savetxt(fxi2d,xi2dwrite,fmt='% 20.10e')

#   print( 'written: ',xi2dfile)


#   for pole, outmat in enumerate(outmats):      
#     #compute the mean and std err for wp and write to file
#      meanout=np.mean(outmat[:,:NJN],axis=1)
#      errout=np.sqrt(NJN-1)*np.std(outmat[:,:NJN],axis=1)
#      outwrite=np.column_stack([col1,meanout,errout,outmat[:,NJN],outmat[:,:NJN]])
#      outfile=outroot+'-'+outtags[pole]+'-'+samp+'-NJN-%d.txt'%NJN
#      with io.FileIO(outfile,'w') as fout:
#         #fout.write('#%s with jacknife, root:%s NJN=%d\n'%(outtags[pole],rootAll,NJN))
#         np.savetxt(fout,['#%s with jacknife, root:%s NJN=%d'%(outtags[pole],rootAll,NJN)],fmt='%s')
#         #fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
#         np.savetxt(fout,['#r(Mpc/h) mean sigma All jacknifecolumns'],fmt='%s')
#         np.savetxt(fout,outwrite,fmt='% 20.10e')
#      print( 'written:',outfile)
   
   if(xi2dtag!=[]):
       #compute the mean and std err for xi2d and write to file
       meanxi2d=np.mean(xi2dmat[:,:NJN],axis=1)
       if(NJN>0):
           errxi2d=np.sqrt(NJN-1)*np.std(xi2dmat[:,:NJN],axis=1)
       else:
           errxi2d=np.zeros(xi2dmat.shape[0])
       xi2dwrite=np.column_stack([meanxi2d,errxi2d,
                                   xi2dmat[:,NJN],xi2dmat[:,:NJN]])

       with io.FileIO(xi2dfile,'w') as fxi2d:
           #fxi2d.write('#xi2d with jacknife, root:%s NJN=%d\n'%(root,NJN))
           np.savetxt(fxi2d,['#xi2d with jacknife, root:%s NJN=%d'%(rootAll,NJN)],fmt='%s')
           for ww,arr in enumerate(xi2daxes):
               w_str='#%s: '%xi2dtag[ww]
               for rr in arr:
                   w_str="%s %10.5f"%(w_str,rr)
               #fxi2d.write(w_str+'\n')
               np.savetxt(fxi2d,[w_str],fmt='%s')
     
           if(NJN>0):
               np.savetxt(fxi2d,['#meanxi2d sigmaxi2d Allxi2d jacknifecolumns'],fmt='%s')
               np.savetxt(fxi2d,xi2dwrite,fmt='% 20.10e')
           elif(NJN==0):
               np.savetxt(fxi2d,['#xi2d'],fmt='%s')
               np.savetxt(fxi2d,xi2dmat[:,NJN],fmt='% 20.10e')

       print('written: ',xi2dfile)


   for pole, outmat in enumerate(outmats):      
     #compute the mean and std err for wp and write to file
      meanout=np.mean(outmat[:,:NJN],axis=1)
      if(NJN>0):
         errout=np.sqrt(NJN-1)*np.std(outmat[:,:NJN],axis=1)
      else:
         errout=np.zeros(outmat.shape[0])
      outwrite=np.column_stack([col1,meanout,errout,outmat[:,NJN],outmat[:,:NJN]])
      outfile=outroot+'-'+outtags[pole]+'-'+samp+'-NJN-%d.txt'%NJN

      with io.FileIO(outfile,'w') as fout:
         #fout.write('#%s with jacknife, root:%s NJN=%d\n'%(outtags[pole],root,NJN))
         np.savetxt(fout,['#%s with jacknife, root:%s NJN=%d'%(outtags[pole],rootAll,NJN)],fmt='%s')
         #fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
         if(NJN>0):
            np.savetxt(fout,['#r(Mpc/h) mean sigma All jacknifecolumns'],fmt='%s')
            np.savetxt(fout,outwrite,fmt='% 20.10e')
         elif(NJN==0):
            np.savetxt(fout,['#r(Mpc/h) val'],fmt='%s')
            np.savetxt(fout,np.column_stack([col1,outmat[:,NJN]]),fmt='% 20.10e')
      print( 'written:',outfile)

   return 0

def jackknife_loop(inroot, xi2droot, samp, NJN, xitype='auto', nscomb=1, RRtype='', Lbox=0, usepip=0):
    polar_coord = ['rmu', 'rtheta', 'logr-theta', 'logrmu']
    cart_coord = ['rp-pi', 'logrp-pi']
    ang_coord = ['angular', 'logangular']

    results = []

    for ii in range(NJN + 1):
        if NJN == 0:
            root = f"{inroot}-{samp}"
        elif ii < NJN:
            root = f"{inroot}-{samp}-JNdir/jk-{ii}"
        else:
            root = f"{inroot}-All-{samp}"

        if samp in polar_coord:
            srebins, murebins, xi2drebin, xi02 = xi2d_xi02_froot(root, '', '', xitype=xitype, samp=samp, nscomb=nscomb, write=0, RRtype=RRtype, Lbox=Lbox, usepip=usepip)
            results.append((srebins, murebins, xi2drebin, xi02))
        elif samp in cart_coord:
            rper_rebins, rpar_rebins, xi2drebin, wp = xi2d_wp_froot(root, '', '', xitype=xitype, samp=samp, nscomb=nscomb, write=0, RRtype=RRtype, Lbox=Lbox, usepip=usepip)
            results.append((rper_rebins, rpar_rebins, xi2drebin, wp))
        elif samp in ang_coord:
            theta, wtheta_DP, wtheta_LS = xi2d_wp_froot(root, '', '', xitype=xitype, samp=samp, nscomb=nscomb, write=0, RRtype=RRtype, Lbox=Lbox, usepip=usepip)
            results.append((theta, wtheta_DP, wtheta_LS))
        else:
            print(f'Invalid sampling: {samp}')
            sys.exit()

    return results

def process_results(results, samp, NJN, xi2droot, outroot):
    polar_coord = ['rmu', 'rtheta', 'logr-theta', 'logrmu']
    cart_coord = ['rp-pi', 'logrp-pi']
    ang_coord = ['angular', 'logangular']


    if samp in polar_coord:
        xi2daxes = [results[0][0], results[0][1]]
        xi2drebin = np.array([r[2] for r in results])
        xi02 = np.array([r[3] for r in results])
        col1 = xi02[0][:, 0]
        outtags = ['xi0', 'xi2']
        outmats = [xi02[:, :, 1], xi02[:, :, 2]]

        if samp in ['rmu', 'logrmu']:
            xi2dtag = ['r', 'mu'] if samp == 'rmu' else ['log10r', 'mu']
        elif samp in ['rtheta', 'logr-theta']:
            xi2dtag = ['r', 'theta']
            outmats.append(xi02[:, :, 3])
            outtags.append('xi1')
    elif samp in cart_coord:
        xi2daxes = [results[0][0], results[0][1]]
        xi2drebin = np.array([r[2] for r in results])
        wp = np.array([r[3] for r in results])
        xi2dtag = ['rper', 'rpar']
        outmats = [wp]
        col1 = results[0][0]
        outtags = ['wp']
    elif samp in ang_coord:
        xi2daxes = []
        xi2dtag = []
        theta = results[0][0]
        wtheta_DP = np.array([r[1] for r in results])
        wtheta_LS = np.array([r[2] for r in results])
        outmats = [wtheta_DP]
        col1 = theta
        outtags = ['wtheta_DP']
        if wtheta_LS.size != 0:
            outmats.append(wtheta_LS)
            outtags.append('wtheta_LS')

    # Write xi2d file
    xi_dic={}
    for ii in range(0,len(xi2dtag)):
        xi_dic[xi2dtag[ii]]=xi2daxes[ii]

    if xi2dtag:
        xi_dic['xi2d']=write_xi2d_file(xi2droot, xi2daxes, xi2dtag, xi2drebin, samp, NJN)

    # Write output files
    for pole, outmat in enumerate(outmats):
        xi_dic[outtags[pole]]=write_output_file(outroot, outtags[pole], samp, NJN, col1, outmat)

    return xi_dic

def write_xi2d_file(xi2droot, xi2daxes, xi2dtag, xi2dmat,samp, NJN):
    if(NJN>0):
        meanxi2d = np.mean(xi2dmat[:NJN,:,:], axis=0)
        errxi2d = np.sqrt(NJN - 1) * np.std(xi2dmat[:NJN,:,:], axis=0)
        xi2dwrite = np.column_stack([meanxi2d.flatten(), errxi2d.flatten(), xi2dmat[NJN,:,:].flatten()])
        for ii in range(0,NJN):
            xi2dwrite=np.column_stack([xi2dwrite,xi2dmat[ii,:,:].flatten()])
    else:
        xi2dwrite = xi2dmat.flatten()

    if(xi2droot is not None):
        xi2dfile = f"{xi2droot}-{samp}-NJN-{NJN}.txt"
        with open(xi2dfile, 'w') as fxi2d:
            fxi2d.write(f'#xi2d with jackknife, NJN={NJN}\n')
            for ww, arr in enumerate(xi2daxes):
                fxi2d.write(f'#{xi2dtag[ww]}: ' + ' '.join(f'{rr:10.5f}' for rr in arr) + '\n')
            if NJN > 0:
                fxi2d.write('#meanxi2d sigmaxi2d Allxi2d jacknifecolumns\n')
                np.savetxt(fxi2d, xi2dwrite, fmt='% 20.10e')
            else:
                fxi2d.write('#xi2d\n')
                np.savetxt(fxi2d, xi2dwrite, fmt='% 20.10e')
        print(f'written: {xi2dfile}')

    return xi2dwrite

def write_output_file(outroot, outtag, samp, NJN, col1, outmat):
    if(NJN>0):
        meanout = np.mean(outmat[:NJN], axis=0)
        errout = np.sqrt(NJN - 1) * np.std(outmat[:NJN], axis=0) if NJN > 0 else np.zeros(outmat.shape[1])
        outwrite = np.column_stack([col1, meanout, errout, outmat[NJN], outmat[:NJN].T])
    else:
        outwrite=np.column_stack([col1, outmat[0,:]])
    outfile = f"{outroot}-{outtag}-{samp}-NJN-{NJN}.txt"

    if(outroot is not None):
        with open(outfile, 'w') as fout:
            fout.write(f'#{outtag} with jackknife, NJN={NJN}\n')
            if NJN > 0:
                fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
                np.savetxt(fout, outwrite, fmt='% 20.10e')
            else:
                fout.write('#r(Mpc/h) val\n')
                np.savetxt(fout,  outwrite, fmt='% 20.10e')
        print(f'written: {outfile}')
    return outwrite

def xi2d_wp_xi02_froot_jn(inroot, xi2droot, outroot, xitype='auto', nscomb=1, samp='logrp-pi',compgZ=False, NJN=1, RRtype='', Lbox=0, usepip=0):
    results = jackknife_loop(inroot, xi2droot, samp, NJN, xitype, nscomb, RRtype, Lbox, usepip)
    return process_results(results, samp, NJN, xi2droot, outroot)


#This function is not used anymore as it is partitioned and refactor into the functions above for clarity
def xi2d_wp_xi02_froot_jn_old(inroot,xi2droot,outroot,xitype='auto',nscomb=1,samp='logrp-pi',compgZ=False,
      NJN=1,RRtype='',Lbox=0,usepip=0):
   polar_coord=['rmu','rtheta','logr-theta','logrmu']
   cart_coord=['rp-pi','logrp-pi']
   ang_coord=['angular','logangular']
   #files to write the compiled xi2d and wp functions from jackife
   xi2dfile=xi2droot+'-'+samp+'-NJN-%d.txt'%NJN
   for ii in range(0,NJN+1):
      if(NJN==0):
         root=inroot+'-'+samp
      elif(ii<NJN): #individual regions
         root=inroot+'-'+samp+'-JNdir/jk-%d'%ii
      else: #All data
         root=inroot+'-All-'+samp
      if(samp in polar_coord):
         srebins,murebins,xi2drebin,xi02=xi2d_xi02_froot(root,
                            '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,RRtype=RRtype,Lbox=Lbox,usepip=usepip)
         if(compgZ):
            gZ=compute_gZ('',srebins, murebins, xi2drebin,samp=samp,write=0)

      elif(samp in cart_coord):
         rper_rebins, rpar_rebins, xi2drebin,wp=xi2d_wp_froot(root,
                           '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,RRtype=RRtype,Lbox=Lbox,usepip=usepip)
      elif(samp in ang_coord):
         theta, wtheta_DP, wtheta_LS=xi2d_wp_froot(root,
                           '','',xitype=xitype,samp=samp,nscomb=nscomb,write=0,RRtype=RRtype,Lbox=Lbox,usepip=usepip)
      else:
         print('Invalid sampling: %s'%samp)
         sys.exit()

      if(ii==0):
         if(samp not in ang_coord): 
            n2d=xi2drebin.size
            xi2dmat=np.zeros(n2d*(NJN+1)).reshape(n2d,NJN+1)


         if(samp in polar_coord):
            nxi02=xi02.shape[0]
            rxi02=xi02[:,0]
            xi0mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
            xi2mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
            xi1mat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1) #dipole usefule only for rtheta
            if(compgZ):
               gZmat=np.zeros(nxi02*(NJN+1)).reshape(nxi02,NJN+1)
         elif(samp in cart_coord):
            nwp=rper_rebins.size
            wpmat = np.zeros(nwp*(NJN+1)).reshape(nwp,NJN+1)
         elif(samp in ang_coord):
            ntheta=theta.size
            wtheta_DPmat = np.zeros(ntheta*(NJN+1)).reshape(ntheta,NJN+1)
            if(wtheta_LS.size!=0):
                wtheta_LSmat = np.zeros(ntheta*(NJN+1)).reshape(ntheta,NJN+1)

      if(samp not in ang_coord): 
         xi2dmat[:,ii]=xi2drebin.reshape(n2d)

      if(samp in polar_coord):
         xi0mat[:,ii]=xi02[:,1]
         xi2mat[:,ii]=xi02[:,2]
         xi1mat[:,ii]=xi02[:,3]
         if(compgZ):
            gZmat[:,ii]=gZ[:,1]
      elif(samp in cart_coord):
         wpmat[:,ii]=wp
      elif(samp in ang_coord):
         wtheta_DPmat[:,ii]=wtheta_DP.reshape(wtheta_DP.size)
         if(wtheta_LS.size!=0):
             wtheta_LSmat[:,ii]=wtheta_LS.reshape(wtheta_LS.size)


   if(samp in polar_coord):
      xi2daxes=[srebins,murebins]
      col1=rxi02
      outtags=['xi0','xi2']
      outmats=[xi0mat,xi2mat]
      if(samp=='rmu'):
         xi2dtag=['r','mu']
      elif(samp=='logrmu'):
         xi2dtag=['log10r','mu']
      elif(samp=='rtheta' or samp=='logr-theta'):
         xi2dtag=['r','theta']
         outmats.append(xi1mat)
         outtags.append('xi1')
      else:
         print('Invalid sampling: %s'%samp)

      if(compgZ):
         outtags.append('gZ')
         outmats.append(gZmat)
   elif(samp in cart_coord):
      xi2daxes=[rper_rebins,rpar_rebins]
      xi2dtag=['rper','rpar']
      outmats=[wpmat]
      col1=rper_rebins
      outtags=['wp']
   elif(samp in ang_coord):
      xi2daxes=[]
      xi2dtag=[]
       
      outmats=[wtheta_DPmat]
      col1=theta
      outtags=['wtheta_DP']
      if(wtheta_LS.size!=0):
         outmats.append(wtheta_LSmat)
         outtags.append('wtheta_LS')

   if(xi2dtag!=[]):
       #compute the mean and std err for xi2d and write to file
       meanxi2d=np.mean(xi2dmat[:,:NJN],axis=1)
       if(NJN>0):
           errxi2d=np.sqrt(NJN-1)*np.std(xi2dmat[:,:NJN],axis=1)
       else:
           errxi2d=np.zeros(xi2dmat.shape[0])
       xi2dwrite=np.column_stack([meanxi2d,errxi2d,
                                   xi2dmat[:,NJN],xi2dmat[:,:NJN]])

       with io.FileIO(xi2dfile,'w') as fxi2d:
           #fxi2d.write('#xi2d with jacknife, root:%s NJN=%d\n'%(root,NJN))
           np.savetxt(fxi2d,['#xi2d with jacknife, root:%s NJN=%d'%(root,NJN)],fmt='%s')
           for ww,arr in enumerate(xi2daxes):
               w_str='#%s: '%xi2dtag[ww]
               for rr in arr:
                   w_str="%s %10.5f"%(w_str,rr)
               #fxi2d.write(w_str+'\n')
               np.savetxt(fxi2d,[w_str],fmt='%s')
     
           if(NJN>0):
               np.savetxt(fxi2d,['#meanxi2d sigmaxi2d Allxi2d jacknifecolumns'],fmt='%s')
               np.savetxt(fxi2d,xi2dwrite,fmt='% 20.10e')
           elif(NJN==0):
               np.savetxt(fxi2d,['#xi2d'],fmt='%s')
               np.savetxt(fxi2d,xi2dmat[:,NJN],fmt='% 20.10e')

       print('written: ',xi2dfile)

   print('outtags: ',outtags)
   for pole, outmat in enumerate(outmats):      
     #compute the mean and std err for wp and write to file
      meanout=np.mean(outmat[:,:NJN],axis=1)
      if(NJN>0):
         errout=np.sqrt(NJN-1)*np.std(outmat[:,:NJN],axis=1)
      else:
         errout=np.zeros(outmat.shape[0])
      outwrite=np.column_stack([col1,meanout,errout,outmat[:,NJN],outmat[:,:NJN]])
      outfile=outroot+'-'+outtags[pole]+'-'+samp+'-NJN-%d.txt'%NJN

      with io.FileIO(outfile,'w') as fout:
         #fout.write('#%s with jacknife, root:%s NJN=%d\n'%(outtags[pole],root,NJN))
         np.savetxt(fout,['#%s with jacknife, root:%s NJN=%d'%(outtags[pole],root,NJN)],fmt='%s')
         #fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
         if(NJN>0):
            np.savetxt(fout,['#r(Mpc/h) mean sigma All jacknifecolumns'],fmt='%s')
            np.savetxt(fout,outwrite,fmt='% 20.10e')
         elif(NJN==0):
            np.savetxt(fout,['#r(Mpc/h) val'],fmt='%s')
            np.savetxt(fout,np.column_stack([col1,outmat[:,NJN]]),fmt='% 20.10e')
      print( 'written:',outfile)

   return 0



def compute_xi02_cross(root,sbins, ns, mubins, nmu, 
        D1D2,D1R2,R1D2,R1R2, nscomb=1,write=0):
   if(write):
      outfile=root+'cross-xi2D.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')

   #for rebinning
   nsrebins=int(np.ceil(ns/float(nscomb)))
   srebins=np.zeros(nsrebins)
   nmurebins=nmu
   murebins=np.zeros(nmu)

   xi2drebin=np.zeros(nsrebins*nmu).reshape(nsrebins,nmu)

   rebin=-1
   for ii in range(0,ns,nscomb):
      ss=np.mean(sbins[ii:ii+nscomb+1])
      rebin=rebin+1
      srebins[rebin]=ss
      for jj in range(0,nmu):
         mu=np.mean(mubins[jj:jj+2])
         if(rebin==0):
            murebins[jj]=mu
         D1D2bin=np.sum(D1D2[ii:ii+nscomb,jj])
         D1R2bin=np.sum(D1R2[ii:ii+nscomb,jj])
         R1D2bin=np.sum(R1D2[ii:ii+nscomb,jj])
         R1R2bin=np.sum(R1R2[ii:ii+nscomb,jj])

         xi2d=(D1D2bin-D1R2bin-R1D2bin+R1R2bin)/R1R2bin
         xi2drebin[rebin,jj]=xi2d
         if(write):
            fout.write('%12.8lf %12.8lf %12.8lf\n' %(ss,mu,xi2d))


   if(write):
      fout.close()

   #print srebins, murebins
   return srebins, murebins, xi2drebin
#End of compute xi02 cross correlation

def Xi_Legendre(root,srebins, murebins, xi2drebin,samp='rmu',write=1):
   if(samp in ['rmu','logrmu']):
      dmu=np.average(murebins[1:]-murebins[:-1])
      diffmu=1 #to account for the fact that integral is 0 to 1
   elif(samp=='rtheta' or samp=='logr-theta'): #in this case murebin is actually theta and not mu
      dtheta=np.average(murebins[1:]-murebins[:-1])
      dmu=np.sin(murebins)*dtheta
      murebins=np.cos(murebins) #this converts theta to mu for integration
      diffmu=2 #to account for the fact that itegral is -1 to 1

   mu2=np.power(murebins,2)
   P0 =np.ones(mu2.size)/2.0    #(2l+1)Pl(mu)*sqrt(1-mu^2) monopole term
   P2=2.5*(3*mu2-1)/2.0         #p2=(3mu^2-1)/2  quadrupole term
   P1=3*murebins/2.0
   

   ns=srebins.size 
   xi02=np.zeros(ns*4).reshape(ns,4)

   if(write):
      outfile=root+'-xi02.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')

   for ii in range(0,ns):
      xi02[ii,0]=srebins[ii]
      xi02[ii,1]=np.sum(xi2drebin[ii,:]*P0*dmu)*2.0/diffmu
      xi02[ii,2]=np.sum(xi2drebin[ii,:]*P2*dmu)*2.0/diffmu
      xi02[ii,3]=np.sum(xi2drebin[ii,:]*P1*dmu)*2.0/diffmu
      if(write==1 and smap=='rmu'):
         fout.write('%12.8lf %12.8lf %12.8lf\n' %(xi02[ii,0],xi02[ii,1],xi02[ii,2]))
      if(write==1 and smap=='rtheta'):
         fout.write('%12.8lf %12.8lf %12.8lf %12.8lf\n' %(xi02[ii,0],xi02[ii,1],xi02[ii,2],xi02[ii,3]))

   
   #plot_xi02(xi02)
   return xi02


def compute_gZ(root,srebins, murebins, xi2drebin,samp='rtheta',write=1):
   '''This function computes the gravitational redshift from 2d correlation'''

   H=100 #hubble constant

   if(samp=='rtheta' or samp=='logr-theta'): #in this case murebin is actually theta and not mu
      dtheta=np.average(murebins[1:]-murebins[:-1])
      dmu=np.sin(murebins)*dtheta
      murebins=-np.cos(murebins) #this converts theta to mu for integration
      diffmu=2 #to account for the fact that itegral is -1 to 1
   else:
      print('invalid sampling for gravitaional redshift: %s'%samp)
      return 0

   ns=srebins.size 
   gZ=np.zeros(ns*2).reshape(ns,2)
   numer=np.zeros(ns)
   denom=np.zeros(ns)

   if(write):
      outfile=root+'-gz.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')

   count_inf=0
   #print np.min(murebins),np.max(murebins)
   for ii in range(0,ns):
      gZ[ii,0]=srebins[ii]
      #applying a mu limit
      mulim=1.0 #1.0
      ind1=murebins<mulim; ind2=murebins>-mulim; ind12=ind1*ind2
      #remove infinities and Nan
      indNaN=np.isnan(xi2drebin[ii,:])
      indinf=np.isinf(xi2drebin[ii,:])
      indcheck=indNaN+indinf
      #remove the infinity and its symmetric points
      indsym=np.copy(indcheck)
      
      for muinf in murebins[indcheck]:
         indsym=indsym+(np.abs(murebins+muinf)<1e-4)
      indcheck=indsym
      #print indcheck
      indall=~indcheck*ind12
      if(np.sum(indcheck)>0):
         count_inf=count_inf+1
         #print('infinities:(r,mu,xi)',srebins[ii],murebins[indcheck],xi2drebin[ii,indcheck])
         #print ~indcheck*ind12
         #import pylab as pl
         #print indcheck
         #pl.plot(murebins[~indcheck],xi2drebin[ii,~indcheck],'s-')
         #pl.show()
         numer[ii]=-1;denom[ii]=0
      else:
         #summing
         #denom[ii]=np.sum((1+xi2drebin[ii,indall])*gZ[ii,0]*dtheta)
         #numer[ii]=H*np.sum((1+xi2drebin[ii,indall])*gZ[ii,0]*gZ[ii,0]*murebins[indall]*dtheta)
         #simpson rule for integration
         #Interpolation integration library
         import scipy.integrate as ssI
         from scipy import interpolate
         denom[ii]=ssI.simps(1+xi2drebin[ii,indall],murebins[indall])
         numer[ii]=H*ssI.simps((1+xi2drebin[ii,indall])*gZ[ii,0]*murebins[indall],murebins[indall])

      gZ[ii,1]=numer[ii]/denom[ii]

      if(write==1):
         fout.write('%12.8lf %12.8lf\n' %(gZ[ii,0],gZ[ii,1]))

   if(write==1):
      fout.close()

   if(0):#To integrate along r
      Rmin=1.0
      nR=15
      Rmax=60.0
      dLR=(np.log(Rmax)-np.log(Rmin))/nR
      #interpolate numer and denomr
      Idenomr = interpolate.splrep(srebins,denom, s=0,k=1)
      Inumerr = interpolate.splrep(srebins,numer, s=0,k=1)
      rr2d_tmp=np.linspace(0,Rmax,2000)
      denom_rtmp=interpolate.splev(rr2d_tmp,Idenomr, der=0)
      numer_rtmp=interpolate.splev(rr2d_tmp,Inumerr, der=0)

      rr=np.zeros(nR)
      gZnew=np.zeros(ns*2).reshape(ns,2)
      for ii in range(0,nR):
         r1=np.exp(np.log(Rmin)+ii*dLR)
         r2=np.exp(np.log(Rmin)+(ii+1)*dLR)
         rr[ii]=0.5*(r1+r2)
         ind1=rr2d_tmp>r1
         ind2=rr2d_tmp<r2
         denominator =ssI.simps(denom_rtmp[ind1*ind2],rr2d_tmp[ind1*ind2])
         numerator   =ssI.simps(numer_rtmp[ind1*ind2],rr2d_tmp[ind1*ind2])

         gZnew[ii,0]=rr[ii]
         gZnew[ii,1]=numerator/denominator
         if(0):
            import pylab as pl
            pl.figure(10)
            pl.plot(gZnew[ii,0],denominator,'b*')
            pl.plot(rr2d_tmp[ind1*ind2],denom_rtmp[ind1*ind2],'b-')
            pl.figure(20)
            pl.plot(gZnew[ii,0],numerator,'r*')
            pl.plot(rr2d_tmp[ind1*ind2],numer_rtmp[ind1*ind2],'r-')
            pl.figure(30)
            pl.plot(gZnew[ii,0],gZnew[ii,1],'k*')
            pl.plot(rr2d_tmp[ind1*ind2],numer_rtmp[ind1*ind2]/denom_rtmp[ind1*ind2],'k-')
            pl.show()
      #return np.column_stack([gZ,gZnew])
      #pl.plot(gZnew[:,0],gZnew[:,1])
      #pl.show()
      return gZnew

   return gZ
#end compute_gZ

def plot_xi02(xi02,mark='o-',lab='',lw=1):
   r=xi02[:,0]
   r2=r*r
   pl.plot(r,r2*xi02[:,1],mark,linewidth=lw,label=lab)
   pl.plot(r,r2*xi02[:,2],mark,linewidth=lw)
   pl.xlabel('r (Mpc/h)',fontsize=32)
   pl.ylabel(r'$r^2 \xi_l$',fontsize=32)
   if(lab!=''):
      pl.legend()

   return 0

def mean_cov(froot='',fold='',sky='NS',Njack=28,Sjack=9):
   xiarr=0
   reg=0
   if(sky=='NS'):
      jack=Njack+Sjack
   elif(sky=='N'):
      jack=Njack
   else:
      jack=Sjack

   title=''
   pl.figure()
   for s in sky:
      if s=='N':
         ext='-xi02.dat'
         nloop=Njack
         mark='r-'
         title=title+' N=red '
      elif s=='S':
         ext='-xi02.dat'
         nloop=Sjack
         mark='b-'
         title=title+' S=blue '
      for ii in range(0,nloop):
         if(fold=='../XI02/'):
            fname=froot+str(ii)+'-'+s+ext
         else:
            fname=froot+str(reg)+'-'+s+str(ii)+ext
         print(fname)
         data=np.loadtxt(fold+fname)
         if(ii==0 and xiarr==0):
            ns=data.shape[0]
            rr=data[:,0]
            rr2=rr*rr
            xi02=np.zeros(ns*2*jack).reshape(2*ns,jack)
            xiarr=1
         xi02[:ns,reg]=data[:,1]
         xi02[ns:,reg]=data[:,2]
         pl.plot(rr,rr2*data[:,1],mark)
         pl.plot(rr,rr2*data[:,2],mark)
         reg=reg+1

   #mean and error bar      
   mean_xi=np.mean(xi02,axis=1)
   cov=np.cov(xi02)*jack
   xierr=np.sqrt(np.diagonal(cov))
   pl.errorbar(rr,rr2*mean_xi[:ns],yerr=rr2*xierr[:ns],fmt='o',color='k',markersize=10,label='mean')
   pl.errorbar(rr,rr2*mean_xi[ns:],yerr=rr2*xierr[ns:],fmt='o',color='k',markersize=10)

   #plot full sample
   data=np.loadtxt(fold+'All-'+sky+'-xi02.dat')
   pl.plot(rr,rr2*data[:,1],'m*-',linewidth=4,markersize=10,label='full sample')
   pl.plot(rr,rr2*data[:,2],'m*-',linewidth=4,markersize=10)
   pl.xlabel(r'$r Mpc/h $',fontsize=32)
   pl.ylabel(r'$r^2 \xi_l$',fontsize=32)
   pl.title(title,fontsize=32)
   pl.legend()
   pl.tight_layout()
   outplot='plots/'+froot+'-'+sky+'.png'
   f_plot.append(outplot)
   pl.savefig(outplot)

   pl.figure()
   pl.pcolor(cov)
   pl.colorbar()

   corr=np.copy(cov)
   for ii in range(0,cov.shape[0]):
      for jj in range(0,cov.shape[1]):
         corr[ii,jj]=cov[ii,jj]/np.sqrt(cov[ii,ii]*cov[jj,jj])


   pl.figure()
   pl.pcolor(corr)
   pl.colorbar()
   outplot='plots/'+froot+'-corr.png'
   f_plot.append(outplot)
   pl.savefig(outplot)
   
 
   return 0

def Mocks_xi_comb(rootN='N',rootS='S',nmock1=0,nmock2=1000,outfold='xi2d',plots_dir='plots',nscomb=1,write=0):

   #compute north only and south only and north+south correlations
   pl.figure(1)  #for xi02 each region
   pl.figure(2)  #for xi02 combined for each region
   for ii in range(nmock1,nmock2):
      if(ii%50==0):
         print(ii)
      #compute the correlation functions for north
      root1=rootN+'-'+str(ii).zfill(4)
      outroot=outfold+'ngc/XI2D/'+root1.split('/')[-1]
      sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root1)
      Wrat=Wsum_data/Wsum_rand
      srebins, murebins, xi2drebin=compute_xi02(outroot,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=write)
      xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold+'ngc/XI02/',write=write)
      pl.figure(1)
      plot_xi02(xi02,mark='r-')

      #compute the correlation function for south
      root2=rootS+'-'+str(ii).zfill(4)
      outroot=outfold+'sgc/XI2D/'+root2.split('/')[-1]
      sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root2)
      Wrat=Wsum_data/Wsum_rand
      srebins, murebins, xi2drebin=compute_xi02(outroot,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=write)
      xi02=Xi_Legendre(root2,srebins, murebins, xi2drebin,outfold=outfold+'sgc/XI02/',write=write)
      pl.figure(2)
      plot_xi02(xi02,mark='b-')

      #compute the combined correlation function
      sbins, ns, mubins, nmu, DD,DR,RR=combine_count(root1,root2)
      outroot=outfold+'combgc/XI2D/'+root1.split('/')[-1]
      srebins, murebins, xi2drebin=compute_xi02(outroot,sbins, ns, mubins, nmu,
                                  DD,DR,RR, 1.0,nscomb=nscomb,write=write)
      xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold+'combgc/XI02/',write=write)
      pl.figure(3)
      plot_xi02(xi02,mark='k-')


   pl.figure(1)
   pl.title(rootN.split('/')[-1])
   pl.savefig(plots_dir+rootN.split('/')[-1]+'-ngc.png')

   pl.figure(2)
   pl.title(rootS.split('/')[-1])
   pl.savefig(plots_dir+rootS.split('/')[-1]+'-sgc.png')

   pl.figure(3)
   pl.title(rootN.split('/')[-1])
   pl.savefig(plots_dir+rootN.split('/')[-1]+'-comb.png')

   return 0
#End of Mocks


def AllJN_realize(nscomb=8):
   rootN='../CMASS-N-JN/'
   rootS='../CMASS-S-JN/'
   outfold1='../XI02/'
   outfoldcomb='../XI02-NS/'

   njack_N=28
   njack_S=9
   reg=0

   #compute north only and north + souht correlations
   root2=rootS+'All-S'
   pl.figure(1)  #for xi02 each region
   pl.figure(2)  #for xi02 combined for each region
   for ii in range(0,njack_N):
      root1=rootN+'JN'+str(ii)+'-N'
      #compute the correlation functions
      sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root1)
      Wrat=Wsum_data/Wsum_rand
      srebins, murebins, xi2drebin=compute_xi02(root1,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=1)
      xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold1,write=1)
      pl.figure(3)
      plot_xi02(xi02,mark='ro-',lab='N-'+str(ii))
      pl.figure(1)
      plot_xi02(xi02,mark='r-')

      #compute the combined correlation function
      sbins, ns, mubins, nmu, DD,DR,RR=combine_count(root1,root2)
      root=rootN+'COMB'+str(reg)+'-N'+str(ii)
      srebins, murebins, xi2drebin=compute_xi02(root,sbins, ns, mubins, nmu,
                                  DD,DR,RR, 1.0,nscomb=nscomb,write=1)
      xi02=Xi_Legendre(root,srebins, murebins, xi2drebin,outfold=outfoldcomb,write=1)
      pl.figure(3)
      plot_xi02(xi02,mark='bo-',lab='COMB')
      outplot='plots/Reg-N'+str(ii)+'.png'
      f_plot.append(outplot)
      pl.savefig(outplot)
      pl.close(3)
      pl.figure(2)
      plot_xi02(xi02,mark='r-')

      reg=reg+1

   #compute south only and north + south correlations
   root2=rootN+'All-N'
   for ii in range(0,njack_S):
      root1=rootS+'JN'+str(ii)+'-S'
      #compute the correlation functions
      sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root1)
      Wrat=Wsum_data/Wsum_rand
      srebins, murebins, xi2drebin=compute_xi02(root1,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=1)
      xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold1,write=1)
      pl.figure(3)
      plot_xi02(xi02,mark='ro-',lab='S-'+str(ii))
      pl.figure(1)
      plot_xi02(xi02,mark='b-')

      #compute the combined correlation function
      sbins, ns, mubins, nmu, DD,DR,RR=combine_count(root1,root2)
      root=rootN+'COMB'+str(reg)+'-S'+str(ii)
      srebins, murebins, xi2drebin=compute_xi02(root,sbins, ns, mubins, nmu,
                                  DD,DR,RR, 1.0,nscomb=nscomb,write=1)
      xi02=Xi_Legendre(root,srebins, murebins, xi2drebin,outfold=outfoldcomb,write=1)
      pl.figure(3)
      plot_xi02(xi02,mark='bo-',lab='COMB')
      outplot='plots/Reg-S'+str(ii)+'.png'
      f_plot.append(outplot)
      pl.savefig(outplot)
      pl.close(3)
      pl.figure(2)
      plot_xi02(xi02,mark='b-')

      reg=reg+1

   root1=rootN+'All-N'
   sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root1)
   Wrat=Wsum_data/Wsum_rand
   srebins, murebins, xi2drebin=compute_xi02(root1,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=1)
   xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold1,write=1)
   pl.figure(3)
   plot_xi02(xi02,mark='ro-',lab=root1)
   pl.figure(1)
   plot_xi02(xi02,mark='r--',lw=1,lab=root1)

   root2=rootS+'All-S'
   sbins, ns, mubins, nmu, DD,DR,RR, Wsum_data,Wsum_rand=load_PairCount_output(root1)
   Wrat=Wsum_data/Wsum_rand
   srebins, murebins, xi2drebin=compute_xi02(root1,sbins, ns, mubins, nmu,
                                  DD,DR,RR, Wrat,nscomb=nscomb,write=1)
   xi02=Xi_Legendre(root1,srebins, murebins, xi2drebin,outfold=outfold1,write=1)
   pl.figure(3)
   plot_xi02(xi02,mark='bo-',lab=root2)
   pl.figure(1)
   plot_xi02(xi02,mark='b--',lw=1,lab=root2)

   sbins, ns, mubins, nmu, DD,DR,RR=combine_count(root1,root2)
   srebins, murebins, xi2drebin=compute_xi02('All-NS',sbins, ns, mubins, nmu,
                               DD,DR,RR, 1.0,nscomb=nscomb,write=1)
   xi02=Xi_Legendre('All-NS',srebins, murebins, xi2drebin,outfold=outfoldcomb,write=1)
   plot_xi02(xi02,mark='k--',lab=root2)
   outplot='plots/All-NS.png'
   f_plot.append(outplot)
   pl.savefig(outplot)
   pl.close(3)

   pl.figure(1)
   pl.title('Individual region, red=N, blue=S',fontsize=32)
   outplot='plots/JN-All-xi02.png'
   f_plot.append(outplot)
   pl.savefig(outplot)

   pl.figure(2)
   plot_xi02(xi02,mark='k--',lab='All-NS')
   pl.title('COMB xi JN region, red=N, blue=S',fontsize=32)
   outplot='plots/JN-COMB-All-xi02.png'
   f_plot.append(outplot)
   pl.savefig(outplot)

def plot_CMASS():
   file='/global/u1/s/shadaba/Projects/Cosmomc/dr11_Comb_8mpc.txt'
   covfile='/global/u1/s/shadaba/Projects/Cosmomc/sdss_DR11_cov_xi02_8Mpch-0-24.mat'
   data=np.loadtxt(file)
   cov=np.loadtxt(covfile)
   diag_err=np.sqrt(np.diagonal(cov))

   rr=data[:-1,0]
   rr2=rr*rr
 
   pl.errorbar(rr,rr2*data[:-1,1],yerr=rr2*diag_err[:24],fmt='o',color='k',markersize=5,label='CMASS')
   pl.errorbar(rr,rr2*data[:-1,2],yerr=rr2*diag_err[24:],fmt='o',color='k',markersize=5)

   #pl.plot(rr,rr2*data[:,1],'ko-',linewidth=2,markersize=4,label='CMASS')
   #pl.plot(rr,rr2*data[:,2],'ko-',linewidth=2,markersize=4)
   pl.legend()


