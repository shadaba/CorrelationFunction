# This is a repo to host correlation function code

This obtained fast paircount with a backend pair-count calculator written in c and called from python using cython.
You only need to see the python part for most calculations. 
The code has on the fly jackknife sampling with several binning to calculate multipoles, wp, wtheta.
One can simply use it with single galaxy weights or incorporate pairwise weight using pip method along with angular up weights.
The inputs to the code can be provided either using a config file or using command line input.

Dependencies:
* numpy
* cython
* configobj (Only if you want to use configuration file for input, https://pypi.org/project/configobj/ )

## To Build the source:
 python setup.py build_ext --inplace

## Features:
 * Modular and easy to adapt to new input file. The idea is that you shouldn't create seperate file for each weight or subsample but rather write small function to deal with your single catalog file which then the rest of the code can interpret. See some examples.
 * Input can be entirely from command line or using a config file. Some advance options can only be given using config file.
 * Various mode of binning is available.
 * The jacknife subsampling is done on the fly in very efficienct way. Utilities are also available to divide the survey in jacknife region. For periodic box the code can do on the fly 3d and 2d jacknife sampling.
 * Parallel and can also be divided into multiple nodes.
 * The pair-wise-inverse-probability (pip) weight can be dealt with easily (still testing under progress).

## To Run the code with default setting and example data file use:
 python Runme_Correlation.py
 This will compute all the two point correlation,

## To get the 2d correlation and multipoles from the pair count use:
 python PairCountTOxi.py


## To get all the input options for pair count use:
 python Runme_Correlation.py --help

## Configuration file vs command line
  One can use a configuration file to provide input using -config_file option.
  It is possible to use both command line and configuration file but note that the options given in configuration file will be priority. This means if an option is provided by command line and in configuration file then the value given in configuration file will be used. To see few example configuration file check the config/ directory.

These two pythong script is very modular and has the ability to compute correlation function with lot of different choices. You could use the modular nature and directly import these function in any other python script for ease of access. Some scripts will be added to automate a lot of things. 

To understand the script options its important to understand how the correlation function is computed and various choices we make. There are many assumption which are never dicscussed and this code has the ability to test some of those assumption.



## Simplest case: 
You just need a data and random file to compute the correaltion function: The code in general has the ability to read fits, text and some file handler format. Also, it can start from RA,DEC, Redshift if needed. For simplicity we will describe only the text file scenario here.

## data files:
The data and random file should have 4 columns consisting of X,Y,Z and weights. The X,Y,Z can be in any unit. The weights are normally used to optimize the correlation function if you dont know what weight to use then just use 1 as weight for all the galaxies and randoms.
## The case 1: To run the correlation function with a simple data and random file use:


   python Runme_Correlation.py -data datafile -rand randomfile -out outroot
   outroot is just a path including some initial file name to be used for all of the output file
   when you run python Runtime_Correlation.py --help then it will show you the deafult options for these inputs.

## Running the correlation function on multiple processor
To use a given number of processor use option -nproc N . The optimal performance will be when you use -nproc = number ofr processor in the node. using larger number than that will over subscribe the processor and might slow down the performance.

If you want to use more than one node then you have to use qsub array job option
I will write about how to do it later

The above code compute only the pair count.
It write all the pair count with -out as the root name
* -out contains the full path of the output including a root name
 for example if you want to write the pair count under diretory example/PairCount and provide the file name and initial root test then -out example/PairCount/test

Now let say you want to write the 2d correlation function with root example/XI2D/test and monopole/quadruple with root example/XI02/test then you should run the PairCountToxi.py as follows:

 pyhton PairCountToxi.py example/PairCount/test example/XI2D/test example/XI02/test
 This will create the 2d correlation and multipole in the respective directory

All of the above directory should exists!! otherwise the script will fail with error


## Comment on Jackknife
The code has integrated jacknife facility. If the file has a column after weight for jacknife region then you simply need to provide how many jacknife region are total and the code will provide pair-count for all the jackknife realization.
For periodic box it can assign jacknife by simply providing njn, where njn a perfect cube will generate 3d jackknife regions and perfect square will do 2d jackknife regions.
There is also a utility script Jacknife_region_2D_keys.py, which generates jackknige region using the random catalogue and split the survey randoms in the sky by equally dividing the randoms in given two axis (typically RA,DEC).



## To submit jobs on multiple nodes:
 Note that the script uses job array, therefore in place of submitting one job on multiple nodes it submits independent jobs on each node. But, it takes care of the fact and combine the result without any extra work from use.
* to submit type
 qsub -t 1-nnode pbs_nnod4.sh
 where nnode is number of nodes to be used. check pbs_nnode4.sh file for details


### Speical instruction for Websky HOD catalogues ###
first of all you dont need to provide a file for random but just say -rand generate and the code will generate its own random.
you must also use randfactor option to say how many times the randoms should be generated.
I will suggest a -randfactor 5 for the initial tests to keep the run time short enough.

This does jacknife for this case with healpix therefore you will also need healpix package installed
you should provide -njn <NJN> (number of jacknife regions. NJN should be 12\*nside^2 to be valid. 
I will suggest using -njn 300 to start with.
The -z1z2 is generally used for redshift selection but for websky it is used to select radial distance which one can convert to redshift.
You should use a radial selection to cover a range in redshift. Ideally for RSD you don't want to use a redshift range which is too large such that the
matter power spectrum, bias and growth evolves significantly.
one must move the galaxy in redshift space to look at RSD signal from the HOD catalogue. This is achieved by setting -RSD 1 while running the correlation function. The redshit sapce converion uses H0 to convert velocity to distance. If distances are in comoving then you should use -H0 100 otherwise for physical unit use -H0 <value of H0 which is 70 for websky>
This moves the galaxies alond line-of-sight based on their respective line-of-sight velocity. If one set -RSD 0 (default option) then we would expect zero quadrupole moment withing uncertainity.

For the webskyHOD it also save the data and random it is going to use for the clustering calculation after all selection in the directory called tmpdir with filenames
tmpdata.txt and tmprand.txt respectively. You must create a tmpdir directory where you are running the code so that these files can be saved for inspection.

random generation: In order to generate random the code first apply all the selections on the data and generate randoms with uniform distribution on the sky and samples the line-of-sight
distance with replacement from the data. These are then converted to x,y,z in consistent manner to get the pair count.
