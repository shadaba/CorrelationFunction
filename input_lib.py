#!/usr/bin/env python

"""
This parse the config file and merge the command line input with config file.
It uses configobj library to parse the input file.
Follow the configobj library convention while creating the input file
"""

from __future__ import print_function,division
import numpy as np
import sys


import argparse

__author__ = "Shadab Alam"
__version__ = "1.0"
__email__  = "salam@roe.ac.uk"


def load_config_files(config_file):
    '''loads the config file and validate input using configspec'''
    #it is imported here to require only if config_file is used
    import configobj, validate

    cfg = """
    config_file = string
    Cinterpolation = boolean
    plots = integer(min=0,max=10)
    sampmode = integer
    data = string
    rand = string
    selection = string
    fastpc = boolean
    noDD = boolean
    noRR = boolean
    noDR = boolean
    noRD = boolean
    data2 = string
    rand2 = string
    RSD = integer
    nbins = int_list
    samplim = float_list
    pbc = integer
    los = integer
    njn = integer
    Lbox = float
    outfile = string
    interactive = integer
    filetype = string
    z1z2 = float_list
    randfactor = integer
    coord = string
    H0 = float
    omM = float
    MemSplit = integer
    num_load = integer
    nproc = integer
    nnode = integer
    nodeid = integer
    catname = string
    pip = boolean
    iip = boolean
    ang_up = boolean
    wang_up_file = string
    theta_max = float
    bit_perlong = integer
    jnfile = string
    njn_per_rose = integer
    """


    #validate
    spec = cfg.split("\n")
    config = configobj.ConfigObj(config_file, configspec=spec,interpolation=False)
    #nested config file
    nc=0;tc_file={}
    for tk in config.keys():
        if('import_config' in tk):
            nc=nc+1
            tc_file[nc]=config[tk]

    for ii in range(nc,0,-1):#important to start from last file to preserve the order
        this_config_file=tc_file[ii] 
        tconfig=load_config_files(this_config_file)
        for ttc in tconfig.keys():
            if(ttc not in config.keys()):
                config[ttc]=tconfig[ttc]
    
    return config

def load_validate_config(config_file):
    import configobj, validate

    config=load_config_files(config_file)
                    
    validator = validate.Validator()
    config.validate(validator, copy=True)
    config.filename = 'tmp.conf'
    config.write()

    if(False):
        print('***********config*****')
        for tk in config.keys():
            print(tk, type(config[tk]), config[tk])
            if(type([])==type(config[tk])):
                for tv in config[tk]:
                    print('    ',type(tv),tv)

    return config


def merge_config(args):
    '''Reads the config file if given and merges/updates args using config file'''

    if(args.config_file==None):
        return args


    config=load_validate_config(args.config_file)
    #config = ConfigObj(args.config_file)

    #current key list
    args_list=args.__dict__.keys()

    #loop over config entry and transfer to the args
    for tt,tkey in enumerate(config.keys()):
        args.__dict__[tkey]=config[tkey]



    #if conditional string interpolation is needed then perform string interpolations
    if(args.Cinterpolation):
        #This package does conditional interpolation
        import conditional_string_interpolation as csi
        args.__dict__=csi.interp_dic(args.__dict__,verbose=args.interactive)

    return args


def type_cast_config():
    if isinstance(args.plots, int):
       print('gg')

def diplay_args(args,outfile=None):

    key_list=args.__dict__.keys()
    if(outfile!=None):
        with open(outfile,'w') as fout:
            fout.write('Input values:\n')
            for tt,tkey in enumerate(key_list):
                fout.write('   ',tkey,' ',args.__dict__[tkey])
    else:
        print('Input values:')
        for tt,tkey in enumerate(key_list):
            print('   ',tkey,' ',args.__dict__[tkey])

    return


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__=="__main__":  
    parser = argparse.ArgumentParser(description='Calculate 2d correlation function:')
    #these should be input
    parser.add_argument('-config_file',default=None,help='Input can be provided using a config file or command line. A mix of both  (i.e. command line and config file) can also be used. Some inputs can only be provided in config file. If an option is provided in config file and command line then the value in config file will be used.')
    parser.add_argument('-plots',type=int,default=1)
    parser.add_argument('-sampmode' ,type=int,default=0,
                  help='''select the sampling mode 0=rmu , 1=rpara-rperp, 2=rtheta, 3=log(rpar)-rperp (The range of r_perp in samplim should be entered in the log space), 4=log(r)-theta (The range of r in samplim should be entered in the log space)''') 
    parser.add_argument('-data' ,default='example/data.txt',help='''data file with columns being  X,Y,Z,weight,JN,Vx,Vy,Vz
The minimum columns should be X,Y,Z,weight. X,Y,Z should be in Mpc/h and Vx,Vy,Vz is in simulation unit used incase of RSD''')
    parser.add_argument('-rand' ,default='example/randoms.txt',help='same as data files. No velocity is used for randoms. rand=generate for periodic box generates the random and rand=norand for periodic box should use analytic RR counts')
    parser.add_argument('-RSD'  ,type=int,default=0, help='This applies RSD (move galaxy from real space to redshift space) where needed')
    parser.add_argument('-data2' ,default='',help='second data file for cross-correlation, same structure as first data file')
    parser.add_argument('-rand2' ,default='',help='second random file for cross-correlation file')
    parser.add_argument('-nbins'  ,nargs='+',type=int,default=[32,100],help='number of bins in xi sampling')
    parser.add_argument('-samplim',nargs='+',type=float, default=[0,160,0,1],help='The range for 2d correlation function calculation depending on the modes it will take lower and higher limit for the two axis')
    parser.add_argument('-pbc'  ,type=int,default=0, help='This is to say whether the PBC should be applied or not') 
    parser.add_argument('-los'  ,type=int,default=0, help='''This is to choose a definition for line of sight. 0=los is defined as mid point of the pair, 1=los is defined along the Z axis,\n(only for cross correlation 2,3) \n2=los is define along the galaxies in the first data file, 3= second data file''') 
    parser.add_argument('-njn'  ,type=int,default=0, help='''0 no jacknife column, >0 number of jacknife regions,
                                 When pbc=1 and los=1 then jacknife can be added internally by providing njn.
                                 If njn is perfect cube then 3d jacknife is done, if njn is perfect square then 2d jacknife is done in x-y plane.''') 
    parser.add_argument('-Lbox'  ,type=float,default=0, help='box size to be used in case of pbc with eith generate random or analytic random')
    parser.add_argument('-outfile' ,default='example/PairCount/out',help="set the output file name")

    parser.add_argument('-interactive'  ,type=int, default=1, help="set 1 to print some information while running") # set zero to not print out things

    parser.add_argument('-filetype'  ,default='txt',help="The script can read few different file type: txt, polartxt, fhandle,fits") 
    parser.add_argument('-z1z2'  ,nargs='+',type=float,default=[0,100], help='To restrict the redshift range if RA,DEC,Redshift is provided.') 
    parser.add_argument('-randfactor'  ,type=int,default=0, help='''This is used incase random file is not provided for periodic box to generate uniform random in the box with a fixed seed. The number of randoms is set as the randomfactor times the number of data''')

    parser.add_argument('-coord',default='xyz', help='This is to decide whether cartesian coordinate inputs xx,yy,zz are available for coord=xyz or sky cor-ordinates RA,DEC,Z needs to be used with provided cosmology for coord=sky')

    parser.add_argument('-pip', type=str2bool, default=False, help='''set this to true if pip weight is needed, this cant be set true with iip
                                                                make sure LPfunc returns data and bitwise map''')
    parser.add_argument('-H0'  ,type=float,default=67.6, help='Hubble constant when cosmology is needed') 
    parser.add_argument('-omM'  ,type=float,default=0.315, help='matter density  when cosmology is needed') 

    parser.add_argument('-MemSplit'  ,type=int,default=0, help='Set 1 to load small amount of data in memory at a time, only works with FITS file') 
    parser.add_argument('-num_load'  ,type=int,default=1000000, help='number of points to be loaded at a time') 

    parser.add_argument('-nproc'  ,type=int,default=4, help='number of multiprocess to run') 
    parser.add_argument('-nnode'  ,type=int,default=1, help='number of nodes to run') 
    parser.add_argument('-nodeid'  ,type=int,default=0, help='nodeid for this job')

    parser.add_argument('-catname'  , default=None,help='''The name of the catalog, this decides 
                                         how to load the catalogue if a catalog utility function is available.
                                         When catname module is available then LPfunc and FITS_selection
                                         is used from this module. otherwise one can provide it directly''')
    parser.add_argument('-LPfunc'  , default=None,help='LoadData_part function needs to be passed')
    parser.add_argument('-FITS_Selection', default=None,help='for loading selection function needs to be passed')

    args = parser.parse_args()


    args=merge_config(args)

    diplay_args(args)
    

    #print(args.samplim,len(args.samplim))
    #basic input checks
    #for tt,tkey in enumerate(args.__dict__.keys()):
    #    print(tt,tkey,args.__dict__[tkey])


    if(args.pip):
       print('pip success')
