#Author: Shadab Alam, March 2015
#This program takes the pair count and normalization from 
#PairCount's code output and compute 2d correlation and
#monopole and quadrupole moment of corrlation


from __future__ import print_function,division

import numpy as np
import os
import sys
import io

#To add the config file functionality
import input_lib

import PairCountTOxi_utility as pcutil

import argparse

__author__ = "Shadab Alam"
__version__ = "1.0"
__email__  = "shadaba@andrew.cmu.edu"

if __name__ == "__main__":
   parser = argparse.ArgumentParser(description='Convert pair count to correlation functions:')
   #these should be input
   parser.add_argument('-config_file',nargs='+',default=None,help='Input can be provided using a config file or command line. A mix of both  (i.e. command line and config file) can also be used. Some inputs can only be provided in config file. If an option is provided in config file and command line then the value in config file will be used.')
   parser.add_argument('-Cinterpolation' ,default=False, type=bool, help='If the string inputs should be interpolated then set this to True')
   parser.add_argument('-plots',type=int,default=0)
   parser.add_argument('-sampmode' ,type=int,default=0,
                          help='''select the sampling mode 0=rmu , 1=rpara-rperp, 2=rtheta, 3=log(rpar)-rperp (The range of r_perp in samplim should be entered in the log space), 4=log(r)-theta (The range of r in samplim should be entered in the log space)
                          5 = log(r)-mu
                          6 = theta in radian,
                          7 = log(theta) in radian''')
   parser.add_argument('-interactive'  ,type=int,default=0, help='Decide verbosity of output')
   parser.add_argument('-njn'  ,type=int,default=0, help='0 no jacknife column, >0 number of jacknife regions')
   parser.add_argument('-Lbox'  ,type=float,default=0, help='box size to be used in case of pbc with eith generate random or analytic random')
   parser.add_argument('-xitype' ,default='auto',help="To decide between auto and cross-correlation")
   parser.add_argument('-pcroot' ,default='',help="set the input root for paircount")
   parser.add_argument('-xi2droot' ,default='',help="set the output 2d correlation file")
   parser.add_argument('-xi02root' ,default='',help="set the output wp/xi02 file")
   parser.add_argument('-noRR' ,type=bool,default=False,help="True if RR count is not needed or for analytic random estimates")
   parser.add_argument('-noDR' ,type=bool,default=False,help="True if RR count is not needed or for analytic random estimates")
   parser.add_argument('-noRD' ,type=bool,default=False,help="True if RR count is not needed or for analytic random estimates")

   parser.add_argument('-pip'  ,type=bool,default=False, help='set True if pip or iip weight was used for pair count')
   parser.add_argument('-iip'  ,type=bool,default=False, help='set True if pip or iip weight was used for pair count')
   parser.add_argument('-ang_up', type=bool, default=False, help='''set this to true if angular-up weight is needed,
                                             can be used in combination with either iip or pip. To use this you should also provide the
                                             DD pair count for the parent sample as the function of theta using wang_up_file''')
   parser.add_argument('-nocheck_pairnorm', type=bool, default=False, help='''The normalization when pair weight is applied is an approxximation. The validity of this approxximation depends on a theta_max. The code by default check for its convergence and quit if not valid. Sometimes we might want to continue despite not convergence and hence set this key to True for continuing despite non-convergence''')
   parser.add_argument('-selection'  , default='ALL',help='''This keyword can be used to apply any kind of selection for your catalogues''')
   parser.add_argument('-CompgZ' ,type=bool,default=False,help="True To compute the shell estimator for gravitational redshift")
   args_in = parser.parse_args()

   #need to handle the multiple config_file scenario and merge the inputs
   if(len(args_in.config_file)==1):
      args_in.config_file=args_in.config_file[0]
      #merge the config file and command line input
      args_in=input_lib.merge_config(args_in)
      args=args_in
   else:
      import copy
      args=[]
      for cfile in args_in.config_file:
         t_args=copy.deepcopy(args_in)
         t_args.config_file=cfile
         #merge the config file and command line input
         t_args=input_lib.merge_config(t_args)
         
         args.append(t_args)



def check_directory(dirin):
    '''checks if directory exists, and whether we need to create on'''

    if(not os.path.isdir(dirin)):
        print('***************\n   Need to create following directory\n   %s'%(dirin))
        res=input('enter yes to create directory or no to quit:')
        if(res.lower()=='yes'):
            os.mkdir(dirin)
        else:
            sys.exit()
    return
def get_sampcode(sampmode):
    sampcodes=['rmu','rp-pi','rtheta','logrp-pi','logr-theta','logrmu','angular','logangular']

    return sampcodes[sampmode]

def get_outdir(sampmode,root='xi02root'):
    if(root=='xi2droot'):
        return 'XI2D'

    dir_sampmode_dic={'XI02':[0,2,4,5], 'WP':[1,3],'Wtheta':[6,7]}
    for tt,tkey in enumerate( dir_sampmode_dic.keys()):
        if(sampmode in dir_sampmode_dic[tkey]):
            outdir=tkey
            break

    return outdir


def update_inputs_list(args):
    '''if mulitple config files are given then this handles that case'''
    if(not isinstance(args,list)):
       args=update_inputs(args)
       return args

    #first make sure all the config files are consistent in terms of weights
    #following input should be same in all config files
    const_input=['sampmode','Lbox','noRR', 'pip', 'iip', 'ang_up', 'selection', 'catname','filetype',
                 'coord', 'randfactor', 'theta_max', 'z1z2', 'samplim', 'nbins', 'H0', 'omM']
    mis_match=0
    tmsg={}
    for ii in range(1,len(args)):
       tmsg[ii]='## Mistmatch in config files\n %s <--> %s'%(args[0].config_file,args[ii].config_file)
       loc_mis=0
       for qq,quant in enumerate(const_input):
          if(quant not in args[0].__dict__.keys()):
              continue
          if(args[0].__dict__[quant]!=args[ii].__dict__[quant]):
             mis_match=mis_match+1
             if(loc_mis==0):
                print('## Mistmatch in config files\n %s <--> %s'%(args[0].config_file,args[ii].config_file))
             loc_mis=loc_mis+1
             print(quant+' : ',args[0].__dict__[quant],' <> ',args[ii].__dict__[quant])

    if(mis_match>0):
       print('Quitting:Please ensure that different config files have consistent inputs')
       sys.exit()


    #Now update the input for each of the config_files in a combined fashion
    args_out=update_inputs(args[0])
    args_out.inroots=[]
    args_out.njns=[]
    for ii in range(0,len(args)):
        args_out.inroots.append(args[ii].outfile)
        args_out.njns.append(args[ii].njn)


    #Now update the xi2droot
    for tt,troot in enumerate(['xi2droot','xi02root']):
        if(args_in.__dict__[troot]==''):
            str1=args_out.outfile
            for ii in range(1,len(args)):
                str2=args[ii].outfile
                str1=combine_str(str1,str2)

            out_comp=str1.split('/')
            #change the directory sampmode based directory names
            out_comp[-2]=get_outdir(args[ii].sampmode,root=troot)
            args_out.__dict__[troot]='/'.join(out_comp)
            check_directory('/'.join(out_comp[:-1]))
        else:
            args_out.__dict__[troot]=args_in.__dict__[troot]


    return args_out

    

def update_inputs(args):
    '''This fill in the needed input if they were not provided regarding file paths based on config_file'''
  
    #check if these input is available 
    for troot in ['pcroot', 'xi2droot','xi02root']:
        if(troot not in args.__dict__.keys()):
            args.__dict__[troot]=''

    #sort the output root
    if(args.pcroot==''):
         args.pcroot=args.outfile

    if(args.xi2droot==''):
         out_comp=args.outfile.split('/')
         #change the directory
         out_comp[-2]='XI2D'
         out_comp[-2]=get_outdir(args.sampmode,root='xi2droot')
         args.xi2droot='/'.join(out_comp)

         check_directory('/'.join(out_comp[:-1]))

    if(args.xi02root==''):
         out_comp=args.outfile.split('/')
         #change the directory sampmode based directory names
         out_comp[-2]=get_outdir(args.sampmode,root='xi02root')

         args.xi02root='/'.join(out_comp)
         #This checks if directory exists or not
         check_directory('/'.join(out_comp[:-1]))

    #add a few info
    if(args.pip or args.iip or args.ang_up or args.ang_up):#this is for pip
         args.usepip = 1
    else:
         args.usepip = 0

    #print('args:' )
    #for tkey in args.__dict__.keys():
    #    print(tkey,args.__dict__[tkey]) 

    if(args.coord !='sky' and args.xitype=='auto'): 
        if( (args.noDR or args.noRD) and args.noRR):
            args.RRtype='analytic_RR' 
        elif(args.noRR and (not args.noDR)):
            args.RRtype='noRR'
        else:
            args.RRtype=''
    else:
        args.RRtype=''
        for tkey in ['noDR','noRD','noRR']:
            if(args.__dict__[tkey]):
                args.RRtype=args.RRtype+tkey

    return args

def combine_str(str1,str2):
    '''combines strings such that the new one contains both'''
    #difflib if not available then please install
    import difflib

    diff=difflib.ndiff(str1,str2)

    strc=''
    for di in diff:
        #if(di[0]==''):
        strc=strc+di[2]

    return strc

def main_call(args):
   #To compute xi02 and xi2d in rmu sampling
    samp=get_sampcode(args.sampmode)

    xitype='auto'
    if('xitype' in args.__dict__.keys()):
        xitype=args.xitype
        
    if('data2' in args.__dict__.keys()):
        if(args.data2!=''):
            xitype='cross'
            print('xitype=cross because data2 is in args')

    #for later use
    args.xitype=xitype

    args=update_inputs_list(args)
    print('assuming xitype:',args.xitype,'RRtype= ',args.RRtype)

    #if(args.sampmode==0):
    #    samp='rmu'
    #call the function for computation
    if('inroots' not in args.__dict__.keys()):
        pcutil.xi2d_wp_xi02_froot_jn(args.pcroot,args.xi2droot,args.xi02root,xitype=xitype,
          nscomb=1,samp=samp,NJN=args.njn,RRtype=args.RRtype,Lbox=args.Lbox,usepip=args.usepip,compgZ=args.CompgZ)
    else: #neeed to combine multiple paircount while taking care of jackknifing issues
        print(args.inroots,args.njns,args.xi2droot,args.xi02root)
        pcutil.xi2d_wp_xi02_Manyfroot_jn(inroots=args.inroots,NJNs=args.njns,
                xi2droot=args.xi2droot,outroot=args.xi02root,xitype=xitype,nscomb=1,samp=samp,
                RRtype=args.RRtype,Lbox=args.Lbox,usepip=args.usepip,CompgZ=args.compgZ)
    
    #print('List of files written:',)
    #for ff in f_write:
    #   print(ff)

    #print('List of plots created:',)
    #for pp in f_plot:
    #   print(pp)


if __name__ == "__main__":
    #map of directory name for different samp mode
    main_call(args)
