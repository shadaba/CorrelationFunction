#Author: Shadab Alam, Nov 2020
'''After you have run angular clustering for Parent and fiber sample then we can run this script to generate angular upweights file'''
import numpy as np
import pylab as pl


import argparse

#To add the config file functionality
import input_lib

__author__ = "Shadab Alam"
__version__ = "1.0"
__email__  = "shadaba@andrew.cmu.edu"

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Calculate 2d Angular upWeight')
    #these should be input
    parser.add_argument('-config_file',default=None,help='Input can be provided using a config file or command line. A mix of both  (i.e. command line and config file) can also be used. Some inputs can only be provided in config file. If an option is provided in config file and command line then the value in config file will be used.')
    parser.add_argument('-Cinterpolation' ,default=False, type=bool, help='If the string inputs should be interpolated then set this to True')
    parser.add_argument('-plots',type=bool,default=True)
    parser.add_argument('-parent_root' ,type=str,default='',help='''paircount root for parent sample''')
    parser.add_argument('-fiber_root' ,type=str,default='',help='''paircount root for fiber sample''')
    parser.add_argument('-xitype' ,type=str,default='auto',help='''to fix whether auto or cross-correlations are analyzed, This has effect on the file name to be read.''')
    parser.add_argument('-outroot' ,type=str,default='',help='''output root to write the paircount, if not given then will be generated based on fiber_root''')


    args = parser.parse_args()
    #print(args)
    #merge the config file and command line input
    args=input_lib.merge_config(args)


def angular_upweight(args,roots_dic={'parent':'','fiber':''}):
    '''calculates the angular upweights'''
    
    if(args.xitype=='auto'):
        pair_list=['DD','DR']
    elif(args.xitype=='cross'):
        pair_list=['D1D2','D1R2']

    data_dic={}
    for tt,troot in enumerate(roots_dic.keys()):
        data_dic[troot]={}
        for ff,ptype in enumerate(pair_list):#,'norm','DR']):
            pcfile='%s-All-logangular-%s.dat'%(roots_dic[troot],ptype)
            #print(tt,troot,ptype,pcfile)
            data_dic[troot][ptype]=np.loadtxt(pcfile,skiprows=2)
            if(ff==0): #load theta bins
                tlines=open(pcfile,'r').readlines()
                theta_list=tlines[0].split()
                theta = np.array([float(ele) for ele in theta_list])
                #convert logtheta to theta
                theta=np.power(10,theta)
                theta_mid=0.5*(theta[1:]+theta[:-1])

    
    out_mat=np.column_stack([theta_mid,theta[:-1],theta[1:]])

    #removing nan and infinities
    ind_bad=np.zeros(theta_mid.size,dtype=bool)

    out_head='theta_mid, theta_min, theta_max'
    for ff,ptype in enumerate(pair_list):#,'DR']):
        ind_zero=data_dic['fiber'][ptype]==0
        data_dic['fiber'][ptype][ind_zero]=1
        ind_bad=ind_bad+ind_zero

        wang=data_dic['parent'][ptype]/data_dic['fiber'][ptype]
        out_mat=np.column_stack([out_mat,wang])
        out_head='%s, wang_%s'%(out_head,ptype)

    if(ind_bad.sum()>0):
        print('****** \n  Found zero pair count for fiber')
        msg='# %s \n'%(out_head)

        for ii in range(0,out_mat.shape[0]):
            if(ind_bad[ii]):
                for jj in range(out_mat.shape[1]):
                    msg=msg+str(out_mat[ii,jj])+'\t'
                msg=msg+'\n'

        print(msg+'\n\t ***')
        print('Removing these rows before writing them to file.')
        out_mat=out_mat[~ind_bad,:]

        #pl.plot(theta_mid,data_dic['parent'][ptype]/data_dic['fiber'][ptype],label='%s'%(ptype))
    #pl.xscale('log')
    #pl.legend()

    outfile=args.outroot+'_wang.txt'
    np.savetxt(outfile,out_mat,header=out_head)
    print('Angular up weights are written in file: \n  %s'%(outfile))

    return


if __name__=="__main__":

    roots_dic={'parent':args.parent_root,'fiber':args.fiber_root}

    if(args.outroot==''):
        tspl=args.fiber_root.split('PairCount')
        args.outroot=tspl[0]+'wang_up'+tspl[1]
    angular_upweight(args,roots_dic=roots_dic)

