#Author: Shadaba Alam, September 2014
#This program takes two randoms and two data files. It assumes that first 3 columns are co-ordinate. It divide the eah axis in Njack regions to produce total of Njack^3 Jacknife regions such that the total number of randoms in each regions is same. 
#This write new files for each of input file with an extra column saying which region the particle belongs to. These files are named with an extension which is printed on the wile running the program.
#This also creates many plots and write the boundaries in files under the -odir tag.
#Look at the end of output in order to find the list of files it has read and created.


#exampleruns on shiel for sims:
#python Jacknife_region_2D_keys.py -r1 /disk2/salam/Proc_Environment/RunPB/ValueAdded_cat/PB00-s10-g10-rand.fits -NjackRA 10 -NjackDEC 10 -ax1 xx -ax2 yy -ax3 zz -odir /disk2/salam/Proc_Environment/RunPB/ValueAdded_cat/Jacknife/PB00-s10-g10

import numpy as np
import fitsio
import argparse
import time
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl

from mpl_toolkits.mplot3d import Axes3D
#import pylab as pl
import sys
#sys.path.insert(0, '/home/shadaba/Projects/Stellar-mass/')
#import RDZ2XYZ as S2C

#Anderson Fiducial Cosmology
H0=70.0
omM=0.274
omL=1-omM

tm=time.localtime()
print('time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))
parser = argparse.ArgumentParser(description='Divide JN using RA DEC from fits file:')
#these should be input
parser.add_argument('-ax1'   ,default='RA')
parser.add_argument('-ax2'   ,default='DEC')
parser.add_argument('-ax3'   ,default=None)
parser.add_argument('-add_sel'   ,default='',help='''This is to add any addition selection on the random catalogue before splitting things into jackknife regions''')
#parser.add_argument('-plots',type=int,default=1) #use this keyword to decide plot or not
#parser.add_argument('-sav'  ,type=int,default=1) #use this keyword to decide save or not
parser.add_argument('-NjackRA',type=int,default=5)
parser.add_argument('-NjackDEC',type=int,default=5)
parser.add_argument('-Ngrid',type=int,default=10240)
parser.add_argument('-d1'   ,default='None')
parser.add_argument('-d2'   ,default='None')
parser.add_argument('-r1'   ,default='fits')
parser.add_argument('-r2'   ,default='fits')
parser.add_argument('-refiney' ,type=float,default=2) #to deal with edges
parser.add_argument('-refinez' ,type=float,default=4) #to deal with edges
parser.add_argument('-z1' ,type=float,default=0) #redshift limit
parser.add_argument('-z2' ,type=float,default=0) #redshift limit
parser.add_argument('-odir'   ,default='./')
parser.add_argument('-version'   ,default='vv')
parser.add_argument('-pixselfile'   ,default='',help='If one would want to apply a selection based on healpix map then you can provide a file with pixels to be selected here. Note that this assumes nside=256 and nest=True for healpy pixelization')
parser.add_argument('-tag'   ,default='')
parser.add_argument('-RAcut' ,nargs='+',type=float,default=[-1000,-1000],
      help="This is to apply a cut on RA. enter[RAmin,RAmax] pbc applied") #RA cut
parser.add_argument('-DECcut' ,nargs='+',type=float,default=[-1000,-1000],
      help="This is to apply a cut on DEC. enter[DECmin,DECmax] pbc applied")#DECcut
      #no DecCut is needed for current data


args = parser.parse_args()
print('argumenst: ',args)

#to setup for the cut on RA
#buffer for RA if RAcut is cyclic. To define continuos region
if(args.RAcut[0]!=-1000):
   if(args.RAcut[0]>args.RAcut[1]): #rollover to apply pbc
      RAmin=0; RAmax=np.mod(args.RAcut[1]-args.RAcut[0],360);
      RAbuf=args.RAcut[0]
   else:
      RAmin=args.RAcut[0]; RAmax=args.RAcut[1];
      RAbuf=0
else:
   RAbuf=0

#redshift cut
#Zcut=[0.43,0.7]
#Zcut=[0.16,0.43]
Zcut=[args.z1,args.z2]
args.odir=args.odir+'-zlim-'+str(args.z1)+'-'+str(args.z2)+'-'

print('using redshift cut: ',Zcut)



#check if Jacknife catalogue exists
file=args.odir+args.r1[:-5].split('/')[-1]+'_JN-'+str(args.NjackRA)+'-'+str(args.NjackDEC)+'.txt'
if(args.version=='vv'):
   ii=1
   while(os.path.isfile(file[:-4]+'_v'+str(ii)+'.txt')):
      ii=ii+1
      if(ii>10):#check if Jacknife catalogue exists
         print('Too many versions of the jacknife catalogue exist:',ii)
         sys.exit(-1)
   else:
      extension='_JN-'+str(args.NjackRA)+'-'+str(args.NjackDEC)
      extension=extension+'_v'+str(ii)
else:
   if(os.path.isfile(file[:-4]+'_'+args.version+'.txt')):
      print('The Jacknife version exists:',file[:-4]+'_'+args.version+'.txt')
      print('Please enter a different version')
   else:
      extension='_JN'+str(args.NjackRA)+'-'+str(args.NjackDEC)+'_v'+args.version

print('using extension:',extension)
#color choice
dcolor=['r','b','g','c','m','y','k']
Nd=len(dcolor)

#to store list of files
file_read= []
file_write=[]
file_plot= []

def Select_pixel(ra,dec,pixfile='',nside=256,nest=True):
    #Note that given pixel size alwys the pixel selection will be slight inaccurate at the boundary
    #The mask might be slightly bigger for compared to the smaller region tracer
    #Be mindful while using them to make plot and alway use ELG for jacknife assignment
    #pixelize the ra,dec
    import healpy as hp
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)
    
    pix_sel=np.loadtxt(pixfile,dtype=int)
    
    ind_valid=np.where(np.in1d(pixel,pix_sel)==1)[0]
    
    
    return ind_valid

def Find_xregion(xyz,Border,N):
   found=0
   for xreg in range(0,N):
      if(xyz[0]>=Border[xreg,0] and xyz[0]<Border[xreg,1]):
         found=1
         break
   if(found==1):
      return xreg
   else:
      print('************************************')
      print('Error value not found in x border:')
      print('coordinate looking for:',xyz)
      print('x border:')
      print( Border[beg:beg+N,0])
      print( Border[beg:beg+N,1])
      return -1

def Find_yregion(xyz,Border,N,xreg):
   found=0
   beg=xreg*N
   for yreg in range(beg,beg+N):
      if(xyz[1]>=Border[yreg,1] and xyz[1]<Border[yreg,3]):
         found=1
         break
   if(found==1):
      return yreg
   else:
      print('************************************')
      print('Error value not found in y border:')
      print('coordinate looking for:',xyz)
      print('y border:')
      print( Border[beg:beg+N,1])
      print( Border[beg:beg+N,3])
      return -1

def Find_zregion(xyz,Border,N,yreg):
   found=0
   beg=yreg*N
   for zreg in range(beg,beg+N):
      if(xyz[2]>=Border[zreg,2] and xyz[2]<Border[zreg,5]):
         found=1
         break
   if(found==1):
      return zreg
   else:
      print('************************************')
      print('Error value not found in z border:')
      print('coord looking for:',xyz ,yreg)
      print('z border:')
      print( Border[beg:beg+N,2])
      print( Border[beg:beg+N,5])
      return -1


def draw_1dcut(x,Hx,Njack,Border,label,tag):
   #create plot to show x partition
   pl.figure()
   pl.plot(x,Hx,'b-',label='Histogram')
   yval=np.array([np.min(Hx),np.max(Hx)])
   pl.plot(Border[0,0]*(np.zeros(2)+1),yval,'r--',linewidth=3,label=label)
   for ii in range(0,Njack):
      pl.plot(Border[ii,1]*(np.zeros(2)+1),yval,'r--',linewidth=3)
   pl.xlabel(label,fontsize=32)
   pl.ylabel('counts',fontsize=32)
   pl.legend()
   pl.tight_layout()
   file=args.odir+args.tag+label+'-1D'+extension+'-'+tag+'.png'
   pl.savefig(file)
   file_plot.append(file)
   #pl.show()

def draw_2dcut(Border,Nreg,xlab='RA', ylab='DEC'):
   pl.figure()
   square=np.zeros(5*2).reshape(5,2)
   for ii in range(0,Nreg):
      square[0,0]=Border[ii,0] ; square[0,1]=Border[ii,1]
      square[1,0]=Border[ii,2] ; square[1,1]=Border[ii,1]
      square[2,0]=Border[ii,2] ; square[2,1]=Border[ii,3]
      square[3,0]=Border[ii,0] ; square[3,1]=Border[ii,3]
      square[4,0]=Border[ii,0] ; square[4,1]=Border[ii,1]
      pl.plot(square[:,0],square[:,1],dcolor[ii%Nd]+'-',linewidth=3)
   pl.xlabel(xlab,fontsize=28)
   pl.ylabel(ylab,fontsize=28)
   pl.tight_layout()
   file=args.odir+args.tag+xlab+'-'+ylab+'-2D'+extension+'.png'
   pl.savefig(file)
   file_plot.append(file)

def draw_cube(ax,v1,v2,reg):
   square=np.zeros(15).reshape(5,3)
   #square1
   square[0,:]=v1
   square[1,:]=np.array([v1[0],v2[1],v1[2]])
   square[2,:]=np.array([v2[0],v2[1],v1[2]])
   square[3,:]=np.array([v2[0],v1[1],v1[2]])
   square[4,:]=np.array([v1[0],v1[1],v1[2]])
   ax.plot(square[:,0],square[:,1],square[:,2],color=dcolor[reg],linewidth=3)
   #square2
   square[0,:]=v2
   square[1,:]=np.array([v2[0],v1[1],v2[2]])
   square[2,:]=np.array([v1[0],v1[1],v2[2]])
   square[3,:]=np.array([v1[0],v2[1],v2[2]])
   square[4,:]=np.array([v2[0],v2[1],v2[2]])
   ax.plot(square[:,0],square[:,1],square[:,2],color=dcolor[reg],linewidth=3)
   #zconnect
   connectz=np.array([[v1[0],v1[1],v1[2]],[v1[0],v1[1],v2[2]]])
   ax.plot(connectz[:,0],connectz[:,1],connectz[:,2],color=dcolor[reg],linewidth=3)
   connectz=np.array([[v2[0],v1[1],v1[2]],[v2[0],v1[1],v2[2]]])
   ax.plot(connectz[:,0],connectz[:,1],connectz[:,2],color=dcolor[reg],linewidth=3)
   connectz=np.array([[v1[0],v2[1],v1[2]],[v1[0],v2[1],v2[2]]])
   ax.plot(connectz[:,0],connectz[:,1],connectz[:,2],color=dcolor[reg],linewidth=3)
   connectz=np.array([[v2[0],v2[1],v1[2]],[v2[0],v2[1],v2[2]]])
   ax.plot(connectz[:,0],connectz[:,1],connectz[:,2],color=dcolor[reg],linewidth=3)

def draw_3dcut(Border,Nreg):
   fig=pl.figure()
   ax = fig.add_subplot(111, projection='3d')
   for ii in range(0,Nreg):
     v1=Border[ii,0:3] 
     v2=Border[ii,3:6]
     draw_cube(ax,v1,v2,ii%Nd) 
   pl.xlabel('X (Mpc/h)',fontsize=28)
   pl.ylabel('Y (Mpc/h)',fontsize=28)
   pl.tight_layout()
   file=args.odir+'XYZ-3D-'+extension+'.png'
   pl.savefig(file)
   file_plot.append(file)
   pl.show()

def check_data_range(data,min_all,max_all,fn):
   for ii in range(0,3):
      low =np.sum(data[:,ii]<min_all[ii])
      high=np.sum(data[:,ii]>max_all[ii])
      if(low!=0 or high!=0):
          print('********************')
          print('Error: data is not within the randoms')
          print('data file:',fn, 'axis:',ii)
          print('number of points below:',low)
          print('number of points above:',high)
          sys.exit(-1)

def get_RADECZW(file,type='rand'):
   file_read.append(file)
   if(file[-5:]=='.fits'):
      f_rand = fitsio.FITS(file)
      if(Zcut[0]!=Zcut[1]):
         Zsel  = "%s >= %s && %s < %s"%(args.ax3,str(Zcut[0]),args.ax3,str(Zcut[1]))
      else:
         Zsel="1>0"

      if(args.add_sel!=''):
          Zsel='%s && %s'%(Zsel,args.add_sel)
      print('**** Applying selection: %s ****'%Zsel)
      index=f_rand[1].where(Zsel)
    
      if(args.pixselfile!=''):#select object only within some pixel
          ind_pixel=Select_pixel(f_rand[1]['RA'][index],f_rand[1]['DEC'][index],
                                  pixfile=args.pixselfile,nside=256,nest=True)
          print('Applied Pixel selection: before/after',index.size,ind_pixel.size)
          index=index[ind_pixel]

      rand=np.zeros(index.size*4).reshape(index.size,4)
      rand[:,0]= f_rand[1][args.ax1][index]
      rand[:,1]= f_rand[1][args.ax2][index]
      if(args.ax3 is not None):
          rand[:,2]= f_rand[1][args.ax3][index]
      if(type=='rand'):
         try:
            rand[:,3]= f_rand[1]['WEIGHT_FKP'][index]
         except:
            rand[:,3]=np.ones(rand.shape[0])
      elif(type=='data'):
         wfkp = f_rand[1]['WEIGHT_FKP'][index]
         wCP  = f_rand[1]['WEIGHT_CP'][index]
         wNOZ = f_rand[1]['WEIGHT_NOZ'][index]
         wSTAR= f_rand[1]['WEIGHT_STAR'][index]
         wSee = f_rand[1]['WEIGHT_SEEING'][index]
         rand[:,3]=wfkp*wSTAR*wSee*(wCP + wNOZ-1)
         wfkp=[None]
         wCP=[None]
         wNOZ=[None]
         wSTAR=[None]
         wSee =[None]
   else:
      print('reading : %s'%(file))
      rand=np.loadtxt(file)
      print('Assuming RA,DEC,Z as columns and res ignored: found %s rows'%(rand.shape[0]))
      #assume file has ra,dec,redshift as the columns rest of the columns are ignored
      ind1=rand[:,2]>=Zcut[0]; ind2=rand[:,2]<Zcut[1]
      rand=rand[ind1*ind2,:3]
      #add the weight column
      rand=np.column_stack([rand[:,:2],np.ones(rand.shape[0])])
      #rand[:,3]=np.ones(rand.shape[0])

   print('size of random array:',rand.shape)


   #Apply RAcut and fold the RA to make one contiguos region
   if(args.RAcut[0]!=-1000):
      #subtract the buffer
      rand[:,0]=np.mod(rand[:,0]-RAbuf,360);
      ind1=rand[:,0]>RAmin
      ind2=rand[:,0]<RAmax
      rand=rand[ind1*ind2,:]
      print('******using RA=modulo(RA-%10.4f,360)' %(RAbuf))
      print('RAmin= %10.4f , RAmax= %10.4f'%(RAmin,RAmax))
      print('rand size before %d  after  %d'%(ind1.size,rand.shape[0]))

   #Apply DECcut only to select a subsample
   if(args.DECcut[0]!=-1000):
      ind1=rand[:,1]>args.DECcut[0]
      ind2=rand[:,1]<args.DECcut[1]
      rand=rand[ind1*ind2,:]
      print('DECmin= %10.4f , DECmax= %10.4f'%(args.DECcut[0],args.DECcut[1]))
      print('rand size before %d  after  %d'%(ind1.size,rand.shape[0]))


   print('size of random array:',rand.shape)
   posmin=np.min(rand[:,:3],0)
   posmax=np.max(rand[:,:3],0)
  
   if(file[-5:]=='.fits'):
      f_rand.close()
   return rand, posmin, posmax


#number of grids to make
Ngrid=args.Ngrid
refiney=args.refiney
refinez=args.refinez

#load randoms
rand1,posmin1,posmax1=get_RADECZW(args.r1,type='rand')
if(args.r2 !='fits'):
   rand2,posmin2,posmax2=get_RADECZW(args.r2,type='rand')
   rand=np.row_stack([rand1,rand2])
   print(rand.shape)
#find the combined minimum of random1 and random2 and dx
   min_all=np.zeros(3)
   max_all=np.zeros(3)
   for ii in range(0,3):
      min_all[ii]=np.min([posmin1[ii],posmin2[ii]])-1
      max_all[ii]=np.max([posmax1[ii],posmax2[ii]])+1
else:
   min_all=np.copy(posmin1)
   max_all=np.copy(posmax1)
   rand=rand1


#define gridding
tm=time.localtime()
print('loaded randoms ,time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))

#number of random per jacknife region
Njack1=args.NjackRA
Njack2=args.NjackDEC*Njack1

#define percentile limit
ax1_plim=[0];dpx1=100.0/args.NjackRA
ax2_plim=[0];dpx2=100.0/args.NjackDEC
for ii in range(1,args.NjackRA+1):
    ax1_plim.append(ii*dpx1)
for ii in range(1,args.NjackDEC+1):
    ax2_plim.append(ii*dpx2)

ax1_plim[-1]=100
ax2_plim[-1]=100

print('ax1_plim: ',ax1_plim)
print('ax2_plim: ',ax2_plim)

Tot_random=rand.shape[0]

#number of randoms per JN regions for the axis
Nper_axis1=0.9999*Tot_random/Njack1
Nper_axis2=0.9999*Tot_random/Njack2

Border_x=np.zeros(Njack1*3).reshape(Njack1,3)  #to store the region border
Border_y=np.zeros(Njack2*5).reshape(Njack2,5)  #to store the region border

Nper_reg=Tot_random/float(Njack2)

print('Number of randoms (total)= ',Tot_random)
print('Number of Jacknife regions=',Njack2)
print('Number of randoms per regions=', Nper_reg)

print('setup done, partition begin, time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))


ax1_pval=np.percentile(rand[:,0],ax1_plim)

#check if got all the randoms
print('   region  low_x         high_x        Ngal')


for reg in range(0,Njack1):
   Border_x[reg,0]=ax1_pval[reg]
   Border_x[reg,1]=ax1_pval[reg+1]
   Border_x[reg,2]=1.0*Tot_random/Njack1
   print('xborder:',reg,Border_x[reg,0],Border_x[reg,1],Border_x[reg,2])

#find y/DEC boundary
print('yregion xregion low_y         high_y       Nreg')

reg=0
for ii in range(0,args.NjackRA):
   #rand1 indices
   ind1=rand[:,0]>=Border_x[ii,0]
   ind2=rand[:,0]< Border_x[ii,1]
   ind_r=ind1*ind2
   ax2_pval=np.percentile(rand[ind_r,1],ax2_plim)
   for jj in range(0,args.NjackDEC):
      Border_y[reg,0]=Border_x[ii,0]
      Border_y[reg,2]=Border_x[ii,1]
      Border_y[reg,1]=ax2_pval[jj]
      Border_y[reg,3]=ax2_pval[jj+1]
      Border_y[reg,4]=1.0*Tot_random/Njack2
      print('ybord:',reg, ii, Border_y[reg,1],Border_y[reg,3],Border_y[reg,4])
      reg=reg+1

draw_2dcut(Border_y,Njack2)#,xlab=lab)
pl.show()
#end of finding y boundary
#create 2D cuts plots


#Write the boundaries to a file:  X axis
file=args.odir+args.tag+'Border_%s'%(args.ax1)+extension+'.txt'
file_write.append(file)
fborder=open(file,'w')
fborder.write('#partition along x-axis Njack=%4i\n' %(Njack1))
fborder.write('# No.          xlow             xup       Nrandom\n')
for ii in range(0,Njack1):
   fborder.write('%3i    %12.5f    %12.5f    %10i\n' %(ii,
      Border_x[ii,0],Border_x[ii,1],Border_x[ii,2]))

fborder.close()

#Write the boundaries to a file:  XY axis
file=args.odir+args.tag+'Border_%s-%s'%(args.ax1,args.ax2)+extension+'.txt'
file_write.append(file)
fborder=open(file,'w')
fborder.write('#partition in %s-%s-plane Njack=(%4i,%4i)\n'%(args.ax1,args.ax2,args.NjackRA,args.NjackDEC))

fborder.write('#RAbuf= %12.4f\n'%RAbuf) 
if(args.RAcut[0]!=-1000):
   fborder.write('#RAcut= %12.4f %12.4f\n'%(args.RAcut[0],args.RAcut[1]))
else:
   fborder.write('#RAcut= -1000 1000\n')

if(RAbuf!=0):
   fborder.write('#No.  RAlow-'+str(RAbuf)+'%360'+'  DEClow  RAup-'+str(RAbuf)+'%360 DECup  Nrandom\n')
else:
   fborder.write('#No.  RAlow  DEClow  RAup DECup  Nrandom\n')

for ii in range(0,Njack2):
   fborder.write('%3i   %12.5f   %12.5f   %12.5f   %12.5f %10i\n' %(ii,
      Border_y[ii,0],Border_y[ii,1],Border_y[ii,2],Border_y[ii,3],Border_y[ii,4]))

fborder.close()
if(1):
   print( '\n\n***files read:***')
   for ff in file_read:
      print( ff)

   print('\n\n***files written:***')
   for ff in file_write:
      print(ff)

   print('\n***files plots:***')
   for ff in file_plot:
      print(ff)

   sys.exit()

tm=time.localtime()
print('time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))
#create new catalogue with region
#Array to store number of objects in each regions
Nrand1=np.zeros(Njack2)
Nrand2=np.zeros(Njack2)
Ndata1=np.zeros(Njack2)
Ndata2=np.zeros(Njack2)

#Write random1
file=args.odir+args.r1[:-5].split('/')[-1]+extension+'.txt'
file_write.append(file)
frand=open(file,'w')
mins=np.zeros(3)+10000
maxs=np.zeros(3)-10000
for ii in range(0,rand1.shape[0]):
   if(ii%5000000==0):
      print(file,ii,rand1.shape[0])
   xreg  = Find_xregion(rand1[ii,:],Border_x,args.NjackRA)
   jn   = Find_yregion(rand1[ii,:],Border_y,args.NjackDEC,xreg)

   #(RA,DEC,z) --> (x,y,z) to file
   if(RAbuf !=-1):
      XYZ=S2C.RDZ2XYZ(np.mod(rand1[ii,0]+RAbuf,360),rand1[ii,1],rand1[ii,2],H0, omM, omL)
   else:
      XYZ=S2C.RDZ2XYZ(rand1[ii,0],rand1[ii,1],rand1[ii,2],H0, omM, omL)
   XYZ=XYZ*H0/100.0  #gives distance in Mpc/h
   for kk in range(0,3):
      if(mins[kk]>XYZ[kk]):
         mins[kk]=XYZ[kk]
      if(maxs[kk]<XYZ[kk]):
         maxs[kk]=XYZ[kk]

   frand.write('%12.8lf %12.8lf %12.8lf %12.8lf %2d\n'
              %(XYZ[0],XYZ[1],XYZ[2],rand[ii,3],jn))

   Nrand1[jn]=Nrand1[jn]+1

frand.close()
print('written rand: ',file)
print('minimums: ',mins)
print('maximums: ',maxs)
print('box size: ',maxs-mins)
print('Nga: ',rand1.shape[0])



tm=time.localtime()
print('time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))
#Write random2 only if its a different file than random1
if(args.r1!=args.r2 and args.r2!='fits'):
   file=args.odir+args.r2[:-5].split('/')[-1]+extension+'.txt'
   file_write.append(file)
   frand=open(file,'w')
   mins=np.zeros(3)+10000
   maxs=np.zeros(3)-10000
   for ii in range(0,rand2.shape[0]):
      if(ii%50000000==0):
         print(file,ii,rand2.shape[0])
      xreg = Find_xregion(rand2[ii,:],Border_x,args.NjackRA)
      jn   = Find_yregion(rand2[ii,:],Border_y,args.NjackDEC,xreg)
      #(RA,DEC,z) --> (x,y,z) to file
      if(RAbuf !=-1):
         XYZ=S2C.RDZ2XYZ(np.mod(rand2[ii,0]+RAbuf,360),rand2[ii,1],rand2[ii,2],H0, omM, omL)
      else:
         XYZ=S2C.RDZ2XYZ(rand2[ii,0],rand2[ii,1],rand2[ii,2],H0, omM, omL)
      XYZ=XYZ*H0/100.0  #gives distance in Mpc/h
      for kk in range(0,3):
         if(mins[kk]>XYZ[kk]):
            mins[kk]=XYZ[kk]
         if(maxs[kk]<XYZ[kk]):
            maxs[kk]=XYZ[kk]

      frand.write('%12.8lf %12.8lf %12.8lf %12.8lf %2d\n'
              %(XYZ[0],XYZ[1],XYZ[2],rand[ii,3],jn))

      Nrand2[jn]=Nrand2[jn]+1
   frand.close()
   print('written rand2: ',file)
   print('minimums: ',mins)
   print('maximums: ',maxs)
   print('box size: ',maxs-mins)
   print('Nga: ',rand2.shape[0])

tm=time.localtime()
print('time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))
#Write data1
data1,posmin1,posmax1=get_RADECZW(args.d1,type='data')
check_data_range(data1,min_all,max_all,args.d1)
file_read.append(args.d1)
#ix=np.floor((data1[:,0]-min_all[0])/dx[0])
#iy=np.floor((data1[:,1]-min_all[1])/dx[1])
#iz=np.floor((data1[:,2]-min_all[2])/dx[2])

file=args.odir+args.d1[:-5].split('/')[-1]+extension+'.txt'
file_write.append(file)
fdata1=open(file,'w')
for ii in range(0,data1.shape[0]):
   if(ii%100000==0):
      print(file,ii,data1.shape[0])
   xreg = Find_xregion(data1[ii,:],Border_x,args.NjackRA)
   jn   = Find_yregion(data1[ii,:],Border_y,args.NjackDEC,xreg)

   #(RA,DEC,z) --> (x,y,z) to file
   if(RAbuf!=-1):
      XYZ=S2C.RDZ2XYZ(np.mod(data1[ii,0]+RAbuf,360),data1[ii,1],data1[ii,2],H0, omM, omL)
   else:
      XYZ=S2C.RDZ2XYZ(data1[ii,0],data1[ii,1],data1[ii,2],H0, omM, omL)
   XYZ=XYZ*H0/100.0  #gives distance in Mpc/h
   for kk in range(0,3):
      if(mins[kk]>XYZ[kk]):
         mins[kk]=XYZ[kk]
      if(maxs[kk]<XYZ[kk]):
         maxs[kk]=XYZ[kk]

   fdata1.write('%12.8lf %12.8lf %12.8lf %12.8lf %2d\n'
              %(XYZ[0],XYZ[1],XYZ[2],data1[ii,3],jn))

   Ndata1[jn]=Ndata1[jn]+1

fdata1.close()
print('written data1: ',file)
print('minimums: ',mins)
print('maximums: ',maxs)
print('box size: ',maxs-mins)
print('Nga: ',data1.shape[0])

tm=time.localtime()
print('time now:',str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec))
#Write data2 only if its different file from data1
if(args.d1!=args.d2 and args.d2 !='None'):
   data2,posmin2,posmax2=get_RADECZW(args.d2,type='data')
   check_data_range(data2,min_all,max_all,args.d2)
   file_read.append(args.d2)
   file=args.odir+args.d2[:-5].split('/')[-1]+extension+'.txt'
   file_write.append(file)
   fdata2=open(file,'w')
   for ii in range(0,data2.shape[0]):
      if(ii%100000==0):
         print(file,ii,data2.shape[0])
      xreg = Find_xregion(data2[ii,:],Border_x,args.NjackRA)
      jn   = Find_yregion(data2[ii,:],Border_y,args.NjackDEC,xreg)
      #(RA,DEC,z) --> (x,y,z) to file
      if(RAbuf!=-1):
         XYZ=S2C.RDZ2XYZ(np.mod(data2[ii,0]+RAbuf,360),data21[ii,1],data2[ii,2],H0, omM, omL)
      else:
         XYZ=S2C.RDZ2XYZ(data2[ii,0],data2[ii,1],data2[ii,2],H0, omM, omL)
      XYZ=XYZ*H0/100.0  #gives distance in Mpc/h
      for kk in range(0,3):
         if(mins[kk]>XYZ[kk]):
            mins[kk]=XYZ[kk]
         if(maxs[kk]<XYZ[kk]):
            maxs[kk]=XYZ[kk]

      frand.write('%12.8lf %12.8lf %12.8lf %12.8lf %2d\n'
              %(XYZ[0],XYZ[1],XYZ[2],data2[ii,3],jn))

      Ndata2[jn]=Ndata2[jn]+1

   fdata2.close()
   print('written data2: ',file)

#save and plot some statistics of the regions
pl.figure()
pl.plot(Nrand1/np.sum(Nrand1),'ro',label='Nrand1='+str(np.sum(Nrand1)))
if(args.r1!=args.r2 and args.r2 !='fits'): #plot only if two random files are different
   pl.plot(Nrand2/np.sum(Nrand2),'r*',label='Nrand2='+str(np.sum(Nrand2)))
pl.plot(Ndata1/np.sum(Ndata1),'bo',label='Ndata1='+str(np.sum(Ndata1)))
if(args.d1!=args.d2 and args.d2 != 'None'): #plot only if two data files are different
   pl.plot(Ndata2/np.sum(Ndata2),'b*',label='Ndata2='+str(np.sum(Ndata2)))

pl.xlabel('regions',fontsize=28)
pl.ylabel('normalized count',fontsize=28)
pl.legend()
pl.tight_layout()
pl.savefig(args.odir+args.tag+'Region_dist'+extension+'.png')
file_plot.append(args.odir+args.tag+'Region_dist'+extension+'.png')
pl.show()
#save this data to file
file_write.append(args.odir+args.tag+'Region_dist'+extension+'.txt')
freg=open(args.odir+args.tag+'Region_dist'+extension+'.txt','w')
for ii in range(0,Njack2):
   freg.write('%3i %8i %8i %8i %8i\n' %(ii,Nrand1[ii],Nrand2[ii],Ndata1[ii],Ndata2[ii]))

freg.close()


print('\n\n***files read:***')
for ff in file_read:
   print(ff)

print('\n\n***files written:***')
for ff in file_write:
   print(ff)

print('\n***files plots:***')
for ff in file_plot:
   print(ff)

